#!/bin/bash
 
# Before this steps databases should be created.
# For create database run 'sqlite3 DATABASE_NAME'
# Then in sqlite shell run '.read path_to_websym_dir/SYMWEB-Simulator/src/main/resources/setup.sql'
# If you are in SYMWEB-Simulator dir you can run '.read src/main/resources/setup.sql'
 
# Build module, you can remove last option for test your build
mvn clean install -DskipTests
echo "Module is ready to install"
# Change name to simulator.jar
mv target/SYMWEB-Simulator-0.0.1-SNAPSHOT.jar target/simulator.jar
echo "Name of jar file is changed to simulator"
# Run server on default port
java -jar target/simulator.jar
