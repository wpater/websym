DROP TABLE IF EXISTS `simulations`;
CREATE TABLE `simulations` (
  simulationId INTEGER PRIMARY KEY AUTOINCREMENT,
  filesSimulationId INTEGER,
  requestedSimulationId INTEGER NOT NULL,
  wasStarted TINYINT NOT NULL
);
DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
	reportId INTEGER PRIMARY KEY AUTOINCREMENT,
	JSONString TEXT NOT NULL
);