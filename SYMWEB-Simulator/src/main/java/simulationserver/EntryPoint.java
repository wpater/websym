package simulationserver;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import simulationserver.database.mappers.IReportMapper;
import simulationserver.database.mappers.ISimulationMapper;

@SpringBootApplication
@ComponentScan
@PropertySource("classpath:server.properties")
public class EntryPoint {

    @Value("${server.ssl.key-store}")
    public void setKeyStore(String keyStore) {
        System.setProperty("javax.net.ssl.keyStore", keyStore);
    }
    @Value("${server.ssl.key-store-password}")
    public void setKeyStorePassword(String keyStorePassword) {
        System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);
    }
    @Value("${server.ssl.key-store-type}")
    public void setKeyStoreType(String keyStoreType) {
        System.setProperty("javax.net.ssl.keyStoreType", keyStoreType);
    }
    @Value("${server.ssl.trust-store}")
    public void setTrustStore(String trustStore) {
        System.setProperty("javax.net.ssl.trustStore", trustStore);
    }
    @Value("${server.ssl.trust-store-password}")
    public void setTrustStorePassword(String trustStorePassword) {
        System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
    }
    @Value("${server.ssl.trust-store-type}")
    public void setTrustStoreType(String trustStoreType) {
        System.setProperty("javax.net.ssl.trustStoreType", trustStoreType);
    }
    
    
    
	@Bean
	@ConfigurationProperties(prefix = "custom.rest.connection")
	public HttpComponentsClientHttpRequestFactory customHttpRequestFactory() {
		return new HttpComponentsClientHttpRequestFactory();
	}

	@Bean
	public RestTemplate customRestTemplate() {
		return new RestTemplate(customHttpRequestFactory());
	}

	public static void main(String[] args) throws IOException {
		System.out.println("Server with simulator started");
		ApplicationContext ctx = SpringApplication.run(EntryPoint.class, args);
		// start simulator and other stuff
		ISimulationMapper simulationMapper = (ISimulationMapper) ctx.getBean("simulationMapper");
		simulationMapper.ensureTable();
		IReportMapper reportMapper = (IReportMapper) ctx.getBean("reportMapper");
		reportMapper.ensureTable();
		ISimulationServer simulator = (ISimulationServer) ctx.getBean("simulator");
		Reporter r = (Reporter) ctx.getBean("reporter");
		r.start();
		simulator.start();
	}

}
