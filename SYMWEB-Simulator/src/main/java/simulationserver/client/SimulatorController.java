package simulationserver.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import simulationserver.FileSet;
import simulationserver.ISimulationServer;
import simulationserver.exceptions.WrongPayloadException;
import simulationserver.scheduler.ISimulationHandler;
import simulationserver.scheduler.SimulationHandlerImpl;

@RestController
public class SimulatorController {
    @Autowired
    private ISimulationServer simulator;
    
    @Autowired
    private ISimulationHandler simulationHandler;
    
    @Autowired
    private FileSet fileSet;

    private @Value("${databaseConfigFilePath}") String dbConfigFilePath;
    
    @RequestMapping(value = "/startSimulation", method = RequestMethod.POST)
    public ResponseEntity addEvent(@RequestBody String json) {
        JSONObject responseObject = new JSONObject();
        try {
            JSONObject obj = new JSONObject(json);
            String foram, environment, simulation;
            int requestedId;
            foram = obj.getJSONObject("config").getString("foram");
            environment = obj.getJSONObject("config").getString("environment");
            simulation = obj.getJSONObject("config").getString("simulation");
            requestedId = obj.getInt("simulationId");
            Integer localIdSimulation = startSimulation(requestedId, foram, simulation, environment);
            if(localIdSimulation == null){
                responseObject.put("started", false);
                responseObject.put("message", "QUEUE_OVERFLOW");
            } else {
                responseObject.put("started", true);
                responseObject.put("message", "");
                responseObject.put("localIdSimulation", localIdSimulation);
            }
        } catch (WrongPayloadException | IOException | InterruptedException e) {
            e.printStackTrace();
            responseObject.put("started", false);
            responseObject.put("message", e.getMessage());
        }
        return new ResponseEntity<String>(responseObject.toString(), HttpStatus.OK);
    }

    
    private Integer startSimulation(int requestedId, String foramConfig, String simulationConfig, String environmentConfig)
            throws IOException, InterruptedException, WrongPayloadException {
        File databaseConfigFile = new File(dbConfigFilePath);
        try(FileInputStream in = new FileInputStream(databaseConfigFile)){
            byte[] mydata = new byte[(int) databaseConfigFile.length()];
            in.read(mydata);
            fileSet.setDatabasePropertiesFile(mydata);
            if(foramConfig==null || foramConfig.equals("")) {
                throw new WrongPayloadException("Foram config not specified");
            }
            mydata = foramConfig.getBytes();
            fileSet.setForamConfigFile(mydata);
            if(simulationConfig==null || simulationConfig.equals("")) {
                throw new WrongPayloadException("Simulation config not specified");
            }
            mydata = simulationConfig.getBytes();
            fileSet.setSimulationConfigFile(mydata);
            if(environmentConfig==null || environmentConfig.equals("")) {
                throw new WrongPayloadException("Environment config not specified");
            } 
            mydata = environmentConfig.getBytes();
            fileSet.setEnvironmentConfigFile(mydata);
            return simulator.queueRequest(simulationHandler, requestedId, fileSet);
        }
    }
    
    @RequestMapping(value = "/stopSimulation", method = RequestMethod.POST)
    public ResponseEntity stopSimulation(@RequestParam("id") int id) {
        simulator.cancelRequest(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
