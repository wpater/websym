package simulationserver;

import simulationserver.scheduler.ISimulationHandler;

public interface ISimulationServer{
	public Integer queueRequest(ISimulationHandler simulationHandler, int requestedSimulationId, FileSet fileSet);
	public void cancelRequest(Integer requestId);
    public void start();
}
