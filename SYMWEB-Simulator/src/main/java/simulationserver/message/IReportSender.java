package simulationserver.message;

import org.json.JSONObject;

public interface IReportSender {
	public void sendSimulationReport(JSONObject simulationReportJSON);

	public void setConnectionEstablished(boolean established);
}
