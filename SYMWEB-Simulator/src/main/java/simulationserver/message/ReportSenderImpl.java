package simulationserver.message;

import java.util.concurrent.Semaphore;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import lombok.Setter;
import simulationserver.database.mappers.IReportMapper;
import simulationserver.model.Report;

@Service("ReportSender")
public class ReportSenderImpl implements IReportSender {

	@Autowired
	IReportMapper reportMapper;

	@Autowired
	RestTemplate restTemplate;

	@Value("${webAppIP}")
	private String webAppIP;

	private boolean connectionEstablished = false;

	private final Semaphore ioSemaphore = new Semaphore(1, true);

	@Override
	public void setConnectionEstablished(boolean established) {
		acquireSemaphore();
		if (established == connectionEstablished) {
			releaseSemaphore();
			return;
		}
		connectionEstablished = established ? flushMessages() : false;
		releaseSemaphore();
	}

	private boolean flushMessages() {
		// Callers should acquire semaphore first
		for (Report report : reportMapper.getReports()) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(report.getJSONString(), headers);
			try {
				ResponseEntity<String> response = restTemplate.exchange("https://" + webAppIP + ":8443/simulationReport",
						HttpMethod.POST, entity, String.class);
				if (response.getStatusCode() == HttpStatus.OK) {

				} else {

				}
				reportMapper.deleteReport(report.getReportId());
			} catch (RestClientException e) {
				return false;
			}
		}
		return true;
	}

	private void acquireSemaphore() {
		while (true) {
			try {
				ioSemaphore.acquire();
				break;
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void releaseSemaphore() {
		ioSemaphore.release();
	}

	@Override
	public void sendSimulationReport(JSONObject simulationReportJSON) {
		acquireSemaphore();
		if (connectionEstablished) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(simulationReportJSON.toString(), headers);
			try {
				ResponseEntity<String> response = restTemplate.exchange("https://" + webAppIP + ":8443/simulationReport",
						HttpMethod.POST, entity, String.class);
				if (response.getStatusCode() == HttpStatus.OK) {

				} else {

				}
			} catch (RestClientException e) {
				connectionEstablished = false;
				reportMapper.insertReport(new Report(simulationReportJSON.toString()));
			}
		} else {
			reportMapper.insertReport(new Report(simulationReportJSON.toString()));
		}
		releaseSemaphore();
	}

}
