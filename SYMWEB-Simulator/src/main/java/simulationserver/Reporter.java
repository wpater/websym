package simulationserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import lombok.NoArgsConstructor;
import simulationserver.message.IReportSender;

@NoArgsConstructor
@Controller
public class Reporter extends Thread {

    private final static long REPORT_SEND_DELAY = 60 * 1000; // Check if server is ready once every 60s
    
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	IReportSender reportSender;
	
	private @Value("${webAppIP}") String webAppIP;
	private @Value("${simulatorIP}") String simulatorIP;

	@RequestMapping(value = "/simulators", method = RequestMethod.POST)
	private void sendReport() {
		try {
			restTemplate.postForObject("https://" + webAppIP + ":8443/simulators", simulatorIP, String.class);
			reportSender.setConnectionEstablished(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run() {
	    while(true){
	        sendReport();
	        try {
                Thread.sleep(REPORT_SEND_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
	    }
	}
}
