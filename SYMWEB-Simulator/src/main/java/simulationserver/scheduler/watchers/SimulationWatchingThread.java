package simulationserver.scheduler.watchers;

import simulationserver.model.Simulation;
import simulationserver.scheduler.ISimulationHandler;

public class SimulationWatchingThread extends WatchingThread {

    ISimulationHandler simulationHandler;
    Simulation simulation;

    public SimulationWatchingThread(Process process, ISimulationHandler simulationHandler, Simulation simulation) {
        this.process = process;
        this.simulationHandler = simulationHandler;
        this.simulation = simulation;
    }

    @Override
    protected void onLineRead(String line) {
        simulationHandler.onSimulationOutput(simulation, line);
    }

}
