package simulationserver.scheduler.watchers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Getter;
import simulationserver.ChartType;

public class ChartGenerationWatchingThread extends WatchingThread {

    private Pattern directoryOutputPattern;
    ChartType chartType;
    @Getter
    String chartDirectory;

    public ChartGenerationWatchingThread(Process process, ChartType chartType) {
        this.process = process;
        this.chartType = chartType;
        directoryOutputPattern = Pattern.compile("^(.*)directory: (.*)$", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    }

    @Override
    protected void onLineRead(String line) {
        Matcher matcher = directoryOutputPattern.matcher(line);
        if (matcher.matches()) {
            chartDirectory = matcher.group(2);
        }
    }

}
