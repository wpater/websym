package simulationserver.scheduler.watchers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Getter;

public class LastSimulationIdWatchingThread extends WatchingThread {
    private static final Pattern LAST_ID_PATTERN = Pattern.compile("^(\\d+)(.*)$",
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);

    @Getter
    String lastId;

    public LastSimulationIdWatchingThread(Process process) {
        this.process = process;
    }

    @Override
    protected void onLineRead(String line) {
        Matcher matcher = LAST_ID_PATTERN.matcher(line);
        if (matcher.matches()) {
            lastId = matcher.group(1);
        }
    }

}
