package simulationserver.scheduler.watchers;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public abstract class WatchingThread extends Thread {

    Process process;
    boolean waitForOutput = true;

    public void run() {
        InputStream processInputStream = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(processInputStream);
        BufferedReader br = new BufferedReader(isr);
        try {
            /*
             * TODO: Find better solution Below code might seem kinda dumb but
             * if we dont check br.ready and thread hangs on readLine then if
             * the process is killed the thread just hangs. This is a temporary
             * solution, need to find more elegant way
             */
            while (waitForOutput) {
                if (br.ready()) {
                    onLineRead(br.readLine());
                } else {
                    if (!process.isAlive())
                        break;
                    Thread.sleep(1000);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopWaitingForOutput() {
        waitForOutput = false;
    }

    protected abstract void onLineRead(String line);

}
