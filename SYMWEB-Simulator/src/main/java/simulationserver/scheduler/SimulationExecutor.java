package simulationserver.scheduler;

import lombok.NoArgsConstructor;
import lombok.Setter;
import simulationserver.database.mappers.ISimulationMapper;
import simulationserver.filemanager.FileManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Predicate;

@Component("executor")
@Setter
@NoArgsConstructor
public class SimulationExecutor {
    private int QUEUE_CAPACITY = 20;

	private BlockingQueue<SimulationRequest> queue = new LinkedBlockingQueue<SimulationRequest>(QUEUE_CAPACITY);
	SimulationRequest currentlyProcessedSimulationRequest;
	@Autowired
    private SimulationManager simulationManager;
	@Autowired
	private ISimulationMapper simulationMapper;
	
	public void run() {
		while(true){
		    currentlyProcessedSimulationRequest = null;
				try {
				    currentlyProcessedSimulationRequest = queue.take();
					simulationMapper.setSimulationWasStarted(currentlyProcessedSimulationRequest.getSimulation().getSimulationId(), true);
				    simulationManager.runSimulation(currentlyProcessedSimulationRequest.getSimulation(),
					        currentlyProcessedSimulationRequest.getSimulationHandler());
					FileManager.cleanup(currentlyProcessedSimulationRequest.getSimulation().getFilesSimulationId());
					simulationMapper.deleteSimulation(currentlyProcessedSimulationRequest.getSimulation().getSimulationId());
				} catch (InterruptedException e) {
					e.printStackTrace();
					continue;
				}
        }
	}
	
    public SimulationManager getSimulationManager() {
        return simulationManager;
    }

    public boolean offer(SimulationRequest request){
        return queue.offer(request);
    }
    
    public void cancelRequest(Integer id){
        if(id == null)
            return;
        queue.removeIf(new Predicate<SimulationRequest>() {

            @Override
            public boolean test(SimulationRequest simulationRequest) {
                return id.equals(simulationRequest.getSimulation().getSimulationId());
            }
        });
        if(currentlyProcessedSimulationRequest != null && id.equals(currentlyProcessedSimulationRequest.getSimulation().getSimulationId()))
            simulationManager.stopSimulation();
    }
    
}
