package simulationserver.scheduler;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("scheduler")
@NoArgsConstructor
public class SimulationScheduler {
	@Autowired
	private SimulationExecutor executor;

	public void start() {
        executor.run();
	}

	public boolean scheduleExecution(SimulationRequest simulationRequest) {
		return executor.offer(simulationRequest);
	}
	
	public void cancelScheduledExecution(Integer id){
	    executor.cancelRequest(id);
	}
}
