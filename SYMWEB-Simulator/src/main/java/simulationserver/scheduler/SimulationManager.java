package simulationserver.scheduler;

import java.util.Formatter;
import java.util.HashMap;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

import simulationserver.ChartType;
import simulationserver.filemanager.FileManager;
import simulationserver.model.Simulation;
import simulationserver.scheduler.watchers.ChartGenerationWatchingThread;
import simulationserver.scheduler.watchers.LastSimulationIdWatchingThread;
import simulationserver.scheduler.watchers.SimulationWatchingThread;

@Service("manager")
public class SimulationManager {

    private final static String CHART_SIMULATION_GENERATION_START_FORMAT = "Generating chart type %s...";
    private final static String CHART_SIMULATION_GENERATION_END_FORMAT = "Chart type %s generated.";

    Formatter stringFormatter;
    StringBuilder formatterStringBuilder;
    public SimulationManager() {
        formatterStringBuilder = new StringBuilder();
        stringFormatter = new Formatter(formatterStringBuilder);
    }

    private Process simulationProcess;
    private Process chartGenerationProcess;
    private SimulationWatchingThread simulationWatchingThread;

    public void runSimulation(Simulation simulation, ISimulationHandler simulationHandler) {

        this.simulationProcess = simulation.startSimulation();
        simulationWatchingThread = null;
        if (simulationHandler != null) {
            simulationHandler.onSimulationStarted(simulation);
            simulationWatchingThread = new SimulationWatchingThread(simulationProcess, simulationHandler, simulation);
            simulationWatchingThread.start();
        }
        waitForProcessAndThread(simulationProcess, simulationWatchingThread);
        chartGenerationProcess = simulation.getLastSimulationId();
        LastSimulationIdWatchingThread lastSimulationIdWatchingThread = new LastSimulationIdWatchingThread(
                chartGenerationProcess);
        lastSimulationIdWatchingThread.start();
        waitForProcessAndThread(chartGenerationProcess, lastSimulationIdWatchingThread);
        HashMap<ChartType, String> chartTypeToChartDirectoryMap = new HashMap<ChartType, String>();
        if (lastSimulationIdWatchingThread.getLastId() != null) {
            for (ChartType chartType : ChartType.values()) {
                if (simulationHandler != null) {
                    simulationHandler.onSimulationOutput(simulation,
                            stringFormatter.format(CHART_SIMULATION_GENERATION_START_FORMAT, chartType).toString());
                    formatterStringBuilder.setLength(0);
                }
                chartGenerationProcess = simulation.getChartGenerationProcess(chartType,
                        lastSimulationIdWatchingThread.getLastId());
                ChartGenerationWatchingThread chartGenerationWatchingThread = new ChartGenerationWatchingThread(
                        chartGenerationProcess, chartType);
                chartGenerationWatchingThread.start();
                waitForProcessAndThread(chartGenerationProcess, chartGenerationWatchingThread);
                chartTypeToChartDirectoryMap.put(chartType, chartGenerationWatchingThread.getChartDirectory());
                if (simulationHandler != null) {
                    simulationHandler.onSimulationOutput(simulation,
                            stringFormatter.format(CHART_SIMULATION_GENERATION_END_FORMAT, chartType).toString());
                    formatterStringBuilder.setLength(0);
                }
            }
        }
        for (Entry<ChartType, String> entry : chartTypeToChartDirectoryMap.entrySet()) {
            ChartType chartType = entry.getKey();
            String directory = entry.getValue();
            if (directory == null)
                continue;
            String fileContent = FileManager.getChartFileContent(chartType, directory);
            if (fileContent == null)
                continue;
            if (simulationHandler != null)
                simulationHandler.onSimulationChartGenerated(simulation, chartType, fileContent);
        }
        if (simulationHandler != null)
            simulationHandler.onSimulationFinished(simulation);
        simulationProcess = null;
    }

    private void waitForProcessAndThread(Process process, Thread thread) {
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
            process.destroy();
        }
        if (thread != null)
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    public void stopSimulation() {
        if (simulationProcess != null && simulationProcess.isAlive()) {
            if (simulationWatchingThread != null)
                simulationWatchingThread.stopWaitingForOutput();
            simulationProcess.destroyForcibly();
        }
    }

}
