package simulationserver.scheduler;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISimulationManager  extends Remote {
    public String getOutput() throws RemoteException;
}
