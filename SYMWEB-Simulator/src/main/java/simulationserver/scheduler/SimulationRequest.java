package simulationserver.scheduler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import simulationserver.model.Simulation;

@Getter
@AllArgsConstructor
public class SimulationRequest {

	private Simulation simulation;
	private ISimulationHandler simulationHandler;
}
