package simulationserver.scheduler;

import simulationserver.ChartType;
import simulationserver.model.Simulation;

public interface ISimulationHandler {

    public void onSimulationStarted(Simulation simulation);

    public void onSimulationFinished(Simulation simulation);

    public void onSimulationError(Simulation simulation, String message);

    public void onSimulationOutput(Simulation simulation, String message);

    public void onSimulationChartGenerated(Simulation simulation, ChartType chartType, String fileContent);

}
