package simulationserver.scheduler;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import simulationserver.ChartType;
import simulationserver.message.IReportSender;
import simulationserver.model.Simulation;

@Component
@RequiredArgsConstructor
public class SimulationHandlerImpl implements ISimulationHandler {

    @Value("${webAppIP}")
    private String webAppIP;

    @Value("${simulatorIP}")
    private String simulatorIP;

    @Autowired
    IReportSender reportSender;

    @Override
    public void onSimulationFinished(Simulation simulation) {
        sendReport(simulation, "DONE", null);
    }

    @Override
    public void onSimulationError(Simulation simulation, String message) {
        sendReport(simulation, "ERROR", message);
    }

    @Override
    public void onSimulationOutput(Simulation simulation, String message) {
        sendReport(simulation, "OUTPUT", message);
    }

    @Override
    public void onSimulationStarted(Simulation simulation) {
        sendReport(simulation, "STARTED", null);
    }

    @Override
    public void onSimulationChartGenerated(Simulation simulation, ChartType chartType, String fileContent) {
        sendReport(simulation, "CHART_GENERATED", chartType.toString(), fileContent);
    }

    private void sendReport(Simulation simulation, String reportType, String message) {
        sendReport(simulation, reportType, message, null);
    }

    private void sendReport(Simulation simulation, String reportType, String message, String fileContent) {
        JSONObject request = new JSONObject();
        request.put("idSimulation", simulation.getRequestedSimulationId());
        request.put("reportType", reportType);
        request.put("message", message);
        request.put("fileContent", fileContent);
        request.put("simulatorIP", simulatorIP + ":8181");
        reportSender.sendSimulationReport(request);
    }

}
