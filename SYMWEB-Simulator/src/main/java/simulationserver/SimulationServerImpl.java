package simulationserver;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Service;

import helpers.PropertiesLoader;
import lombok.NoArgsConstructor;
import simulationserver.database.mappers.ISimulationMapper;
import simulationserver.filemanager.FileManager;
import simulationserver.model.Simulation;
import simulationserver.scheduler.ISimulationHandler;
import simulationserver.scheduler.SimulationRequest;
import simulationserver.scheduler.SimulationScheduler;

@Configuration
@Service("simulator")
@PropertySource("classpath:server.properties")
@NoArgsConstructor
public class SimulationServerImpl implements ISimulationServer {

    private @Value("${configFilePath}") String configFileName;

    @Autowired
    private SimulationScheduler scheduler;

    @Autowired
    private ISimulationMapper simulationMapper;
    
    @Autowired
    ISimulationHandler simulationHandler;

    Properties properties;

    public void start() {
        properties = PropertiesLoader.loadProperties(configFileName);
        Simulation.setEvolutusScriptPath(properties.getProperty("scriptPath"));
        loadSimulations();
        scheduler.start();
    }

    private void loadSimulations() {
        for (Simulation simulation : simulationMapper.getSimulations()) {
            if (simulation.isWasStarted()) {
                simulationHandler.onSimulationError(simulation, "Server was restarted during simulation.");
                FileManager.cleanup(simulation.getFilesSimulationId());
                simulationMapper.deleteSimulation(simulation.getSimulationId());
            } else {
                if (!scheduler.scheduleExecution(new SimulationRequest(simulation, simulationHandler))) {
                    throw new RuntimeException(
                            "Scheduled simulation cannot be queued, as the queue is full. Increase queue size or manually remove simulations entries.");
                }
                ;
            }
        }

    }

    @Override
    public Integer queueRequest(ISimulationHandler simulationHandler, int requestedSimulationId, FileSet fileSet) {
        Integer result = FileManager.persistFiles(fileSet);
        if (result != null) {
            System.out.println("Files downloaded " + FileManager.getDatabaseConfigFilePath(result) + " "
                    + FileManager.getConfigDirectoryPath(result));
            Simulation simulation = new Simulation(result, requestedSimulationId);
            simulationMapper.insertSimulation(simulation);
            boolean scheduled = scheduler.scheduleExecution(new SimulationRequest(simulation, simulationHandler));
            if (scheduled)
                return simulation.getSimulationId();
            else {
                FileManager.cleanup(result);
                simulationMapper.deleteSimulation(simulation.getSimulationId());
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public void cancelRequest(Integer requestId) {
        scheduler.cancelScheduledExecution(requestId);
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
