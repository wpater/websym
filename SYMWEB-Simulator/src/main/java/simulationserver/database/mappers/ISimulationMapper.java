package simulationserver.database.mappers;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import simulationserver.model.Simulation;

public interface ISimulationMapper {
	public void ensureTable();
	public void insertSimulation(Simulation simulation);
	public void deleteSimulation(@Param("simulationId") int simulationId);
	public void setSimulationWasStarted(@Param("simulationId") int simulationId, @Param("wasStarted") boolean wasStarted);
	public ArrayList<Simulation> getSimulations();
}
