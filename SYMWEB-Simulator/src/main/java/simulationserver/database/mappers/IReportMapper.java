package simulationserver.database.mappers;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import simulationserver.model.Report;

public interface IReportMapper {
	public void ensureTable();
	public void insertReport(Report report);
	public ArrayList<Report> getReports();
	public void deleteReport(@Param("reportId") int reportId);
}
