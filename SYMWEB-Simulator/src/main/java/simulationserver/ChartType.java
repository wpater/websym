package simulationserver;

import java.util.regex.Pattern;

import lombok.Getter;

@Getter
public enum ChartType {
    POPULATION_CHART("^populationChart-.*\\.html$", "population-chart"),
    BORN_DEAD_CHART("^borndeadChart-.*\\.html$", "energy-chart"), 
    ENERGY_CHART("^energyChart-.*\\.html$", "born-dead-chart");

    private Pattern chartFilePattern;
    private String scriptTypeName;

    ChartType(String patternString, String scriptTypeName) {
        chartFilePattern = Pattern.compile(patternString);
        this.scriptTypeName = scriptTypeName;
    }

}
