package simulationserver.model;

import java.io.IOException;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import simulationserver.ChartType;
import simulationserver.filemanager.FileManager;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Simulation {

    public Simulation(int filesSimulationId, int requestedSimulationId) {
        this.filesSimulationId = filesSimulationId;
        this.requestedSimulationId = requestedSimulationId;
    }

    private static String evolutusScriptPath;

    public static void setEvolutusScriptPath(String evolutusScriptPath) {
        Simulation.evolutusScriptPath = evolutusScriptPath;
    }

    private int simulationId;
    private int filesSimulationId;
    private int requestedSimulationId;
    private boolean wasStarted = false;

    public Process startSimulation() {
        try {
            return new ProcessBuilder(evolutusScriptPath, "sim", FileManager.getDatabaseConfigFilePath(filesSimulationId), FileManager.getConfigDirectoryPath(filesSimulationId)).start();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Process getLastSimulationId() {
        try {
            return new ProcessBuilder(evolutusScriptPath, "list", FileManager.getDatabaseConfigFilePath(filesSimulationId)).start();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Process getChartGenerationProcess(ChartType chartType, String simulationId) {
        try {
            return new ProcessBuilder(evolutusScriptPath, "gen", chartType.getScriptTypeName(),
                    FileManager.getDatabaseConfigFilePath(filesSimulationId), FileManager.getResultDirectoryPath(filesSimulationId), simulationId).start();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
