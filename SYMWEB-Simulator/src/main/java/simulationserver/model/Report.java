package simulationserver.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Report {
	
	public Report(String JSONString){
		this.JSONString = JSONString;
	}
	
	private int reportId;
	private String JSONString;
	
}
