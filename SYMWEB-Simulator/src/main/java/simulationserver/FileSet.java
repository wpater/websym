package simulationserver;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class FileSet  {
	
	byte[] databasePropertiesFile;
	
	public void setDatabasePropertiesFile(byte[] databasePropertiesFile){
		this.databasePropertiesFile = databasePropertiesFile;
	}
	@Bean
	public byte[] getDatabasePropertiesFile(){
		return databasePropertiesFile;
	}
	
	int environmentConfigFileLength;
	byte[] environmentConfigFile;
	
	public void setEnvironmentConfigFile(byte[] environmentConfigFile){
		this.environmentConfigFile = environmentConfigFile;
	}
	@Bean
	public byte[] getEnvironmentConfigFile(){
		return environmentConfigFile;
	}
	byte[] foramConfigFile;
	
	public void setForamConfigFile(byte[] foramConfigFile){
		this.foramConfigFile = foramConfigFile;
	}
	@Bean
	public byte[] getForamConfigFile(){
		return foramConfigFile;
	}
	
	byte[] simulationConfigFile;
	
	public void setSimulationConfigFile(byte[] simulationConfigFile){
		this.simulationConfigFile = simulationConfigFile;
	}
	@Bean
	public byte[] getSimulationConfigFile(){
		return simulationConfigFile;
	}
}
