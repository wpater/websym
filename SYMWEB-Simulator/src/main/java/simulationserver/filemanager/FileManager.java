package simulationserver.filemanager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import lombok.NoArgsConstructor;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import simulationserver.ChartType;
import simulationserver.FileSet;

@Component("fileManager")
@NoArgsConstructor
public class FileManager {

    private static final String BASE_DIRECTORY = Paths.get("").toAbsolutePath().toString() + File.separatorChar
            + "configurations";

    private static AtomicInteger counter = new AtomicInteger(0);

    public static Integer persistFiles(FileSet fileSet) {
        int id = counter.getAndIncrement();

        HashMap<FileType, byte[]> map = new HashMap<FileType, byte[]>();
        map.put(FileType.DATABASE_CONFIG, fileSet.getDatabasePropertiesFile());
        map.put(FileType.ENVIRONMENT_CONFIG, fileSet.getEnvironmentConfigFile());
        map.put(FileType.FORAM_CONFIG, fileSet.getForamConfigFile());
        map.put(FileType.SIMULATION_CONFIG, fileSet.getSimulationConfigFile());

        File directory = new File(getDirectoryPath(id));
        File configDirectory = new File(getConfigDirectoryPath(id));
        File resultDirectory = new File(getResultDirectoryPath(id));
        while (directory.exists() || configDirectory.exists() || resultDirectory.exists()) {
            id = counter.getAndIncrement();
            directory = new File(getDirectoryPath(id));
            configDirectory = new File(getConfigDirectoryPath(id));
            resultDirectory = new File(getResultDirectoryPath(id));
        }
        if (!directory.mkdirs() || !configDirectory.mkdirs() || !resultDirectory.mkdirs()) {
            cleanup(id);
            System.out.println("dirs not created");
            return null;
        }

        for (Map.Entry<FileType, byte[]> entry : map.entrySet()) {
            FileType fileType = entry.getKey();
            byte[] content = entry.getValue();
            if (content != null && content.length > 0) {
                try {
                    File file = new File(
                            (fileType.getPutInConfigDirectory() ? getConfigDirectoryPath(id) : getDirectoryPath(id))
                                    + File.separatorChar + fileType.getFileName());
                    if (!file.createNewFile()) {
                        cleanup(id);
                        return null;
                    }
                    FileOutputStream out = new FileOutputStream(file, false);
                    out.write(content, 0, content.length);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    cleanup(id);
                    return null;

                }
            }
        }
        return id;
    }

    public static void cleanup(int id) {
        try {
            FileUtils.deleteDirectory(new File(getDirectoryPath(id)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getDirectoryPath(int id) {
        return BASE_DIRECTORY + File.separatorChar + id;
    }

    public static String getConfigDirectoryPath(int id) {
        return getDirectoryPath(id) + File.separatorChar + "config";
    }

    public static String getDatabaseConfigFilePath(int id) {
        return getDirectoryPath(id) + File.separatorChar + FileType.DATABASE_CONFIG.getFileName();
    }

    public static String getResultDirectoryPath(int id) {
        return getDirectoryPath(id) + File.separatorChar + "result";
    }

    public static String getChartFileContent(ChartType chartType, String directoryPath) {
        File chartsDirectory = new File(directoryPath + File.separator + "charts");
        File[] files = chartsDirectory.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return chartType.getChartFilePattern().matcher(name).matches();
            }
        });
        if (files == null || files.length != 1)
            return null;
        File file = files[0];
        try {
            return FileUtils.readFileToString(file, Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
