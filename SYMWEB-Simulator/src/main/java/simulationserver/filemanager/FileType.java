package simulationserver.filemanager;

public enum FileType {
	DATABASE_CONFIG {
		@Override
		public String getFileName() {
			return "db.properties";
		}

		@Override
		public boolean getPutInConfigDirectory() {
			return false;
		}
	}, SIMULATION_CONFIG {
		@Override
		public String getFileName() {
			return "simulation_config.js";
		}

		@Override
		public boolean getPutInConfigDirectory() {
			return true;
		}
	}, FORAM_CONFIG {
		@Override
		public String getFileName() {
			return "foram_config.js";
		}

		@Override
		public boolean getPutInConfigDirectory() {
			return true;
		}
	}, ENVIRONMENT_CONFIG {
		@Override
		public String getFileName() {
			return "environment_config.js";
		}

		@Override
		public boolean getPutInConfigDirectory() {
			return true;
		}
	};
	
	public abstract String getFileName();
	public abstract boolean getPutInConfigDirectory();
}
