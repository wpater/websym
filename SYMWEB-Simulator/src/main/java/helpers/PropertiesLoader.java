package helpers;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Component("propertiesLoader")
@NoArgsConstructor
public class PropertiesLoader {

	public static Properties loadProperties(String configFileName) {
		InputStream input = null;
		try {
			Properties prop = new Properties();
			input = new FileInputStream(configFileName);
			prop.load(input);
			return prop;
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
