#!/bin/bash
 
# Before this steps database should be created.
# For create database run 'sqlite3 DATABASE_NAME'
# Then in sqlite shell run '.read path_to_websym_dir/SYMWEB-Server/src/main/resources/setup.sql'
# If you are in SYMWEB-Server dir you can run '.read src/main/resources/setup.sql'
 
# Build module, you can remove last option for test your build
mvn clean install -DskipTests
echo "Module is ready to install"
# Change name to ROOT.war - web app will be deployed under 'address:port/'
mv target/*.war target/ROOT.war
echo "Name of war file is changed to ROOT"
# This is default path to tomcat8 dir, if you installed tomcat manually change it
cp target/ROOT.war /var/lib/tomcat8/webapps
echo "File is copied to tomcat dir"
# This is step is not needed if tomcat8 has set auto-deploy option to true (by default it is)
service tomcat8 restart
echo "tomcat service is restarted"
