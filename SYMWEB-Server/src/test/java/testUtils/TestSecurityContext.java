package testUtils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;

public class TestSecurityContext implements SecurityContext {

    String username;
    boolean isAdmin;
    Authentication authentication;

    public TestSecurityContext(String username, boolean isAdmin) {
        this.username = username;
        this.isAdmin = isAdmin;
        authentication = getDefaultTestAuthentication();
    }

    private Authentication getDefaultTestAuthentication() {
        return new Authentication() {

            List<GrantedAuthority> authorities;
            boolean authenticated = true;

            @Override
            public String getName() {
                return username;
            }

            @Override
            public void setAuthenticated(boolean arg0) throws IllegalArgumentException {
                this.authenticated = arg0;

            }

            @Override
            public boolean isAuthenticated() {
                return authenticated;
            }

            @Override
            public Object getPrincipal() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                if (authorities == null) {
                    authorities = new LinkedList<GrantedAuthority>();
                    GrantedAuthority userAuthority = new GrantedAuthority() {

                        @Override
                        public String getAuthority() {
                            return "ROLE_USER";
                        }
                    };
                    authorities.add(userAuthority);
                    if(isAdmin){
                        GrantedAuthority adminAuthority = new GrantedAuthority() {

                            @Override
                            public String getAuthority() {
                                return "ROLE_ADMIN";
                            }
                        }; 
                        authorities.add(adminAuthority);
                    }
                }

                return authorities;
            }
        };
    }

    @Override
    public Authentication getAuthentication() {
        return authentication;
    }

    @Override
    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;

    }

}
