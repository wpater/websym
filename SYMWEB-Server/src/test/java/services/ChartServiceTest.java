package services;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import app.charts.ChartType;
import app.exceptions.UnauthorizedException;
import app.model.Chart;
import app.model.Comment;
import app.model.Config;
import app.model.Simulation;
import app.model.User;
import app.simulator.ChartHandlerImpl;
import app.simulator.CommentHandlerImpl;
import app.simulator.IFileHandler;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import testUtils.TestSecurityContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = app.spring.WebConfig.class)
@WebAppConfiguration
public class ChartServiceTest {

    private static User user;

    @Mock
    private ISimulationMapper simMapper;
    
    @Mock
    private IFileHandler fileHandlerMock;
    
    @InjectMocks
    private ChartHandlerImpl chartsHandlerImpl;

    @BeforeClass
    public static void initSuite() {
        user = new User();
        user.setEmail("test@test.com");
        user.setUsername("username_test_chart_service");
        user.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        reset(simMapper);
    }

    @After
    public void cleanupTestCase() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void tc001AddChart() throws Exception {
        ChartType chartType = ChartType.BORN_DEAD_CHART; 
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
              Object[] args = invocation.getArguments();
              Chart chart = (Chart) args[0];
              chart.setIdChart(1);
              Assert.assertNotEquals(chart.getCreationTimestamp(), 0);
              Assert.assertEquals(chart.getIdSimulation(), 1);
              Assert.assertEquals(chart.getChartType(), chartType);
              return null;
            }
        }).when(simMapper).insertChart(any(Chart.class));
        int id = chartsHandlerImpl.createChart(1, chartType, "");
        verify(simMapper, times(1)).insertChart(any(Chart.class));
        verifyNoMoreInteractions(simMapper);
        verify(fileHandlerMock, times(1)).saveChartContent(1, chartType, "");
        verifyNoMoreInteractions(fileHandlerMock);
    }

    @Test
    public void tc002GetChart() throws Exception {
        Chart chart = new Chart();
        when(simMapper.getChart(1)).thenReturn(chart);
        Chart chart2 = chartsHandlerImpl.getChart(1);
        Assert.assertEquals(chart, chart2);
        verify(simMapper, times(1)).getChart(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(fileHandlerMock);
    }

    @Test
    public void tc003GetChartsForSimulation() throws Exception {
        ArrayList<Chart> chartList = new ArrayList<Chart>();
        when(simMapper.getChartsForSimulation(1)).thenReturn(chartList);
        ArrayList<Chart> actualChartList = chartsHandlerImpl.getChartsForSimulation(1);
        Assert.assertEquals(actualChartList, chartList);
        verify(simMapper, times(1)).getChartsForSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(fileHandlerMock);
    }
}
