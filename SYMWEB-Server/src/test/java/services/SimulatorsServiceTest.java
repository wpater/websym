package services;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import app.exceptions.WrongSimulationReportPayloadException;
import app.model.Simulation;
import app.model.SimulationReport;
import app.model.User;
import app.simulator.FileHandlerImpl;
import app.simulator.SimulatorsHandlerImpl;
import app.spring.database.ISimulationMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = app.spring.WebConfig.class)
@WebAppConfiguration
public class SimulatorsServiceTest {
    private static User user;

    @InjectMocks
    private SimulatorsHandlerImpl simulatorHandler;

    @Mock
    private ISimulationMapper simMapper;

    @Mock
    private FileHandlerImpl fileHandlerImplMock;

    @BeforeClass
    public static void initSuite() {
        java.util.Date date = new java.util.Date();
        user = new User();
        user.setEmail("test@test.com");
        user.setUsername("username_test_simulators_service" + date.getTime());
        user.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        reset(simMapper);
        reset(fileHandlerImplMock);
    }

    @After
    public void cleanupTestCase() {
    }

    @Test(expected = WrongSimulationReportPayloadException.class)
    public void tc001SimulationReportWrongPayload() throws Exception {
        simulatorHandler.handleReport(new SimulationReport(1, "", "", "", null));
    }

    @Test
    public void tc002SimulationReportOutput() throws Exception {
        String message = "message";
        String address = "localhost:8080";
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, "", 1, 1, address));
        simulatorHandler.handleReport(new SimulationReport(1, address, "OUTPUT", message, null));
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(fileHandlerImplMock, times(1)).appendSimulationOutput(1, message);
        verifyNoMoreInteractions(fileHandlerImplMock);
    }

}
