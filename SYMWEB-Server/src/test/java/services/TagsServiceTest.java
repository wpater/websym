package services;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import app.model.ConfigTag;
import app.model.SimulationTag;
import app.model.Tag;
import app.simulator.TagHandlerImpl;
import app.spring.database.ISimulationMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = app.spring.WebConfig.class)
@WebAppConfiguration
public class TagsServiceTest {

    @Mock
    private ISimulationMapper simMapper;

    @InjectMocks
    private TagHandlerImpl tagHandlerImpl;

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        reset(simMapper);
    }
    @Test
    public void tc001AddConfigTags() throws Exception {
        tagHandlerImpl.createConfigTags(1, "test1;test2");
        verify(simMapper, times(3)).insertConfigTag(any(ConfigTag.class));
        verify(simMapper, times(3)).insertTag(any(Tag.class));
        verifyNoMoreInteractions(simMapper);
    }
    
    @Test
    public void tc002AddSimulationTags() throws Exception {
        tagHandlerImpl.createSimulationTags(1, "test1;test2");
        verify(simMapper, times(4)).insertSimulationTag(any(SimulationTag.class));
        verify(simMapper, times(4)).insertTag(any(Tag.class));
        verifyNoMoreInteractions(simMapper);
    }
}
