package services;

import app.exceptions.EmailExistsException;
import app.exceptions.UsernameExistsException;
import app.model.User;
import app.model.dataTransferObjects.UserDto;
import app.spring.database.IUserMapper;
import app.svc.web.user.IRegisterService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=app.spring.WebConfig.class)
@WebAppConfiguration
public class RegisterServiceTest {
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    private MockMvc mockMvc;
    private static UserDto user;

    @Autowired
    private IRegisterService registerService;
    @Autowired
    private IUserMapper userMapper;

    @BeforeClass
    public static void initSuite(){
        java.util.Date date= new java.util.Date();
        user = new UserDto();
        user.setEmail("testregiserservice@test.com");
        user.setUsername("register_service"+date.getTime());
        user.setPassword("password_test");
        user.setMatchingPassword(user.getMatchingPassword());
    }

    @Before
    public void initTestCase() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void tc001_add_user_to_database() throws EmailExistsException, UsernameExistsException{
        User user2 = registerService.register(user);
        Assert.assertNotEquals(0, userMapper.getIdUser(user.getUsername()));
        Assert.assertEquals(user.getUsername(), userMapper.selectByName(user.getUsername()).getUsername());
        Assert.assertTrue(passwordEncoder.matches(user.getPassword(), userMapper.selectByName(user.getUsername()).getPassword()));
        Assert.assertEquals(user.getEmail(), userMapper.selectByName(user.getUsername()).getEmail());
        //cleanup
        registerService.deleteUser(user2);
    }

    @Test(expected = org.apache.ibatis.binding.BindingException.class)
    public void tc002_remove_user_from_database() throws EmailExistsException, UsernameExistsException{
        User user2 = registerService.register(user);
        registerService.deleteUser(user2);
        Assert.assertNull(userMapper.selectByName(user.getUsername()));
        userMapper.getIdUser(user.getUsername());
    }
}
