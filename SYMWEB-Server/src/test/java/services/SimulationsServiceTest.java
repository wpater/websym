package services;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import app.exceptions.UnauthorizedException;
import app.model.Config;
import app.model.Simulation;
import app.model.Simulation.SimulationStatus;
import app.model.Tag;
import app.model.User;
import app.simulator.ConfigHandlerImpl;
import app.simulator.FileHandlerImpl;
import app.simulator.SimulationHandlerImpl;
import app.simulator.TagHandlerImpl;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import testUtils.TestSecurityContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = app.spring.WebConfig.class)
@WebAppConfiguration
public class SimulationsServiceTest {
    private static User user;

    @InjectMocks
    private SimulationHandlerImpl simulationHandler;

    @Mock
    private ISimulationMapper simMapper;

    @Mock
    private IUserMapper userMapper;

    @Mock
    private FileHandlerImpl fileHandlerImplMock;

    @Mock
    private TagHandlerImpl tagHandlerImplMock;
    
    @Mock
    private ConfigHandlerImpl configHandlerImplMock;

    @BeforeClass
    public static void initSuite() {
        java.util.Date date = new java.util.Date();
        user = new User();
        user.setEmail("test@test.com");
        user.setUsername("username_test_simulations_service" + date.getTime());
        user.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        reset(simMapper);
        reset(userMapper);
        reset(fileHandlerImplMock);
        reset(configHandlerImplMock);
        reset(tagHandlerImplMock);
    }

    @After
    public void cleanupTestCase() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void tc001GetSimulationsOngoingEmpty() throws UnauthorizedException {
        SimulationStatus[] statuses = Arrays.stream(SimulationStatus.values()).filter(x -> x.getCanStop()).toArray(SimulationStatus[]::new);
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(simMapper.getUserSimulationsWithStatus(1, statuses)).thenReturn(new ArrayList<Simulation>());
        ArrayList<Simulation> list = simulationHandler.getSimulations(1, true);
        Assert.assertNotNull(list);
        Assert.assertEquals(list.size(), 0);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getUserSimulationsWithStatus(1, statuses);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandlerImplMock);
    }
    
    @Test
    public void tc002GetSimulationsFinishedEmpty() throws UnauthorizedException {
        SimulationStatus[] statuses = Arrays.stream(SimulationStatus.values()).filter(x -> !x.getCanStop()).toArray(SimulationStatus[]::new);
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(simMapper.getUserSimulationsWithStatus(1, statuses)).thenReturn(new ArrayList<Simulation>());
        ArrayList<Simulation> list = simulationHandler.getSimulations(1, false);
        Assert.assertNotNull(list);
        Assert.assertEquals(list.size(), 0);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getUserSimulationsWithStatus(1, statuses);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandlerImplMock);
    }

    @Test
    public void tc003GetSimulationsOngoingNonEmpty() throws UnauthorizedException {
        SimulationStatus[] statuses = Arrays.stream(SimulationStatus.values()).filter(x -> x.getCanStop()).toArray(SimulationStatus[]::new);
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        ArrayList<Simulation> mockSimulationsList = new ArrayList<Simulation>();
        Simulation simulation1 = new Simulation(1, ".", 1, 1, "");
        Simulation simulation2 = new Simulation(2, ".", 1, 2, "");
        mockSimulationsList.add(simulation1);
        mockSimulationsList.add(simulation2);
        ArrayList<Tag> configMockTagsList1 = new ArrayList<Tag>();
        configMockTagsList1.add(new Tag(1, "tag1"));
        configMockTagsList1.add(new Tag(2, "tag2"));
        simulation1.setConfigTagsCollection(configMockTagsList1);
        ArrayList<Tag> configMockTagsList2 = new ArrayList<Tag>();
        configMockTagsList2.add(new Tag(3, "tag3"));
        configMockTagsList2.add(new Tag(4, "tag4"));
        simulation2.setConfigTagsCollection(configMockTagsList2);
        when(simMapper.getUserSimulationsWithStatus(1, statuses)).thenReturn(mockSimulationsList);
        ArrayList<Simulation> simulationsWithTagsList = simulationHandler.getSimulations(1, true);
        Assert.assertEquals(simulationsWithTagsList.size(), 2);
        Assert.assertEquals(simulationsWithTagsList.get(0).getIdSimulation(), 1);
        Assert.assertEquals(simulationsWithTagsList.get(0).getIdConfig(), 1);
        Assert.assertEquals(simulationsWithTagsList.get(0).getPathToResult(), ".");
        Assert.assertEquals(simulationsWithTagsList.get(0).getIdUser(), 1);
        Assert.assertTrue(simulationsWithTagsList.get(0).getTagsString().contains("tag1"));
        Assert.assertTrue(simulationsWithTagsList.get(0).getTagsString().contains("tag2"));
        Assert.assertEquals(simulationsWithTagsList.get(1).getIdSimulation(), 2);
        Assert.assertEquals(simulationsWithTagsList.get(1).getIdConfig(), 2);
        Assert.assertEquals(simulationsWithTagsList.get(1).getPathToResult(), ".");
        Assert.assertEquals(simulationsWithTagsList.get(1).getIdUser(), 1);
        Assert.assertTrue(simulationsWithTagsList.get(1).getTagsString().contains("tag3"));
        Assert.assertTrue(simulationsWithTagsList.get(1).getTagsString().contains("tag4"));
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getUserSimulationsWithStatus(1, statuses);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandlerImplMock);
    }
    
    @Test
    public void tc004GetSimulationsFinishedNonEmpty() throws UnauthorizedException {
        SimulationStatus[] statuses = Arrays.stream(SimulationStatus.values()).filter(x -> !x.getCanStop()).toArray(SimulationStatus[]::new);
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        ArrayList<Simulation> mockSimulationsList = new ArrayList<Simulation>();
        Simulation simulation1 = new Simulation(1, ".", 1, 1, "");
        Simulation simulation2 = new Simulation(2, ".", 1, 2, "");
        mockSimulationsList.add(simulation1);
        mockSimulationsList.add(simulation2);
        ArrayList<Tag> configMockTagsList1 = new ArrayList<Tag>();
        configMockTagsList1.add(new Tag(1, "tag1"));
        configMockTagsList1.add(new Tag(2, "tag2"));
        simulation1.setConfigTagsCollection(configMockTagsList1);
        ArrayList<Tag> configMockTagsList2 = new ArrayList<Tag>();
        configMockTagsList2.add(new Tag(3, "tag3"));
        configMockTagsList2.add(new Tag(4, "tag4"));
        simulation2.setConfigTagsCollection(configMockTagsList2);
        when(simMapper.getUserSimulationsWithStatus(1, statuses)).thenReturn(mockSimulationsList);
        ArrayList<Simulation> simulationsWithTagsList = simulationHandler.getSimulations(1, false);
        Assert.assertEquals(simulationsWithTagsList.size(), 2);
        Assert.assertEquals(simulationsWithTagsList.get(0).getIdSimulation(), 1);
        Assert.assertEquals(simulationsWithTagsList.get(0).getIdConfig(), 1);
        Assert.assertEquals(simulationsWithTagsList.get(0).getPathToResult(), ".");
        Assert.assertEquals(simulationsWithTagsList.get(0).getIdUser(), 1);
        Assert.assertTrue(simulationsWithTagsList.get(0).getTagsString().contains("tag1"));
        Assert.assertTrue(simulationsWithTagsList.get(0).getTagsString().contains("tag2"));
        Assert.assertEquals(simulationsWithTagsList.get(1).getIdSimulation(), 2);
        Assert.assertEquals(simulationsWithTagsList.get(1).getIdConfig(), 2);
        Assert.assertEquals(simulationsWithTagsList.get(1).getPathToResult(), ".");
        Assert.assertEquals(simulationsWithTagsList.get(1).getIdUser(), 1);
        Assert.assertTrue(simulationsWithTagsList.get(1).getTagsString().contains("tag3"));
        Assert.assertTrue(simulationsWithTagsList.get(1).getTagsString().contains("tag4"));
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getUserSimulationsWithStatus(1, statuses);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandlerImplMock);
    }

    @Test
    public void tc005AddSimulation() throws UnauthorizedException {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(fileHandlerImplMock.getSimulationResultDirPath(user.getUsername(), 1)).thenReturn(".");
        when(fileHandlerImplMock.createPathToResultDirectory(user.getUsername(), 1)).thenReturn(true);
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
              Object[] args = invocation.getArguments();
              Simulation simulation = (Simulation) args[0];
              simulation.setIdSimulation(1);
              return null;
            }
        }).when(simMapper).insertSimulation(any(Simulation.class));
        String tagsString = "tag1;tag2";
        int result = simulationHandler.createSimulation(new Config(), "", tagsString);
        Assert.assertEquals(result, 1);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).insertSimulation(any(Simulation.class));
        verify(simMapper, times(1)).updateSimulation(any(Simulation.class));
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(configHandlerImplMock);
        verify(fileHandlerImplMock, times(1)).getSimulationResultDirPath(user.getUsername(), 1);
        verify(fileHandlerImplMock, times(1)).createPathToResultDirectory(user.getUsername(), 1);
        verifyNoMoreInteractions(fileHandlerImplMock);
        verify(tagHandlerImplMock, times(1)).createSimulationTags(1, tagsString);
        verifyNoMoreInteractions(tagHandlerImplMock);
    }

    @Test
    public void tc006GetSimulation() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        Simulation simulation = new Simulation(1, "", 1, 1, "");
        simulation.setProgressPercentage(50);
        when(simMapper.getSimulation(1)).thenReturn(simulation);
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        Simulation simulation2 = simulationHandler.getSimulationWithId(1);
        Assert.assertEquals(simulation, simulation2);
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verifyNoMoreInteractions(tagHandlerImplMock);
        
    }
    
    @Test
    public void tc007DeleteSimulation() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        Simulation simulation = new Simulation(1, "", 1, 1, "");
        when(simMapper.getSimulation(1)).thenReturn(simulation);
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(userMapper.selectById(1)).thenReturn(user);
        simulationHandler.deleteSimulation(1);
        verify(simMapper, times(1)).getSimulation(1);
        verify(simMapper, times(1)).deleteSimulation(1);
        verify(simMapper, times(1)).deleteSimulationComments(1);
        verify(simMapper, times(1)).deleteChartsForSimulation(1);
        verify(simMapper, times(1)).deleteSimulationTag(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verify(userMapper,times(1)).selectById(1);
        verifyNoMoreInteractions(userMapper);
        verify(fileHandlerImplMock, times(1)).deleteSimulationDirectory(user.getUsername(), 1);
        verifyNoMoreInteractions(fileHandlerImplMock);
        verifyNoMoreInteractions(tagHandlerImplMock);
    }
    
    @Test(expected = UnauthorizedException.class)
    public void tc008DeleteNonSimulation() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        Simulation simulation = new Simulation(1, "", 2, 1, "");
        when(simMapper.getSimulation(1)).thenReturn(simulation);
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(userMapper.selectById(1)).thenReturn(user);
        simulationHandler.deleteSimulation(1);
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verifyNoMoreInteractions(fileHandlerImplMock);
        verifyNoMoreInteractions(tagHandlerImplMock);
    }
    
    @Test
    public void tc009DeleteSimulationAdmin() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), true));
        Simulation simulation = new Simulation(1, "", 2, 1, "");
        when(simMapper.getSimulation(1)).thenReturn(simulation);
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        User user2 = new User(2, true, "", "", "username2");
        when(userMapper.selectById(simulation.getIdUser())).thenReturn(user2);
        simulationHandler.deleteSimulation(1);
        verify(simMapper, times(1)).getSimulation(1);
        verify(simMapper, times(1)).deleteSimulation(1);
        verify(simMapper, times(1)).deleteSimulationComments(1);
        verify(simMapper, times(1)).deleteChartsForSimulation(1);
        verify(simMapper, times(1)).deleteSimulationTag(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verify(userMapper,times(1)).selectById(2);
        verifyNoMoreInteractions(userMapper);
        verify(fileHandlerImplMock, times(1)).deleteSimulationDirectory(user2.getUsername(), 1);
        verifyNoMoreInteractions(fileHandlerImplMock);
        verifyNoMoreInteractions(tagHandlerImplMock);
    }

}
