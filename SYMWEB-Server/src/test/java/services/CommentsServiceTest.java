package services;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import app.exceptions.UnauthorizedException;
import app.model.Comment;
import app.model.Simulation;
import app.model.User;
import app.simulator.CommentHandlerImpl;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import testUtils.TestSecurityContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = app.spring.WebConfig.class)
@WebAppConfiguration
public class CommentsServiceTest {

    private static User user;

    @Mock
    private ISimulationMapper simMapper;

    @Mock
    private IUserMapper userMapper;

    @InjectMocks
    private CommentHandlerImpl commentHandlerImpl;

    @BeforeClass
    public static void initSuite() {
        user = new User();
        user.setEmail("test@test.com");
        user.setUsername("username_test_comments_service");
        user.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        reset(simMapper);
        reset(userMapper);
    }

    @After
    public void cleanupTestCase() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void tc001AddCommentOwner() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, "", 1, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        commentHandlerImpl.addComment(1, "comment");
        verify(simMapper, times(1)).getSimulation(1);
        verify(simMapper, times(1)).insertComment(any(Comment.class));
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
    }

    @Test(expected = UnauthorizedException.class)
    public void tc002AddCommentNotOwner() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, "", 1, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(2);
        commentHandlerImpl.addComment(1, "comment");
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
    }

    @Test
    public void tc003AddCommentNotOwnerButAdmin() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), true));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, "", 1, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(2);
        commentHandlerImpl.addComment(1, "comment");
        verify(simMapper, times(1)).getSimulation(1);
        verify(simMapper, times(1)).insertComment(any(Comment.class));
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
    }

    @Test
    public void tc004GetCommentsOwner() throws Exception {
        ArrayList<Comment> commentsList = new ArrayList<Comment>();
        commentsList.add(new Comment(1, 1, user.getUsername(), "test", new Date().getTime()));
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, "", 1, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(simMapper.getCommentsForSimulation(1)).thenReturn(commentsList);
        List<Comment> resultList = commentHandlerImpl.getCommentsForSimulation(1);
        Assert.assertEquals(resultList, commentsList);
        verify(simMapper, times(1)).getSimulation(1);
        verify(simMapper, times(1)).getCommentsForSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
    }

    @Test(expected = UnauthorizedException.class)
    public void tc005GetCommentsNotOwner() throws Exception {
        ArrayList<Comment> commentsList = new ArrayList<Comment>();
        commentsList.add(new Comment(1, 1, user.getUsername(), "test", new Date().getTime()));
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, "", 1, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(2);
        when(simMapper.getCommentsForSimulation(1)).thenReturn(commentsList);
        commentHandlerImpl.getCommentsForSimulation(1);
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
    }

    @Test
    public void tc006GetCommentsNotOwnerButAdmin() throws Exception {
        ArrayList<Comment> commentsList = new ArrayList<Comment>();
        commentsList.add(new Comment(1, 1, user.getUsername(), "test", new Date().getTime()));
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), true));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, "", 1, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(2);
        when(simMapper.getCommentsForSimulation(1)).thenReturn(commentsList);
        List<Comment> resultList = commentHandlerImpl.getCommentsForSimulation(1);
        Assert.assertEquals(resultList, commentsList);
        verify(simMapper, times(1)).getSimulation(1);
        verify(simMapper, times(1)).getCommentsForSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
    }
}
