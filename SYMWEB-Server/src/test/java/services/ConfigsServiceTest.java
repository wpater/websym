package services;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import app.exceptions.UnauthorizedException;
import app.model.Config;
import app.model.ConfigTag;
import app.model.Simulation;
import app.model.Tag;
import app.model.User;
import app.simulator.ConfigHandlerImpl;
import app.simulator.IFileHandler;
import app.simulator.ISimulationHandler;
import app.simulator.ITagHandler;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import testUtils.TestSecurityContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = app.spring.WebConfig.class)
@WebAppConfiguration
public class ConfigsServiceTest {

    private final String TEST_FORAM_CONFIG_CONTENT = "foramContent";
    private final String TEST_SIMULATION_CONFIG_CONTENT = "simulationContent";
    private final String TEST_ENVIRONMENT_CONFIG_CONTENT = "environmentContent";

    private static User user;

    @Mock
    private ISimulationMapper simMapper;

    @Mock
    private IUserMapper userMapper;

    @Mock
    private IFileHandler fileHandler;

    @Mock
    private ITagHandler tagHandler;
    
    @Mock
    private ISimulationHandler simulationHandler;
    
    @InjectMocks
    private ConfigHandlerImpl configHandler;

    @BeforeClass
    public static void initSuite() {
        user = new User();
        user.setEmail("test@test.com");
        user.setUsername("username_test_configs_service");
        user.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        reset(simMapper);
        reset(userMapper);
        reset(fileHandler);
        reset(tagHandler);
        reset(simulationHandler);
    }

    @After
    public void cleanupTestCase() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void tc001AddConfigToDatabaseSuccessful() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(fileHandler.saveUserConfigFiles(user.getUsername(), 1, TEST_FORAM_CONFIG_CONTENT,
                TEST_SIMULATION_CONFIG_CONTENT, TEST_ENVIRONMENT_CONFIG_CONTENT)).thenReturn(true);
        when(fileHandler.getEnvironmentConfigFilePath(user.getUsername(), 1)).thenReturn(".");
        when(fileHandler.getForamConfigFilePath(user.getUsername(), 1)).thenReturn(".");
        when(fileHandler.getSimulationConfigFilePath(user.getUsername(), 1)).thenReturn(".");
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
              Object[] args = invocation.getArguments();
              Config config = (Config) args[0];
              config.setIdConfig(1);
              return null;
            }
        }).when(simMapper).insertConfig(any(Config.class));
        String tagsString = "tag1;tag2";
        int result = configHandler.addConfigToDB(TEST_SIMULATION_CONFIG_CONTENT, TEST_ENVIRONMENT_CONFIG_CONTENT, TEST_FORAM_CONFIG_CONTENT, tagsString);
        Assert.assertEquals(result, 1);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(fileHandler, times(1)).saveUserConfigFiles(user.getUsername(), 1, TEST_FORAM_CONFIG_CONTENT,
                TEST_SIMULATION_CONFIG_CONTENT, TEST_ENVIRONMENT_CONFIG_CONTENT);
        verify(fileHandler, times(1)).getEnvironmentConfigFilePath(user.getUsername(), 1);
        verify(fileHandler, times(1)).getForamConfigFilePath(user.getUsername(), 1);
        verify(fileHandler, times(1)).getSimulationConfigFilePath(user.getUsername(), 1);
        verifyNoMoreInteractions(fileHandler);
        verify(simMapper, times(1)).updateConfig(any(Config.class));
        verify(simMapper, times(1)).insertConfig(any(Config.class));
        verifyNoMoreInteractions(simMapper);
        verify(tagHandler, times(1)).createConfigTags(1, tagsString);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test
    public void tc002AddConfigToDatabaseFailed() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(fileHandler.saveUserConfigFiles(user.getUsername(), 1, TEST_FORAM_CONFIG_CONTENT,
                TEST_SIMULATION_CONFIG_CONTENT, TEST_ENVIRONMENT_CONFIG_CONTENT)).thenReturn(false);
        int result = configHandler.addConfigToDB(TEST_SIMULATION_CONFIG_CONTENT, TEST_ENVIRONMENT_CONFIG_CONTENT, TEST_FORAM_CONFIG_CONTENT, "");
        Assert.assertEquals(result, -1);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(fileHandler, times(1)).saveUserConfigFiles(user.getUsername(), 0, TEST_FORAM_CONFIG_CONTENT,
                TEST_SIMULATION_CONFIG_CONTENT, TEST_ENVIRONMENT_CONFIG_CONTENT);
        verifyNoMoreInteractions(fileHandler);
        verify(simMapper, times(1)).insertConfig(any(Config.class));
        verify(simMapper, times(1)).deleteConfig(any(Integer.class));
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test(expected = UnauthorizedException.class)
    public void tc003AddConfigToDatabaseUnauthorized() throws Exception {
        configHandler.addConfigToDB(TEST_SIMULATION_CONFIG_CONTENT, TEST_FORAM_CONFIG_CONTENT, TEST_ENVIRONMENT_CONFIG_CONTENT, "");
        verifyNoMoreInteractions(userMapper);
        verifyNoMoreInteractions(fileHandler);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test
    public void tc004DeleteConfigFromDatabaseEmptySimulationList() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(simMapper.getUserSimulations(1)).thenReturn(new ArrayList<Simulation>());
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(userMapper.selectById(1)).thenReturn(user);
        Config config = new Config();
        config.setIdUser(1);
        when(simMapper.getConfig(1)).thenReturn(config);
        configHandler.deleteConfig(1);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verify(userMapper, times(1)).selectById(1);
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getConfig(1);
        verify(simMapper, times(1)).getUserSimulations(1);
        verify(simMapper, times(1)).deleteConfig(1);
        verify(simMapper, times(1)).deleteConfigTag(1);
        verifyNoMoreInteractions(simMapper);
        verify(fileHandler, times(1)).deleteConfigDirectory(user.getUsername(), 1);
        verifyNoMoreInteractions(fileHandler);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test
    public void tc005DeleteConfigFromDatabaseNonEmptySimulationList() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        Simulation sim1 = new Simulation(1, ".", 1, 1, "");
        Simulation sim2 = new Simulation(2, ".", 1, 1, "");
        ArrayList<Simulation> simulationList = new ArrayList<Simulation>();
        simulationList.add(sim1);
        simulationList.add(sim2);
        when(simMapper.getUserSimulations(1)).thenReturn(simulationList);
        Config config = new Config();
        config.setIdUser(1);
        when(simMapper.getConfig(1)).thenReturn(config);
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(userMapper.selectById(1)).thenReturn(user);
        configHandler.deleteConfig(1);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verify(userMapper, times(1)).selectById(1);
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getConfig(1);
        verify(simMapper, times(1)).getUserSimulations(1);
        verify(simMapper, times(1)).deleteConfig(1);
        verify(simMapper, times(1)).deleteConfigTag(1);
        verifyNoMoreInteractions(simMapper);
        verify(fileHandler, times(1)).deleteConfigDirectory(user.getUsername(), 1);
        verifyNoMoreInteractions(fileHandler);
        verifyNoMoreInteractions(tagHandler);
        verify(simulationHandler, times(1)).deleteSimulation(1);
        verify(simulationHandler, times(1)).deleteSimulation(2);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test(expected = UnauthorizedException.class)
    public void tc006DeleteConfigFromDatabaseUserNotOwner() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        Simulation sim1 = new Simulation(1, ".", 1, 1, "");
        Simulation sim2 = new Simulation(2, ".", 1, 1, "");
        ArrayList<Simulation> simulationList = new ArrayList<Simulation>();
        simulationList.add(sim1);
        simulationList.add(sim2);
        when(simMapper.getUserSimulations(1)).thenReturn(simulationList);
        Config config = new Config();
        config.setIdUser(1);
        when(simMapper.getConfig(1)).thenReturn(config);
        when(userMapper.getIdUser(user.getUsername())).thenReturn(2);
        configHandler.deleteConfig(1);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getConfig(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(fileHandler);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test
    public void tc007DeleteConfigFromDatabaseUserNotOwnerButAdmin() throws Exception {
        String ownerUsername = "ownerUsername";
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), true));
        Simulation sim1 = new Simulation(1, ".", 1, 1, "");
        Simulation sim2 = new Simulation(2, ".", 1, 1, "");
        ArrayList<Simulation> simulationList = new ArrayList<Simulation>();
        simulationList.add(sim1);
        simulationList.add(sim2);
        when(simMapper.getUserSimulations(1)).thenReturn(simulationList);
        Config config = new Config();
        config.setIdUser(1);
        when(simMapper.getConfig(1)).thenReturn(config);
        when(userMapper.getIdUser(user.getUsername())).thenReturn(2);
        when(userMapper.selectById(1)).thenReturn(new User(1, true, "", "", ownerUsername));
        configHandler.deleteConfig(1);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verify(userMapper, times(1)).selectById(1);
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getConfig(1);
        verify(simMapper, times(1)).getUserSimulations(1);
        verify(simMapper, times(1)).deleteConfig(1);
        verify(simMapper, times(1)).deleteConfigTag(1);
        verifyNoMoreInteractions(simMapper);
        verify(fileHandler, times(1)).deleteConfigDirectory(ownerUsername, 1);
        verifyNoMoreInteractions(fileHandler);
        verifyNoMoreInteractions(tagHandler);
        verify(simulationHandler, times(1)).deleteSimulation(1);
        verify(simulationHandler, times(1)).deleteSimulation(2);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test(expected = UnauthorizedException.class)
    public void tc008DeleteConfigFromDatabaseNotAuthorized() throws Exception {
        configHandler.deleteConfig(1);
        verifyNoMoreInteractions(userMapper);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(fileHandler);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test
    public void tc009GetConfig() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        Config config = new Config();
        config.setIdConfig(1);
        config.setIdUser(1);
        ArrayList<Tag> tagsList = new ArrayList<Tag>();
        tagsList.add(new Tag(1, "tag1"));
        tagsList.add(new Tag(2, "tag2"));
        config.setTagsCollection(tagsList);
        when(simMapper.getConfig(1)).thenReturn(config);
        Config configWithTags = configHandler.getConfig(1);
        checkConfigAttributesEquality(configWithTags, config);
        Assert.assertEquals(configWithTags.getTagsCollection(), tagsList);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getConfig(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test(expected = UnauthorizedException.class)
    public void tc009GetConfigNotAuthorized() throws Exception {
        configHandler.getConfig(1);
        verifyNoMoreInteractions(userMapper);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test(expected = UnauthorizedException.class)
    public void tc010GetConfigNotOwner() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        Config config = new Config();
        config.setIdConfig(1);
        config.setIdUser(2);
        when(simMapper.getConfig(1)).thenReturn(config);
        configHandler.getConfig(1);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getConfig(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test
    public void tc011GetConfigUserNotOwnerButAdmin() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), true));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(2);
        Config config = new Config();
        config.setIdConfig(1);
        config.setIdUser(1);
        ArrayList<Tag> tagsList = new ArrayList<Tag>();
        tagsList.add(new Tag(1, "tag1"));
        tagsList.add(new Tag(2, "tag2"));
        config.setTagsCollection(tagsList);
        when(simMapper.getConfig(1)).thenReturn(config);
        Config configWithTags = configHandler.getConfig(1);
        checkConfigAttributesEquality(configWithTags, config);
        Assert.assertEquals(configWithTags.getTagsCollection(), tagsList);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getConfig(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test
    public void tc012GetConfigsForUserWithNoConfigs() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        when(simMapper.getConfigsForUser(1)).thenReturn(new ArrayList<Config>());
        ArrayList<Config> configsList = configHandler.getConfigsForUser(1);
        Assert.assertNotNull(configsList);
        Assert.assertEquals(0, configsList.size());
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getConfigsForUser(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test
    public void tc013GetConfigsForUserWithConfigs() throws Exception {
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        ArrayList<Tag> mockTagList1 = new ArrayList<Tag>();
        mockTagList1.add(new Tag(1, "tag1"));
        ArrayList<Tag> mockTagList2 = new ArrayList<Tag>();
        mockTagList2.add(new Tag(3, "tag2"));
        mockTagList2.add(new Tag(3, "tag3"));
        ArrayList<Config> mockConfigsList = new ArrayList<Config>();
        Config conf1 = new Config(1, ".", ".", ".", 1, mockTagList1);
        Config conf2 = new Config(2, ".", ".", ".", 1, mockTagList2);
        mockConfigsList.add(conf1);
        mockConfigsList.add(conf2);
        when(simMapper.getConfigsForUser(1)).thenReturn(mockConfigsList);
        ArrayList<Config> configsList = configHandler.getConfigsForUser(1);
        Assert.assertNotNull(configsList);
        Assert.assertEquals(2, configsList.size());
        checkConfigAttributesEquality(configsList.get(0), conf2);
        Assert.assertEquals(configsList.get(0).getTagsCollection(), mockTagList2);
        checkConfigAttributesEquality(configsList.get(1), conf1);
        Assert.assertEquals(configsList.get(1).getTagsCollection(), mockTagList1);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        verify(simMapper, times(1)).getConfigsForUser(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    @Test(expected = UnauthorizedException.class)
    public void tc014GetConfigsUnauthorized() throws Exception {
        configHandler.getConfigsForUser(1);
        verifyNoMoreInteractions(userMapper);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(tagHandler);
        verifyNoMoreInteractions(simulationHandler);
    }

    public void checkConfigAttributesEquality(Config conf1, Config conf2) {
        Assert.assertEquals(conf1.getEnvironment_config(), conf2.getEnvironment_config());
        Assert.assertEquals(conf1.getForam_config(), conf2.getForam_config());
        Assert.assertEquals(conf1.getSimulation_config(), conf2.getSimulation_config());
        Assert.assertEquals(conf1.getIdConfig(), conf2.getIdConfig());
    }

}
