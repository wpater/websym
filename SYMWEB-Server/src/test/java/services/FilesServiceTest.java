package services;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import app.charts.ChartType;
import app.exceptions.UnauthorizedException;
import app.model.Chart;
import app.model.Simulation;
import app.model.User;
import app.simulator.FileHandlerImpl;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import testUtils.TestSecurityContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = app.spring.WebConfig.class)
@WebAppConfiguration
public class FilesServiceTest {
    private static User user;
    private final String mockedUserConfigDir = "." + File.separatorChar + "testDir";
    private final String TEST_FORAM_CONFIG_CONTENT = "foramContent";
    private final String TEST_SIMULATION_CONFIG_CONTENT = "simulationContent";
    private final String TEST_ENVIRONMENT_CONFIG_CONTENT = "environmentContent";
    @InjectMocks
    private FileHandlerImpl fileHandler;

    @Mock
    private ISimulationMapper simMapper;

    @Mock
    private IUserMapper userMapper;
    
    @BeforeClass
    public static void initSuite() {
        user = new User();
        user.setEmail("test@test.com");
        user.setUsername("username_test_files_service");
        user.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(fileHandler, "userConfigDir", mockedUserConfigDir);
    }

    @After
    public void cleanupTestCase() {
        SecurityContextHolder.clearContext();
        reset(simMapper);
        reset(userMapper);
    }

    @Test
    public void tc001GetResultDirPath() {
        String resultDir = fileHandler.getSimulationResultDirPath(user.getUsername(), 1);
        Assert.assertEquals(resultDir, mockedUserConfigDir + File.separatorChar + user.getUsername()
                + File.separatorChar + "simulations" + File.separatorChar + 1 + File.separatorChar + "result");
    }

    @Test
    public void tc002GetSimulationConfigFilePath() {
        String resultDir = fileHandler.getSimulationConfigFilePath(user.getUsername(), 1);
        Assert.assertEquals(resultDir, mockedUserConfigDir + File.separatorChar + user.getUsername()
                + File.separatorChar + "configs" + File.separatorChar + "1" + File.separatorChar + "simulation.js");
    }

    @Test
    public void tc003GetEnvironmentConfigFilePath() {
        String resultDir = fileHandler.getEnvironmentConfigFilePath(user.getUsername(), 1);
        Assert.assertEquals(resultDir, mockedUserConfigDir + File.separatorChar + user.getUsername()
                + File.separatorChar + "configs" + File.separatorChar + 1 + File.separatorChar + "environment.js");
    }

    @Test
    public void tc004GetForamConfigFilePath() {
        String resultDir = fileHandler.getForamConfigFilePath(user.getUsername(), 1);
        Assert.assertEquals(resultDir, mockedUserConfigDir + File.separatorChar + user.getUsername()
                + File.separatorChar + "configs" + File.separatorChar + 1 + File.separatorChar + "foram.js");
    }

    @Test
    public void tc005ReadAllBytesFromAFile() throws Exception {
        String testFileContent = "testContent";
        String testFilePath = "." + File.separatorChar + "testFile";
        File testFile = new File(testFilePath);
        if (testFile.exists())
            throw new FileAlreadyExistsException(
                    "Cannot test fileHandler readAllBytesFromFile method as file already exists.");
        testFile.createNewFile();
        PrintWriter writer = new PrintWriter(testFile);
        writer.write("testContent");
        writer.close();
        Assert.assertArrayEquals(testFileContent.getBytes(), fileHandler.readAllBytesFromFile(testFilePath));
        testFile.delete();
    }

    @Test
    public void tc006DeleteEmptySimulationDirectory() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + "simulations" + File.separatorChar + 1;
        if (!(new File(testConfigFilePath).mkdirs())) {
            throw new Exception("Cannot test fileHandler readAllBytesFromFile method as mkdirs method failed.");
        }
        fileHandler.deleteSimulationDirectory(user.getUsername(), 1);
        Assert.assertTrue(!new File(testConfigFilePath).exists());
    }

    @Test
    public void tc007DeleteNonEmptySimulationDirectory() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + "simulations" + File.separatorChar + 1;
        if (!(new File(testConfigFilePath).mkdirs())) {
            throw new Exception("Cannot test fileHandler readAllBytesFromFile method as mkdirs method failed.");
        }
        new File(testConfigFilePath + File.separator + "testFile").createNewFile();
        fileHandler.deleteSimulationDirectory(user.getUsername(), 1);
        Assert.assertTrue(!new File(testConfigFilePath + File.separator + "testFile").exists());
        Assert.assertTrue(!new File(testConfigFilePath).exists());
    }

    @Test
    public void tc008SaveUserConfigFilesSuccesful() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + "configs" + File.separatorChar + 2;
        if (new File(testConfigFilePath).exists()) {
            throw new Exception("Cannot test fileHandler saveUserConfigFiles method as dir " + testConfigFilePath
                    + " already exists");
        }
        boolean successful = fileHandler.saveUserConfigFiles(user.getUsername(), 2, TEST_FORAM_CONFIG_CONTENT,
                TEST_SIMULATION_CONFIG_CONTENT, TEST_ENVIRONMENT_CONFIG_CONTENT);
        Assert.assertTrue(successful);
        Assert.assertArrayEquals(Files.readAllBytes(Paths.get(testConfigFilePath + File.separatorChar + "foram.js")),
                TEST_FORAM_CONFIG_CONTENT.getBytes());
        Assert.assertArrayEquals(
                Files.readAllBytes(Paths.get(testConfigFilePath + File.separatorChar + "simulation.js")),
                TEST_SIMULATION_CONFIG_CONTENT.getBytes());
        Assert.assertArrayEquals(
                Files.readAllBytes(Paths.get(testConfigFilePath + File.separatorChar + "environment.js")),
                TEST_ENVIRONMENT_CONFIG_CONTENT.getBytes());
        FileUtils.deleteDirectory(new File(testConfigFilePath));
    }

    @Test
    public void tc009SaveUserConfigFilesFailed() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + "configs" + File.separatorChar + +3;
        if (new File(testConfigFilePath).exists()) {
            throw new Exception("Cannot test fileHandler saveUserConfigFiles method as dir " + testConfigFilePath
                    + " already exists");
        }
        new File(testConfigFilePath).mkdirs();
        boolean successful = fileHandler.saveUserConfigFiles(user.getUsername(), 3, TEST_FORAM_CONFIG_CONTENT,
                TEST_SIMULATION_CONFIG_CONTENT, TEST_ENVIRONMENT_CONFIG_CONTENT);
        Assert.assertFalse(successful);
        Assert.assertTrue(new File(testConfigFilePath).exists());
        Assert.assertFalse(new File(testConfigFilePath + File.separatorChar + "foram.js").exists());
        Assert.assertFalse(new File(testConfigFilePath + File.separatorChar + "simulation.js").exists());
        Assert.assertFalse(new File(testConfigFilePath + File.separatorChar + "environment.js").exists());
        FileUtils.deleteDirectory(new File(testConfigFilePath));
    }

    @Test
    public void tc010AppendSimulationOutputNonexistingFile() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + 4 + File.separatorChar + "result";
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, testConfigFilePath, 1, 1, ""));
        if (new File(testConfigFilePath).exists()) {
            throw new Exception("Cannot test fileHandler appendSimulationOutput method as dir " + testConfigFilePath
                    + " already exists");
        }
        new File(testConfigFilePath).mkdirs();
        fileHandler.appendSimulationOutput(1, "test");
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(userMapper);
        File file = new File(testConfigFilePath + File.separatorChar + "output.txt");
        Assert.assertTrue(file.exists());
        Assert.assertEquals("test" + System.lineSeparator(),
                new String(Files.readAllBytes(Paths.get(testConfigFilePath + File.separatorChar + "output.txt"))));
        file.delete();
        new File(testConfigFilePath).delete();
    }

    @Test
    public void tc011AppendSimulationOutputExistingFile() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + 5 + File.separatorChar + "result";
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, testConfigFilePath, 1, 1, ""));
        if (new File(testConfigFilePath).exists()) {
            throw new Exception("Cannot test fileHandler appendSimulationOutput method as dir " + testConfigFilePath
                    + " already exists");
        }
        new File(testConfigFilePath).mkdirs();
        File file = new File(testConfigFilePath + File.separatorChar + "output.txt");
        file.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
        writer.write("test1");
        writer.newLine();
        writer.close();
        fileHandler.appendSimulationOutput(1, "test2");
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verifyNoMoreInteractions(userMapper);
        Assert.assertTrue(file.exists());
        Assert.assertEquals("test1" + System.lineSeparator() + "test2" + System.lineSeparator(),
                new String(Files.readAllBytes(Paths.get(testConfigFilePath + File.separatorChar + "output.txt"))));
        file.delete();
        new File(testConfigFilePath).delete();
    }

    @Test
    public void tc012GetSimulationOutputNonExistingFile() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + 6 + File.separatorChar + "result";
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, testConfigFilePath, 1, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        if (new File(testConfigFilePath + File.separatorChar + "output.txt").exists()) {
            throw new Exception("Cannot test fileHandler getSimulationOutput method as dir " + testConfigFilePath
                    + " already exists");
        }
        String content = fileHandler.getSimulationOutput(1);
        Assert.assertNull(content);
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
    }

    @Test
    public void tc013GetSimulationOutputExistingFile() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + 7 + File.separatorChar + "result";
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, testConfigFilePath, 1, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        File dirFile = new File(testConfigFilePath);
        File outputFile = new File(testConfigFilePath + File.separatorChar + "output.txt");
        if (dirFile.exists()) {
            throw new Exception("Cannot test fileHandler getSimulationOutput method as dir " + testConfigFilePath
                    + " already exists");
        }
        dirFile.mkdirs();
        outputFile.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));
        writer.write("test1");
        writer.newLine();
        writer.close();
        String content = fileHandler.getSimulationOutput(1);
        Assert.assertEquals("test1" + System.lineSeparator(), content);
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        outputFile.delete();
        dirFile.delete();
    }

    @Test(expected = UnauthorizedException.class)
    public void tc014GetSimulationOutputExistingFileUnauthorized() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + 8 + File.separatorChar + "result";
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), false));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, testConfigFilePath, 2, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        fileHandler.getSimulationOutput(1);
    }

    @Test
    public void tc013GetSimulationOutputExistingFileUnauthorizedButAdmin() throws Exception {
        String testConfigFilePath = mockedUserConfigDir + File.separatorChar + user.getUsername() + File.separatorChar
                + 9 + File.separatorChar + "result";
        SecurityContextHolder.setContext(new TestSecurityContext(user.getUsername(), true));
        when(simMapper.getSimulation(1)).thenReturn(new Simulation(1, testConfigFilePath, 2, 1, ""));
        when(userMapper.getIdUser(user.getUsername())).thenReturn(1);
        File dirFile = new File(testConfigFilePath);
        File outputFile = new File(testConfigFilePath + File.separatorChar + "output.txt");
        if (dirFile.exists()) {
            throw new Exception("Cannot test fileHandler getSimulationOutput method as dir " + testConfigFilePath
                    + " already exists");
        }
        dirFile.mkdirs();
        outputFile.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));
        writer.write("test1");
        writer.newLine();
        writer.close();
        String content = fileHandler.getSimulationOutput(1);
        Assert.assertEquals("test1" + System.lineSeparator(), content);
        verify(simMapper, times(1)).getSimulation(1);
        verifyNoMoreInteractions(simMapper);
        verify(userMapper, times(1)).getIdUser(user.getUsername());
        verifyNoMoreInteractions(userMapper);
        outputFile.delete();
        dirFile.delete();
    }
    
    @Test
    public void tc014GetChartPath() {
        ChartType chartType = ChartType.BORN_DEAD_CHART;
        Chart chart = new Chart(1, chartType, 1, 1);
        when(simMapper.getChart(1)).thenReturn(chart);
        Simulation simulation = new Simulation(1, ".", 1, 1, "");
        when(simMapper.getSimulation(1)).thenReturn(simulation);
        String resultDir = fileHandler.getChartPath(chart.getIdChart());
        Assert.assertEquals(resultDir, simulation.getPathToResult() + File.separator + chart.getChartType().getFileName());
    }

}
