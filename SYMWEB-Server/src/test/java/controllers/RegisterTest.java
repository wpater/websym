package controllers;

import app.exceptions.EmailExistsException;
import app.exceptions.UnauthorizedException;
import app.model.User;
import app.model.dataTransferObjects.UserDto;
import app.simulator.UserDtoValidator;
import app.svc.web.user.IRegisterService;
import app.svc.web.user.RegisterController;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.server.result.FlashAttributeResultMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = app.spring.WebConfig.class)
@WebAppConfiguration
public class RegisterTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    private static UserDto user;

    @Mock
    UserDtoValidator userDtoValidator;

    @InjectMocks
    RegisterController registerController;

    @Autowired
    private IRegisterService registerService;

    @BeforeClass
    public static void init_suite() {
        java.util.Date date = new java.util.Date();
        user = new UserDto();
        user.setEmail("testRegisterController" + date.getTime() + "@test.com");
        user.setUsername("test_register" + date.getTime());
        user.setPassword("password_test");
        user.setMatchingPassword("password_test");
    }

    @Before
    public void init_test_case() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void tc001_register_method_post_no_content() throws Exception {
        mockMvc.perform(post("/register")).andExpect(view().name("redirect:/home"));
    }

    @Test
    public void tc002_register_method_post_with_new_user() throws Exception {
        User user2 = new User();
        user2.setUsername(user.getUsername());
        mockMvc.perform(post("/register").param("username", user.getUsername()).param("email", user.getEmail())
                .param("password", user.getPassword()).param("matchingPassword", user.getMatchingPassword()))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/home"));
        registerService.deleteUser(user2);
    }

    @Test
    public void tc003_register_method_post_with_same_user() throws Exception {
        User user2 = registerService.register(user);
        mockMvc.perform(post("/register").param("username", user.getUsername()).param("email", user.getEmail())
                .param("password", user.getPassword())).andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/home")).andExpect(flash().attribute("registerSuccessful", false))
                .andExpect(flash().attributeExists("error")).andReturn();
        registerService.deleteUser(user2);
    }
}