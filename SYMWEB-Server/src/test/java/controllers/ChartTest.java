package controllers;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.View;

import app.charts.ChartType;
import app.model.Chart;
import app.model.User;
import app.simulator.IChartHandler;
import app.simulator.IFileHandler;
import app.svc.web.simulator.ChartsController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { app.spring.WebConfig.class })
@WebAppConfiguration
public class ChartTest {

    @Autowired
    private Filter springSecurityFilterChain;

    private MockMvc mockMvc;
    private static User userObj;

    @InjectMocks
    private ChartsController chartsController;

    @Mock
    View view;

    @Mock
    private IFileHandler fileHandlerMock;

    @Mock
    private IChartHandler chartHandlerMock;

    @BeforeClass
    public static void initSuite() {
        java.util.Date date = new java.util.Date();
        userObj = new User();
        userObj.setEmail("test@test.com");
        userObj.setUsername("username_test_chart_controller" + date.getTime());
        userObj.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(chartsController).setSingleView(view).build();
        reset(fileHandlerMock);
        reset(chartHandlerMock);
    }

    @Test
    public void tc001chartsDisplayPage() throws Exception {
        Chart chart = new Chart(1, ChartType.BORN_DEAD_CHART, 1, 1);
        when(chartHandlerMock.getChart(1)).thenReturn(chart);
        when(fileHandlerMock.getChartPath(1)).thenReturn(".");
        when(fileHandlerMock.readAllBytesFromFile(".")).thenReturn("".getBytes());
        mockMvc.perform(get("/chart/1").with(user(userObj.getUsername()).password(userObj.getPassword())))
                .andExpect(status().isOk()).andExpect(view().name("chart"))
                .andExpect(model().attribute("chartType", chart.getChartType()))
                .andExpect(model().attribute("chartContent", ""));
        verify(chartHandlerMock, times(1)).getChart(1);
        verifyNoMoreInteractions(chartHandlerMock);
        verify(fileHandlerMock, times(1)).getChartPath(1);
        verify(fileHandlerMock, times(1)).readAllBytesFromFile(".");
        verifyNoMoreInteractions(fileHandlerMock);
    }

    @Test
    public void tc002chartsDisplayPageNoChart() throws Exception {
        mockMvc.perform(get("/chart/1").with(user(userObj.getUsername()).password(userObj.getPassword())))
                .andExpect(status().isOk()).andExpect(view().name("chart"))
                .andExpect(model().attributeDoesNotExist("chartType", "chartContent"));
        verify(chartHandlerMock, times(1)).getChart(1);
        verifyNoMoreInteractions(chartHandlerMock);
        verifyNoMoreInteractions(fileHandlerMock);
    }

}