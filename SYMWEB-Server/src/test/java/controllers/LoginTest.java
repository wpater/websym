package controllers;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import app.exceptions.EmailExistsException;
import app.exceptions.UsernameExistsException;
import app.model.User;
import app.model.dataTransferObjects.UserDto;
import app.simulator.IMessageHandler;
import app.svc.web.user.IRegisterService;
import app.svc.web.user.LoginController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = app.spring.WebConfig.class)
@WebAppConfiguration
public class LoginTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    private static UserDto userDto;
    private static User user;

    @Autowired
    private IRegisterService registerService;

    @Autowired
    private Filter springSecurityFilterChain;

    @BeforeClass
    public static void initSuite() {
        java.util.Date date = new java.util.Date();
        userDto = new UserDto();
        userDto.setEmail("test_login_controller" + date.getTime() + "@test.com");
        userDto.setUsername("username_test_login_controller" + date.getTime());
        userDto.setPassword("password_test");
        userDto.setMatchingPassword("password_test");

    }

    @Before
    public void initTestCase() throws EmailExistsException, UsernameExistsException {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).addFilters(springSecurityFilterChain).build();
        user = registerService.register(userDto);
    }

    @After
    public void cleanupTestCase() {
        registerService.deleteUser(user);
    }

    @Test
    public void tc001LoginMethodGet() throws Exception {
        mockMvc.perform(get("/login")).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void tc002LogoutMethodGetWithoutAuthentication() throws Exception {
        mockMvc.perform(get("/logout")).andExpect(view().name("redirect:/home"))
                .andExpect(flash().attribute("success", "You have succesfuly logged out"));
    }

    @Test
    public void tc003LogoutMethodGetWithAuthentication() throws Exception {
        mockMvc.perform(get("/logout").with(user(user.getUsername()).password(userDto.getPassword())))
                .andExpect(view().name("redirect:/home"))
                .andExpect(flash().attribute("success", "You have succesfuly logged out"));
    }

    @Test
    public void tc004LoginMethodPostValid() throws Exception {
        mockMvc.perform(
                post("/login").param("ssoId", user.getUsername()).param("password", userDto.getPassword()).with(csrf()))
                .andExpect(status().isFound()).andExpect(header().string("Location", "/login?success"));
    }

    @Test
    public void tc005LoginMethodPostInvalid() throws Exception {
        mockMvc.perform(
                post("/login").param("ssoId", user.getUsername()).param("password", "someInvalidPassword").with(csrf()))
                .andExpect(status().isFound()).andExpect(header().string("Location", "/login?error"));
    }

}
