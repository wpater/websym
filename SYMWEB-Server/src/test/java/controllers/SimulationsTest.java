package controllers;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.View;

import app.charts.ChartType;
import app.exceptions.EmailExistsException;
import app.exceptions.UsernameExistsException;
import app.model.Chart;
import app.model.Comment;
import app.model.Simulation;
import app.model.User;
import app.model.dataTransferObjects.UserDto;
import app.simulator.IChartHandler;
import app.simulator.ICommentHandler;
import app.simulator.IFileHandler;
import app.simulator.IMessageHandler;
import app.simulator.ISimulationHandler;
import app.svc.web.simulator.SimulationsController;
import app.svc.web.user.IRegisterService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { app.spring.WebConfig.class })
@WebAppConfiguration
public class SimulationsTest {

    @Autowired
    private Filter springContextSecurityFilter;

    private MockMvc mockMvc;
    private static UserDto userObj;
    private static User user;
    @Autowired
    private IRegisterService registerService;

    @Mock
    private ISimulationHandler simulationHandlerMock;

    @Mock
    private ICommentHandler commentHandlerMock;

    @Mock
    private View view;

    @Mock
    private IMessageHandler messageHandlerMock;

    @Mock
    private IFileHandler fileHandlerMock;
    
    @Mock
    private IChartHandler chartHandlerMock;
    
    @InjectMocks
    private SimulationsController simulationsController;

    @BeforeClass
    public static void initSuite() {
        java.util.Date date = new java.util.Date();
        userObj = new UserDto();
        userObj.setEmail("testsimulations@test.com");
        userObj.setUsername("simulations_controller" + date.getTime());
        userObj.setPassword("password_test");
        userObj.setMatchingPassword(userObj.getPassword());
    }

    @Before
    public void initTestCase() throws EmailExistsException, UsernameExistsException {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(simulationsController).setSingleView(view).build();
        user = registerService.register(userObj);
        reset(commentHandlerMock);
        reset(simulationHandlerMock);
        reset(messageHandlerMock);
        reset(fileHandlerMock);
        reset(chartHandlerMock);
    }

    @After
    public void cleanupTestCase() {
        registerService.deleteUser(user);
    }

    @Test
    public void tc001SimulationsMethodGetOngoingWithNoSimulations() throws Exception {
        when(simulationHandlerMock.getSimulations(null, true)).thenReturn(new ArrayList<Simulation>());
        mockMvc.perform(get("/simulations").param("ongoing", "true")
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("simulations")).andExpect(model().attribute("table", hasSize(0)));
        verify(simulationHandlerMock, times(1)).getSimulations(null, true);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(fileHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }

    @Test
    public void tc002SimulationsMethodGetFinishedWithNoSimulations() throws Exception {
        when(simulationHandlerMock.getSimulations(null, false)).thenReturn(new ArrayList<Simulation>());
        mockMvc.perform(get("/simulations").param("ongoing", "false")
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("simulations")).andExpect(model().attribute("table", hasSize(0)));
        verify(simulationHandlerMock, times(1)).getSimulations(null, false);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(fileHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }

    @Test
    public void tc003SimulationsMethodGetOngoingWithExistingSimulation() throws Exception {
        Simulation simulation = new Simulation(1, "", 1, 1, "");
        ArrayList<Simulation> simulationList = new ArrayList<Simulation>();
        simulationList.add(simulation);
        when(simulationHandlerMock.getSimulations(null, true)).thenReturn(simulationList);
        mockMvc.perform(get("/simulations").param("ongoing", "true")
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("simulations")).andExpect(model().attribute("table", hasSize(1)))
                .andExpect(model().attribute("table", hasItem(simulation)));
        verify(simulationHandlerMock, times(1)).getSimulations(null, true);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(fileHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }

    @Test
    public void tc004SimulationsMethodGetFinishedWithExistingSimulation() throws Exception {
        Simulation simulation = new Simulation(1, "", 1, 1, "");
        ArrayList<Simulation> simulationList = new ArrayList<Simulation>();
        simulationList.add(simulation);
        when(simulationHandlerMock.getSimulations(null, false)).thenReturn(simulationList);
        mockMvc.perform(get("/simulations").param("ongoing", "false")
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("simulations")).andExpect(model().attribute("table", hasSize(1)))
                .andExpect(model().attribute("table", hasItem(simulation)));
        verify(simulationHandlerMock, times(1)).getSimulations(null, false);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(fileHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }

    @Test
    public void tc005PostAddComment() throws Exception {
        mockMvc.perform(post("/addComment").with(csrf()).param("id", "1").param("content", "comment")
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("redirect:/simulations/1"));
        verifyNoMoreInteractions(simulationHandlerMock);
        verify(commentHandlerMock, times(1)).addComment(1, "comment");
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(fileHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }

    @Test
    public void tc006GetSimulationInfoNoComments() throws Exception {
        String outputString = "test output";
        when(commentHandlerMock.getCommentsForSimulation(1)).thenReturn(new ArrayList<Comment>());
        Simulation simulation = new Simulation(1, "", 1, 1, "");
        when(simulationHandlerMock.getSimulationWithId(1)).thenReturn(simulation);
        when(fileHandlerMock.getSimulationOutput(1)).thenReturn(outputString);
        ArrayList<Chart> chartsForSimulation = new ArrayList<Chart>();
        chartsForSimulation.add(new Chart(1, ChartType.BORN_DEAD_CHART, 1, 1));
        when(chartHandlerMock.getChartsForSimulation(1)).thenReturn(chartsForSimulation);
        mockMvc.perform(
                get("/simulations/1").with(csrf()).with(user(userObj.getUsername()).password(userObj.getPassword())))
                .andExpect(status().isOk()).andExpect(view().name("simulationInfo"))
                .andExpect(model().attribute("simulation", simulation))
                .andExpect(model().attribute("comments", hasSize(0)))
                .andExpect(model().attribute("output", equalTo(outputString)))
                .andExpect(model().attribute("charts", equalTo(chartsForSimulation)));
        verify(commentHandlerMock, times(1)).getCommentsForSimulation(1);
        verifyNoMoreInteractions(commentHandlerMock);
        verify(simulationHandlerMock, times(1)).getSimulationWithId(1);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verify(fileHandlerMock, times(1)).getSimulationOutput(1);
        verifyNoMoreInteractions(fileHandlerMock);
        verify(chartHandlerMock, times(1)).getChartsForSimulation(1);
        verifyNoMoreInteractions(chartHandlerMock);
    }

    @Test
    public void tc007GetSimulationInfoWithComments() throws Exception {
        String outputString = "test output";
        ArrayList<Comment> comments = new ArrayList<Comment>();
        Comment c1 = new Comment(1, 1, "username1", "content1", new Date().getTime());
        Comment c2 = new Comment(2, 1, "username1", "content2", new Date().getTime());
        comments.add(c1);
        comments.add(c2);
        when(commentHandlerMock.getCommentsForSimulation(1)).thenReturn(comments);
        Simulation simulation = new Simulation(1, "", 1, 1, "");
        when(simulationHandlerMock.getSimulationWithId(1)).thenReturn(simulation);
        when(fileHandlerMock.getSimulationOutput(1)).thenReturn(outputString);
        ArrayList<Chart> chartsForSimulation = new ArrayList<Chart>();
        chartsForSimulation.add(new Chart(1, ChartType.BORN_DEAD_CHART, 1, 1));
        when(chartHandlerMock.getChartsForSimulation(1)).thenReturn(chartsForSimulation);
        mockMvc.perform(
                get("/simulations/1").with(user(userObj.getUsername()).password(userObj.getPassword())).with(csrf()))
                .andExpect(status().isOk()).andExpect(view().name("simulationInfo"))
                .andExpect(model().attribute("simulation", simulation))
                .andExpect(model().attribute("comments", hasSize(2)))
                .andExpect(model().attribute("comments", hasItem(c1)))
                .andExpect(model().attribute("comments", hasItem(c2)))
                .andExpect(model().attribute("output", equalTo(outputString)))
                .andExpect(model().attribute("charts", equalTo(chartsForSimulation)));
        verify(commentHandlerMock, times(1)).getCommentsForSimulation(1);
        verifyNoMoreInteractions(commentHandlerMock);
        verify(simulationHandlerMock, times(1)).getSimulationWithId(1);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verify(fileHandlerMock, times(1)).getSimulationOutput(1);
        verifyNoMoreInteractions(fileHandlerMock);
        verify(chartHandlerMock, times(1)).getChartsForSimulation(1);
        verifyNoMoreInteractions(chartHandlerMock);
    }

    @Test
    public void tc008StopSimulationSuccessful() throws Exception {
        when(simulationHandlerMock.stopSimulation(1)).thenReturn(true);
        when(messageHandlerMock.getMessage("simulation.stop.success")).thenReturn("1");
        mockMvc.perform(post("/stopSimulation/1").with(csrf())
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("redirect:/simulations/1")).andExpect(flash().attribute("success", "1"));
        verify(simulationHandlerMock, times(1)).stopSimulation(1);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(simulationHandlerMock);
        verify(messageHandlerMock, times(1)).getMessage("simulation.stop.success");
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }

    @Test
    public void tc009StopSimulationFailure() throws Exception {
        when(simulationHandlerMock.stopSimulation(1)).thenReturn(false);
        when(messageHandlerMock.getMessage("simulation.stop.failure")).thenReturn("1");
        mockMvc.perform(post("/stopSimulation/1").with(csrf())
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("redirect:/simulations/1")).andExpect(flash().attribute("error", "1"));
        verify(simulationHandlerMock, times(1)).stopSimulation(1);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(simulationHandlerMock);
        verify(messageHandlerMock, times(1)).getMessage("simulation.stop.failure");
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }

    @Test
    public void tc010ConfigSimulationsNoSimulations() throws Exception {
        ArrayList<Simulation> simulationsList = new ArrayList<>();
        when(simulationHandlerMock.getConfigSimulations(1)).thenReturn(simulationsList);
        mockMvc.perform(get("/configSimulations").param("id", "1"))
        .andExpect(view().name("simulations"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("table", simulationsList));
        verify(simulationHandlerMock, times(1)).getConfigSimulations(1);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }
    
    @Test
    public void tc011ConfigSimulationsWithSimulations() throws Exception {
        ArrayList<Simulation> simulationsList = new ArrayList<>();
        simulationsList.add(new Simulation(1, "", 1, 1, ""));
        simulationsList.add(new Simulation(2, "", 1, 1, ""));
        when(simulationHandlerMock.getConfigSimulations(1)).thenReturn(simulationsList);
        mockMvc.perform(get("/configSimulations").param("id", "1"))
        .andExpect(view().name("simulations"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("table", simulationsList));
        verify(simulationHandlerMock, times(1)).getConfigSimulations(1);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }
    
    @Test
    public void tc012GetUserSimulationsNoSimulations() throws Exception {
        ArrayList<Simulation> simulationsList = new ArrayList<>();
        when(simulationHandlerMock.getSimulations(1, null)).thenReturn(simulationsList);
        mockMvc.perform(get("/simulations").param("user", "1"))
        .andExpect(view().name("simulations"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("table", simulationsList));
        verify(simulationHandlerMock, times(1)).getSimulations(1, null);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }
    
    @Test
    public void tc013GetUserSimulationsWithSimulations() throws Exception {
        ArrayList<Simulation> simulationsList = new ArrayList<>();
        simulationsList.add(new Simulation(1, "", 1, 1, ""));
        simulationsList.add(new Simulation(2, "", 1, 1, ""));
        when(simulationHandlerMock.getSimulations(1, null)).thenReturn(simulationsList);
        mockMvc.perform(get("/simulations").param("user", "1"))
        .andExpect(view().name("simulations"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("table", simulationsList));
        verify(simulationHandlerMock, times(1)).getSimulations(1, null);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }
    
    @Test
    public void tc014GetUserSimulationsOngoingWithSimulations() throws Exception {
        ArrayList<Simulation> simulationsList = new ArrayList<>();
        simulationsList.add(new Simulation(1, "", 1, 1, ""));
        simulationsList.add(new Simulation(2, "", 1, 1, ""));
        when(simulationHandlerMock.getSimulations(1, true)).thenReturn(simulationsList);
        mockMvc.perform(get("/simulations").param("user", "1").param("ongoing", "true"))
        .andExpect(view().name("simulations"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("table", simulationsList));
        verify(simulationHandlerMock, times(1)).getSimulations(1, true);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }
    
    @Test
    public void tc015GetUserSimulationsDoneWithSimulations() throws Exception {
        ArrayList<Simulation> simulationsList = new ArrayList<>();
        simulationsList.add(new Simulation(1, "", 1, 1, ""));
        simulationsList.add(new Simulation(2, "", 1, 1, ""));
        when(simulationHandlerMock.getSimulations(1, false)).thenReturn(simulationsList);
        mockMvc.perform(get("/simulations").param("user", "1").param("ongoing", "false"))
        .andExpect(view().name("simulations"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("table", simulationsList));
        verify(simulationHandlerMock, times(1)).getSimulations(1, false);
        verifyNoMoreInteractions(simulationHandlerMock);
        verifyNoMoreInteractions(commentHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
        verifyNoMoreInteractions(chartHandlerMock);
    }

}