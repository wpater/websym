package controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.View;

import app.model.Config;
import app.model.User;
import app.simulator.IConfigHandler;
import app.simulator.IFileHandler;
import app.simulator.IMessageHandler;
import app.svc.web.simulator.ConfigsController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { app.spring.WebConfig.class })
@WebAppConfiguration
public class ConfigTest {

    @Autowired
    private Filter springSecurityFilterChain;

    private MockMvc mockMvc;
    private static User userObj;

    @InjectMocks
    private ConfigsController configsController;

    @Mock
    View view;

    @Mock
    private IConfigHandler configHandlerMock;

    @Mock
    private IFileHandler fileHandlerMock;

    @Mock
    private IMessageHandler messageHandlerMock;

    @BeforeClass
    public static void initSuite() {
        java.util.Date date = new java.util.Date();
        userObj = new User();
        userObj.setEmail("test@test.com");
        userObj.setUsername("username_test_config_controller" + date.getTime());
        userObj.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(configsController).setSingleView(view).build();
        reset(configHandlerMock);
        reset(messageHandlerMock);
    }

    @Test
    public void tc001ConfigsMethodGetAuthenticated() throws Exception {
        when(configHandlerMock.getConfigsForUser(null)).thenReturn(new ArrayList<Config>());
        mockMvc.perform(get("/configs").with(user(userObj.getUsername()).password(userObj.getPassword())))
                .andExpect(status().isOk()).andExpect(view().name("configs"))
                .andExpect(model().attribute("table", hasSize(0)));
        verify(configHandlerMock, times(1)).getConfigsForUser(null);
        verifyNoMoreInteractions(configHandlerMock);
    }

    @Test
    public void tc002ConfigMethodDelete() throws Exception {
        Config config = new Config();
        config.setIdUser(1);
        when(messageHandlerMock.getMessage("config.deleted")).thenReturn("1");
        when(configHandlerMock.deleteConfig(1)).thenReturn(config);
        mockMvc.perform(post("/deleteConfig").param("id", "1").with(csrf())
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("redirect:/configs")).andExpect(flash().attribute("success", "1"))
                .andExpect(model().attribute("user", "1"));
        verify(configHandlerMock, times(1)).deleteConfig(1);
        verifyNoMoreInteractions(configHandlerMock);
        verify(messageHandlerMock, times(1)).getMessage("config.deleted");
        verifyNoMoreInteractions(messageHandlerMock);
    }

    @Test
    public void tc003ConfigMethodLoad() throws Exception {
        Config config = new Config();
        config.setIdUser(1);
        config.setForam_config("foramPath");
        config.setEnvironment_config("envPath");
        config.setSimulation_config("simPath");
        when(configHandlerMock.getConfig(1)).thenReturn(config);
        when(fileHandlerMock.readAllBytesFromFile("foramPath")).thenReturn("foramContent".getBytes());
        when(fileHandlerMock.readAllBytesFromFile("envPath")).thenReturn("envContent".getBytes());
        when(fileHandlerMock.readAllBytesFromFile("simPath")).thenReturn("simContent".getBytes());
        when(messageHandlerMock.getMessage("config.loaded")).thenReturn("1");
        mockMvc.perform(
                get("/loadConfig").param("id", "1").with(user(userObj.getUsername()).password(userObj.getPassword())))
                .andExpect(status().isOk()).andExpect(view().name("redirect:/configs"))
                .andExpect(flash().attribute("foram", "foramContent"))
                .andExpect(flash().attribute("environment", "envContent"))
                .andExpect(flash().attribute("simulation", "simContent")).andExpect(flash().attribute("success", "1"))
                .andExpect(model().attribute("user", "1"));
        verify(configHandlerMock, times(1)).getConfig(1);
        verifyNoMoreInteractions(configHandlerMock);
        verify(fileHandlerMock, times(1)).readAllBytesFromFile("foramPath");
        verify(fileHandlerMock, times(1)).readAllBytesFromFile("envPath");
        verify(fileHandlerMock, times(1)).readAllBytesFromFile("simPath");
        verifyNoMoreInteractions(fileHandlerMock);
        verify(messageHandlerMock, times(1)).getMessage("config.loaded");
        verifyNoMoreInteractions(messageHandlerMock);
    }

}