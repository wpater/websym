package controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.View;

import app.model.Config;
import app.model.User;
import app.simulator.IConfigHandler;
import app.simulator.IFileHandler;
import app.simulator.IMessageHandler;
import app.simulator.ISimulationHandler;
import app.simulator.ISimulatorsHandler;
import app.svc.web.simulator.ConfigsController;
import app.svc.web.simulator.CreateController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { app.spring.WebConfig.class })
@WebAppConfiguration
public class CreateTest {

    @Autowired
    private Filter springSecurityFilterChain;

    private MockMvc mockMvc;
    private static User userObj;

    @InjectMocks
    private CreateController createController;

    @Mock
    View view;

    @Mock
    private IConfigHandler configHandlerMock;

    @Mock
    private ISimulatorsHandler simulatorsHandlerMock;

    @Mock
    private IMessageHandler messageHandlerMock;

    @BeforeClass
    public static void initSuite() {
        java.util.Date date = new java.util.Date();
        userObj = new User();
        userObj.setEmail("test@test.com");
        userObj.setUsername("username_test_create_controller" + date.getTime());
        userObj.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(createController).setSingleView(view).build();
        reset(configHandlerMock);
        reset(messageHandlerMock);
        reset(simulatorsHandlerMock);
    }

    @Test
    public void tc001RunConfig() throws Exception {
        ArrayList<String> availableSimulators = new ArrayList<String>();
        when(simulatorsHandlerMock.getSimulators()).thenReturn(availableSimulators);
        mockMvc.perform(
                get("/runConfig").param("id", "1").with(user(userObj.getUsername()).password(userObj.getPassword())))
                .andExpect(status().isOk()).andExpect(view().name("run"))
                .andExpect(model().attribute("simulators", availableSimulators))
                .andExpect(model().attribute("idConfig", 1));
        verify(simulatorsHandlerMock, times(1)).getSimulators();
        verifyNoMoreInteractions(simulatorsHandlerMock);
        verifyNoMoreInteractions(configHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
    }

    @Test
    public void tc002Create() throws Exception {
        ArrayList<String> availableSimulators = new ArrayList<String>();
        when(simulatorsHandlerMock.getSimulators()).thenReturn(availableSimulators);
        mockMvc.perform(get("/create").with(user(userObj.getUsername()).password(userObj.getPassword())))
                .andExpect(status().isOk()).andExpect(view().name("create"))
                .andExpect(model().attribute("simulators", availableSimulators));
        verify(simulatorsHandlerMock, times(1)).getSimulators();
        verifyNoMoreInteractions(simulatorsHandlerMock);
        verifyNoMoreInteractions(configHandlerMock);
        verifyNoMoreInteractions(messageHandlerMock);
    }

    @Test
    public void tc003CreateConfigWithRunSuccess() throws Exception {
        when(configHandlerMock.addConfigToDB("", "", "", "")).thenReturn(1);
        when(messageHandlerMock.getMessage("config.creation.success")).thenReturn("config.creation.success");
        mockMvc.perform(post("/createConfig").param("simulation_config", "").param("environment_config", "")
                .param("foram_config", "").param("tags", "").param("run", "true")
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("redirect:/runConfig?id=1"))
                .andExpect(flash().attribute("success", "config.creation.success"));
        verifyNoMoreInteractions(simulatorsHandlerMock);
        verify(configHandlerMock, times(1)).addConfigToDB("", "", "", "");
        verifyNoMoreInteractions(configHandlerMock);
        verify(messageHandlerMock, times(1)).getMessage("config.creation.success");
        verifyNoMoreInteractions(messageHandlerMock);
    }

    @Test
    public void tc004CreateConfigWithoutRunSuccess() throws Exception {
        when(configHandlerMock.addConfigToDB("", "", "", "")).thenReturn(1);
        when(messageHandlerMock.getMessage("config.creation.success")).thenReturn("config.creation.success");
        mockMvc.perform(post("/createConfig").param("simulation_config", "").param("environment_config", "")
                .param("foram_config", "").param("tags", "").param("run", "false")
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("redirect:/configs"))
                .andExpect(flash().attribute("success", "config.creation.success"));
        verifyNoMoreInteractions(simulatorsHandlerMock);
        verify(configHandlerMock, times(1)).addConfigToDB("", "", "", "");
        verifyNoMoreInteractions(configHandlerMock);
        verify(messageHandlerMock, times(1)).getMessage("config.creation.success");
        verifyNoMoreInteractions(messageHandlerMock);
    }

    @Test
    public void tc005CreateConfigWithRunFailure() throws Exception {
        when(configHandlerMock.addConfigToDB("", "", "", "")).thenReturn(-1);
        when(messageHandlerMock.getMessage("config.creation.failure")).thenReturn("config.creation.failure");
        mockMvc.perform(post("/createConfig").param("simulation_config", "").param("environment_config", "")
                .param("foram_config", "").param("tags", "").param("run", "true")
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("redirect:/configs"))
                .andExpect(flash().attribute("error", "config.creation.failure"));
        verifyNoMoreInteractions(simulatorsHandlerMock);
        verify(configHandlerMock, times(1)).addConfigToDB("", "", "", "");
        verifyNoMoreInteractions(configHandlerMock);
        verify(messageHandlerMock, times(1)).getMessage("config.creation.failure");
        verifyNoMoreInteractions(messageHandlerMock);
    }

    @Test
    public void tc006CreateConfigWithoutRunFailure() throws Exception {
        when(configHandlerMock.addConfigToDB("", "", "", "")).thenReturn(-1);
        when(messageHandlerMock.getMessage("config.creation.failure")).thenReturn("config.creation.failure");
        mockMvc.perform(post("/createConfig").param("simulation_config", "").param("environment_config", "")
                .param("foram_config", "").param("tags", "").param("run", "false")
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(view().name("redirect:/configs"))
                .andExpect(flash().attribute("error", "config.creation.failure"));
        verifyNoMoreInteractions(simulatorsHandlerMock);
        verify(configHandlerMock, times(1)).addConfigToDB("", "", "", "");
        verifyNoMoreInteractions(configHandlerMock);
        verify(messageHandlerMock, times(1)).getMessage("config.creation.failure");
        verifyNoMoreInteractions(messageHandlerMock);
    }
}