package controllers;

import app.model.Chart;
import app.model.Simulation;
import app.model.User;
import app.simulator.IChartHandler;
import app.simulator.IFileHandler;
import app.simulator.ISimulationHandler;
import app.svc.web.simulator.OutputController;

import org.json.JSONObject;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.View;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import javax.servlet.Filter;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { app.spring.WebConfig.class })
@WebAppConfiguration
public class OutputTest {

    @Autowired
    private Filter springSecurityFilterChain;

    private MockMvc mockMvc;
    private static User userObj;

    @InjectMocks
    private OutputController outputController;

    @Mock
    View view;

    @Mock
    private ISimulationHandler simulationHandler;

    @Mock
    private IFileHandler fileHandlerMock;

    @Mock
    private IChartHandler chartHandlerMock;
    
    @BeforeClass
    public static void initSuite() {
        java.util.Date date = new java.util.Date();
        userObj = new User();
        userObj.setEmail("test@test.com");
        userObj.setUsername("username_test_output_controller" + date.getTime());
        userObj.setPassword("password_test");
    }

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(outputController).setSingleView(view)
                .addFilters(springSecurityFilterChain).build();
        reset(fileHandlerMock);
        reset(simulationHandler);
        reset(chartHandlerMock);
    }

    @Test
    public void tc001GetOutputJSON() throws Exception {
        String output = "output";
        int progressPercentage = 50;
        Simulation simulation = new Simulation(1, "", 1, 1, "");
        simulation.setProgressPercentage(progressPercentage);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("progress", progressPercentage);
        jsonObject.put("output", output);
        jsonObject.put("status", simulation.getSimulationStatus().toString());
        jsonObject.put("canStop", simulation.getSimulationStatus().getCanStop());
        jsonObject.put("charts", new HashMap<String, String>());
        when(chartHandlerMock.getChartsForSimulation(1)).thenReturn(new ArrayList<Chart>());
        when(fileHandlerMock.getSimulationOutput(1)).thenReturn(output);
        when(simulationHandler.getSimulationWithId(1)).thenReturn(simulation);
        mockMvc.perform(get("/outputJSON").param("id", "1").with(csrf())
                .with(user(userObj.getUsername()).password(userObj.getPassword()))).andExpect(status().isOk())
                .andExpect(content().string(jsonObject.toString()));
        verify(fileHandlerMock, times(1)).getSimulationOutput(1);
        verifyNoMoreInteractions(fileHandlerMock);
        verify(simulationHandler, times(1)).getSimulationWithId(1);
        verifyNoMoreInteractions(simulationHandler);
        verify(chartHandlerMock, times(1)).getChartsForSimulation(1);
        verifyNoMoreInteractions(chartHandlerMock);
    }

}