package controllers;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.View;

import app.exceptions.EmailExistsException;
import app.exceptions.UsernameExistsException;
import app.exceptions.WrongSimulationReportPayloadException;
import app.model.SimulationReport;
import app.model.User;
import app.model.dataTransferObjects.UserDto;
import app.simulator.ISimulatorsHandler;
import app.svc.web.simulator.SimulatorsController;
import app.svc.web.user.IRegisterService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { app.spring.WebConfig.class })
@WebAppConfiguration
public class SimulatorsTest {

    @Autowired
    private Filter springContextSecurityFilter;

    private MockMvc mockMvc;
    private static UserDto userObj;
    private static User user;
    @Autowired
    private IRegisterService registerService;

    @Mock
    private View view;
    
    @Mock
    private ISimulatorsHandler simulatorHandlerMock;

    @InjectMocks
    private SimulatorsController simulatorsController;

    @BeforeClass
    public static void initSuite() {
        java.util.Date date = new java.util.Date();
        userObj = new UserDto();
        userObj.setEmail("testsimulators@test.com");
        userObj.setUsername("simulators_controller" + date.getTime());
        userObj.setPassword("password_test");
        userObj.setMatchingPassword(userObj.getPassword());
    }

    @Before
    public void initTestCase() throws EmailExistsException, UsernameExistsException {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(simulatorsController).setSingleView(view)
                .addFilters(springContextSecurityFilter).build();
        user = registerService.register(userObj);
        reset(simulatorHandlerMock);
    }

    @After
    public void cleanupTestCase() {
        registerService.deleteUser(user);
    }

    @Test
    public void tc001SimulationProgressReportFailure() throws Exception {
        JSONObject object = new JSONObject();
        object.put("simulatorIP", "");
        object.put("reportType", "");
        object.put("message", "");
        object.put("idSimulation", 1);
        object.put("fileContent", "");
        doThrow(new WrongSimulationReportPayloadException()).when(simulatorHandlerMock).handleReport(any(SimulationReport.class));
        mockMvc.perform(post("/simulationReport").with(csrf()).content(object.toString()))
        .andExpect(status().isExpectationFailed());
        verify(simulatorHandlerMock, times(1)).handleReport(any(SimulationReport.class));
        verifyNoMoreInteractions(simulatorHandlerMock);
    }
    
    @Test
    public void tc002SimulationProgressReportSuccess() throws Exception {
        JSONObject object = new JSONObject();
        object.put("simulatorIP", "");
        object.put("reportType", "");
        object.put("message", "");
        object.put("idSimulation", 1);
        object.put("fileContent", "");
        mockMvc.perform(post("/simulationReport").with(csrf()).content(object.toString()))
        .andExpect(status().isOk());
        verify(simulatorHandlerMock, times(1)).handleReport(any(SimulationReport.class));
        verifyNoMoreInteractions(simulatorHandlerMock);
    }

}