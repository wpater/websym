package app.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserUtils {
    
    public static String getLoggedUserName(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if ( authentication == null)
            return null;
        return authentication.getName();
    }
    
    public static boolean isLoggedUserAdmin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if( authentication == null || authentication.getAuthorities() == null)
            return false;
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            if ("ROLE_ADMIN".equals(authority.getAuthority()))
                return true;
        }
        return false;
    }
    
}
