package app.charts;

import lombok.Getter;

public enum ChartType {
    POPULATION_CHART("populationChart.html"),
    BORN_DEAD_CHART("bornDeadChart.html"),
    ENERGY_CHART("energyChart.html");

    ChartType(String fileName) {
        this.fileName = fileName;
    }

    @Getter
    public String fileName;

}
