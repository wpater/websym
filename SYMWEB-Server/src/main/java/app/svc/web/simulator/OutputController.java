package app.svc.web.simulator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import app.charts.ChartType;
import app.exceptions.UnauthorizedException;
import app.model.Chart;
import app.model.Simulation;
import app.simulator.IChartHandler;
import app.simulator.IFileHandler;
import app.simulator.IMessageHandler;
import app.simulator.ISimulationHandler;

@Controller
public class OutputController {

    @Autowired
    IFileHandler fileHandler;
    
    @Autowired
    IChartHandler chartHandler;
    
    @Autowired
    ISimulationHandler simulationHandler;
    
    @Autowired
    IMessageHandler messageHandler;
    
    @Secured("ROLE_USER")
    @RequestMapping(value = "/outputJSON", method = RequestMethod.GET)
    public @ResponseBody String downloadOutputJSON(@RequestParam("id") int id) throws UnauthorizedException, IOException {
        JSONObject obj = new JSONObject();
        Simulation simulation = simulationHandler.getSimulationWithId(id);
        obj.put("progress", simulation.getProgressPercentage());
        obj.put("status", simulation.getSimulationStatus().toString());
        obj.put("output", fileHandler.getSimulationOutput(id));
        obj.put("canStop", simulation.getSimulationStatus().getCanStop());
        HashMap<String, String> chartIdToTypeMap = new HashMap<>();
        ArrayList<Chart> charts = chartHandler.getChartsForSimulation(id);
        for(Chart chart : charts){
            chartIdToTypeMap.put(Integer.toString(chart.getIdChart()), messageHandler.getMessage("simulation.details.chart."+chart.getChartType().toString()));
        }
        obj.put("charts", chartIdToTypeMap);
        return obj.toString();
    }

}
