package app.svc.web.simulator;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import app.exceptions.UnauthorizedException;
import app.model.Chart;
import app.model.Simulation;
import app.simulator.IChartHandler;
import app.simulator.ICommentHandler;
import app.simulator.IFileHandler;
import app.simulator.IMessageHandler;
import app.simulator.ISimulationHandler;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SimulationsController {
    private final ISimulationHandler simulationHandler;
    private final ICommentHandler commentHandler;
    private final IMessageHandler messageHandler;
    private final IFileHandler fileHandler;
    private final IChartHandler chartHandler;

    @Secured("ROLE_USER")
    @RequestMapping(value = { "/simulations" }, method = RequestMethod.GET)
    public ModelAndView simulationsPage(@RequestParam(value = "ongoing", required = false) Boolean ongoing,
            @RequestParam(value = "user", required = false) Integer idUser) throws UnauthorizedException {
        ModelAndView model = new ModelAndView("simulations");
        model.addObject("table", simulationHandler.getSimulations(idUser, ongoing));
        return model;
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = { "/configSimulations" }, method = RequestMethod.GET)
    public ModelAndView simulationsPage(@RequestParam("id") int idConfig) throws UnauthorizedException {
        ModelAndView model = new ModelAndView("simulations");
        model.addObject("table", simulationHandler.getConfigSimulations(idConfig));
        return model;
    }

    @Secured("ROLE_USER")
    @RequestMapping(value="/simulations/{id}", method = RequestMethod.GET)
    public String simulationInfoPage(@PathVariable("id") int id, Model model) throws UnauthorizedException, IOException {
        model.addAttribute("comments", commentHandler.getCommentsForSimulation(id));
        Simulation simulation = simulationHandler.getSimulationWithId(id);
        model.addAttribute("simulation", simulation);
        model.addAttribute("output", fileHandler.getSimulationOutput(id));
        ArrayList<Chart> charts = chartHandler.getChartsForSimulation(simulation.getIdSimulation());
        if(charts != null && charts.size() > 0){
            model.addAttribute("charts", charts);
        }
        return "simulationInfo";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/stopSimulation/{id}", method = RequestMethod.POST)
    public String stopSimulation(@PathVariable("id") int id, RedirectAttributes redirectAttributes)
            throws UnauthorizedException {
        boolean succesful = simulationHandler.stopSimulation(id);
        redirectAttributes.addFlashAttribute(succesful ? "success" : "error",
                messageHandler.getMessage("simulation.stop." + (succesful ? "success" : "failure")));
        return "redirect:/simulations/" + id;
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public String addCommentPage(@RequestParam("id") int id, @RequestParam("content") String content,
            RedirectAttributes redirectAttributes) throws UnauthorizedException {
        commentHandler.addComment(id, content);
        redirectAttributes.addFlashAttribute("success", messageHandler.getMessage("comment.added"));
        return "redirect:/simulations/" + id;
    }
}
