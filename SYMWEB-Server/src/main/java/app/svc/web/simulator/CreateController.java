package app.svc.web.simulator;

import java.io.IOException;

import javax.json.Json;
import javax.json.JsonObject;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import app.exceptions.UnauthorizedException;
import app.model.Config;
import app.model.Simulation;
import app.simulator.IConfigHandler;
import app.simulator.IFileHandler;
import app.simulator.IMessageHandler;
import app.simulator.ISimulationHandler;
import app.simulator.ISimulatorsHandler;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CreateController {
    private final IConfigHandler configHandler;
    private final IFileHandler fileHandler;
    private final ISimulationHandler simulationHandler;
    private final ISimulatorsHandler simulatorsHandler;
    private final RestTemplate sslRestTemplate;
    private final IMessageHandler messageHandler;
    
    @Secured("ROLE_USER")
    @RequestMapping(value = "/createSimulationAndConfig", method = RequestMethod.POST)
    public String createConfigAndSimulation(@RequestParam("foram_config") MultipartFile foramConfigFile,
            @RequestParam("simulation_config") MultipartFile simulationConfigFile,
            @RequestParam("environment_config") MultipartFile environmentConfigFile, @RequestParam("tags") String tags,
            @RequestParam("machine") String machine,
            RedirectAttributes redirectAttributes) throws UnauthorizedException, IOException {
        String simulationConfigText = new String(simulationConfigFile.getBytes());
        String environmentConfigText = new String(environmentConfigFile.getBytes());
        String foramConfigText = new String(foramConfigFile.getBytes());
        int idConfig = configHandler.addConfigToDB(simulationConfigText, environmentConfigText, foramConfigText, tags);
        if (idConfig < 0) {
            redirectAttributes.addFlashAttribute("error", messageHandler.getMessage("config.creation.failure"));
            return "redirect:/create";
        }
        redirectAttributes.addFlashAttribute("success", messageHandler.getMessage("config.creation.success"));
        // Empty tags, as they will be shown in simulations view anyways
        // since they're already assigned to config
        return createSimulationImpl(idConfig, machine, "", redirectAttributes, "create");
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/createConfig", method = RequestMethod.POST)
    public String createConfig(@RequestParam("foram_config") String foramConfigText,
            @RequestParam("simulation_config") String simulationConfigText,
            @RequestParam("environment_config") String environmentConfigText, @RequestParam("tags") String tags,
            @RequestParam("run") boolean run, RedirectAttributes redirectAttributes) throws UnauthorizedException, IOException {
        int idConfig = configHandler.addConfigToDB(simulationConfigText, environmentConfigText, foramConfigText, tags);
        if (idConfig < 0) {
            redirectAttributes.addFlashAttribute("error", messageHandler.getMessage("config.creation.failure"));
            return "redirect:/configs";
        }
        redirectAttributes.addFlashAttribute("success", messageHandler.getMessage("config.creation.success"));
        if (!run) {
            return "redirect:/configs";
        } 
        return "redirect:/runConfig?id=" + idConfig;
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/runConfig", method = RequestMethod.GET)
    public String runConfig(@RequestParam("id") int idConfig, Model model) {
        model.addAttribute("simulators", simulatorsHandler.getSimulators());
        model.addAttribute("idConfig", idConfig);
        return "run";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/createSimulation", method = RequestMethod.POST)
    public String createSimulation(@RequestParam("idConfig") int idConfig,
            @RequestParam("machine") String machine, @RequestParam("tags") String tags, RedirectAttributes redirectAttributes)
            throws UnauthorizedException, IOException {
        return createSimulationImpl(idConfig, machine, tags, redirectAttributes, "runConfig?id=" + idConfig);
    }

    private String createSimulationImpl(int idConfig, String machine, String tagsString, RedirectAttributes redirectAttributes, String redirectFailureView)
            throws UnauthorizedException, IOException {
        String simulatorAddress = machine + ":8181";
        Config config = configHandler.getConfig(idConfig);
        int idSimulation = simulationHandler.createSimulation(config, simulatorAddress, tagsString);
        if (idSimulation >= 0) {
            Simulation simulation = simulationHandler.getSimulationWithId(idSimulation);
            JsonObject jo = Json.createObjectBuilder().add("config", Json.createObjectBuilder()
                    .add("foram", new String(fileHandler.readAllBytesFromFile(config.getForam_config())))
                    .add("environment", new String(fileHandler.readAllBytesFromFile(config.getEnvironment_config())))
                    .add("simulation", new String(fileHandler.readAllBytesFromFile(config.getSimulation_config()))))
                    .add("simulationId", simulation.getIdSimulation()).build();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<>(jo.toString(), headers);
            ResponseEntity<String> loginResponse = sslRestTemplate.exchange(
                    "https://" + simulatorAddress + "/startSimulation", HttpMethod.POST, entity, String.class);
            if (loginResponse.getStatusCode() == HttpStatus.OK) {
                JSONObject object = new JSONObject(loginResponse.getBody());
                if (object.getBoolean("started")) {
                    int simulationServerIdSimulation = object.getInt("localIdSimulation");
                    simulationHandler.updateSimulationSimulationServerIdSimulation(simulation.getIdSimulation(),
                            simulationServerIdSimulation);
                    redirectAttributes.addFlashAttribute("success", messageHandler.getMessage("simulation.creation.success"));
                    return "redirect:/simulations?ongoing=true";
                } else {
                    simulationHandler.deleteSimulation(simulation.getIdSimulation());
                    redirectAttributes.addFlashAttribute("error", messageHandler.getMessage("simulation.creation.failure. " + object.getString("message")));
                    return "redirect:/" + redirectFailureView;
                }
            } else {
                simulationHandler.deleteSimulation(simulation.getIdSimulation());
                redirectAttributes.addFlashAttribute("error", messageHandler.getMessage("simulation.creation.status.error", new Object[]{loginResponse.getStatusCode()}));
                return "redirect:/" + redirectFailureView;
            }
        } else {
            redirectAttributes.addFlashAttribute("error", messageHandler.getMessage("simulation.creation.failure"));
            return "redirect:/" + redirectFailureView;
        }
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String showCreate(Model model) {
        model.addAttribute("simulators", simulatorsHandler.getSimulators());
        return "create";
    }
}
