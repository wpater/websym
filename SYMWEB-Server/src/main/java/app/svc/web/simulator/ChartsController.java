package app.svc.web.simulator;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.exceptions.UnauthorizedException;
import app.model.Chart;
import app.simulator.IChartHandler;
import app.simulator.IFileHandler;

@Controller
public class ChartsController {

    @Autowired
    IChartHandler chartHandler;

    @Autowired
    IFileHandler fileHandler;
    
    @Secured("ROLE_USER")
    @RequestMapping(value = "/chart/{id}", method = RequestMethod.GET)
    public String chartDisplayPage(@PathVariable("id") int id, Model model) throws UnauthorizedException, IOException {
        Chart chart = chartHandler.getChart(id);
        if (chart != null) {
            model.addAttribute("chartContent", new String(fileHandler.readAllBytesFromFile(fileHandler.getChartPath(id))));
            model.addAttribute("chartType", chart.getChartType());
        }
        return "chart";
    }

}
