package app.svc.web.simulator;

import java.io.IOException;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.exceptions.WrongSimulationReportPayloadException;
import app.model.SimulationReport;
import app.simulator.ISimulatorsHandler;

@RestController
public class SimulatorsController {

    @Autowired
    private ISimulatorsHandler simulators;

    // TODO: Secure it somehow?

    @RequestMapping(value = "/simulators", method = RequestMethod.POST)
    public ResponseEntity<Void> addSimulator(@RequestBody String simulatorIP) {
        simulators.add(simulatorIP);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/simulationReport", method = RequestMethod.POST)
    public ResponseEntity<Void> simulationProgressReport(@RequestBody String reportJSON) {
        SimulationReport simulationReport;
        JSONObject obj = new JSONObject(reportJSON);
        String simulatorIP, reportType, message = null, fileContent = null;
        int idSimulation;
        simulatorIP = obj.getString("simulatorIP");
        reportType = obj.getString("reportType");
        if(!obj.isNull("message")){
            message = obj.getString("message");   
        }
        if (!obj.isNull("fileContent")) {
            fileContent = obj.getString("fileContent");
        }
        idSimulation = obj.getInt("idSimulation");
        simulationReport = new SimulationReport(idSimulation, simulatorIP, reportType, message, fileContent);
        try {
            simulators.handleReport(simulationReport);
        } catch (WrongSimulationReportPayloadException | IOException e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
