package app.svc.web.simulator;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import app.exceptions.UnauthorizedException;
import app.model.Config;
import app.simulator.IConfigHandler;
import app.simulator.IFileHandler;
import app.simulator.IMessageHandler;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ConfigsController {
    private final IConfigHandler configHandler;
    private final IFileHandler fileHandler;
    private final IMessageHandler messageHandler;
    
    @Secured("ROLE_USER")
    @RequestMapping(value = "/configs", method = RequestMethod.GET)
    public String showConfigs(Model model, @RequestParam(value = "user", required = false) Integer idUser) throws UnauthorizedException {
        model.addAttribute("table", configHandler.getConfigsForUser(idUser));
        return "configs";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/deleteConfig", method = RequestMethod.POST)
    public String deleteConfig(@RequestParam("id") int idConfig, RedirectAttributes redirectAttributes) throws UnauthorizedException, IOException {
        Config config = configHandler.deleteConfig(idConfig);
        redirectAttributes.addFlashAttribute("success", messageHandler.getMessage("config.deleted"));
        redirectAttributes.addAttribute("user", config.getIdUser());
        return "redirect:/configs";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/loadConfig", method = RequestMethod.GET)
    public String loadConfig(@RequestParam("id") int idConfig, RedirectAttributes redirectAttributes) throws UnauthorizedException, IOException {
        Config cfg = configHandler.getConfig(idConfig);
        String simulationPath = cfg.getSimulation_config();
        String foramPath = cfg.getForam_config();
        String environmentPath = cfg.getEnvironment_config();
        redirectAttributes.addFlashAttribute("success", messageHandler.getMessage("config.loaded"));
        redirectAttributes.addFlashAttribute("foram", new String(fileHandler.readAllBytesFromFile(foramPath)));
        redirectAttributes.addFlashAttribute("simulation", new String(fileHandler.readAllBytesFromFile(simulationPath)));
        redirectAttributes.addFlashAttribute("environment", new String(fileHandler.readAllBytesFromFile(environmentPath)));
        redirectAttributes.addAttribute("user", cfg.getIdUser());
        return "redirect:/configs";
    }
}
