package app.svc.web.user;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.exceptions.UnauthorizedException;
import app.simulator.ISimulationHandler;
import app.spring.database.IUserMapper;


@Controller
public class AdminController {

    @Autowired
    ISimulationHandler simulationHandler;
    
    @Autowired
    IUserMapper userMapper;
    
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String loginPage(ServletRequest request, Model model) throws UnauthorizedException {
        model.addAttribute("simulations", simulationHandler.getSimulationsInProgress());
        model.addAttribute("users", userMapper.getUsers());
        return "admin";
    }
    
}
