package app.svc.web.user;

import app.exceptions.EmailExistsException;
import app.exceptions.UsernameExistsException;
import app.model.User;
import app.model.dataTransferObjects.UserDto;
import app.spring.database.IUserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service("registerService")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RegisterServiceImpl implements IRegisterService {
    private final IUserMapper userMapper;

    @Autowired
    PasswordEncoder passwordEncoder;

    public void deleteUser(User user){
        userMapper.deleteUser(user.getUsername());
    }

    @Override
    public User register(UserDto userDto) throws EmailExistsException, UsernameExistsException {
        if(userMapper.selectByEmail(userDto.getEmail()) != null)
            throw new EmailExistsException();
        if(userMapper.selectByName(userDto.getUsername()) != null)
            throw new UsernameExistsException();
        User user = new User(-1, true, userDto.getEmail(), passwordEncoder.encode(userDto.getPassword()), userDto.getUsername());
        userMapper.insertUser(user);
        userMapper.setRoleUser(user.getIdUser());
        return user;
    }
}
