package app.svc.web.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import app.spring.database.IUserMapper;
import lombok.RequiredArgsConstructor;

@Service("loginService")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginServiceImpl implements UserDetailsService {
    private final IUserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        app.model.User user = userMapper.selectByName(login);
        if (user == null)
            throw new UsernameNotFoundException("User with given login does not exist.");
        app.model.Role role = userMapper.selectRoleById(user.getIdUser());

        List<SimpleGrantedAuthority> authList = new ArrayList<SimpleGrantedAuthority>();
        authList.add(new SimpleGrantedAuthority("ROLE_USER"));
        if (role.getRolename().equals("ROLE_ADMIN"))
            authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        User springUser = new User(user.getUsername(), user.getPassword(), authList);
        return springUser;
    }
}
