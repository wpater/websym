package app.svc.web.user;

import app.exceptions.EmailExistsException;
import app.exceptions.UsernameExistsException;
import app.model.User;
import app.model.dataTransferObjects.UserDto;

public interface IRegisterService {
    User register(UserDto userDto) throws EmailExistsException, UsernameExistsException;
    void deleteUser(User user);
}
