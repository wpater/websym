package app.svc.web.user;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import app.model.dataTransferObjects.UserDto;
import app.simulator.IMessageHandler;

@Controller
public class LoginController {
    
    @Autowired
    IMessageHandler messageHandler;
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(ServletRequest request, Model model) {
        if (!model.containsAttribute("registerUser"))
            model.addAttribute("registerUser", new UserDto());
        if(request.getParameterMap().containsKey("error")){
            model.addAttribute("error", messageHandler.getMessage("invalid.username.or.password"));
        }
        if(request.getParameterMap().containsKey("success")){
            model.addAttribute("success", messageHandler.getMessage("login.successful"));
        } else {
            model.addAttribute("loginRequest", true);
        }
        return "index";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response,
            RedirectAttributes redirectAttributes) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        redirectAttributes.addFlashAttribute("success", messageHandler.getMessage("logged.out.message"));
        return "redirect:/home";
    }

    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    private String getPrincipal() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
}
