package app.svc.web.user;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import app.exceptions.EmailExistsException;
import app.exceptions.UsernameExistsException;
import app.model.dataTransferObjects.UserDto;
import app.simulator.IMessageHandler;
import app.simulator.UserDtoValidator;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RegisterController {

    @Autowired
    UserDtoValidator userDtoValidator;
    
    @Autowired
    IMessageHandler messageHandler;
    
    private final IRegisterService registerService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String processRegistration(@ModelAttribute("registerUser") UserDto registerUser, HttpServletRequest request,
            BindingResult binding, RedirectAttributes redirectAttributes) {
        userDtoValidator.validate(registerUser, binding);
        if (binding.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.registerUser", binding);
            redirectAttributes.addFlashAttribute("registerUser", registerUser);
            redirectAttributes.addFlashAttribute("registerSuccessful", false);
            redirectAttributes.addFlashAttribute("error", messageHandler.getMessage("register.failure"));
            return "redirect:/home";
        }
        try {
            registerService.register(registerUser);
        } catch (EmailExistsException e) {
            binding.rejectValue("email", "registerForm.email.taken");
        } catch (UsernameExistsException e) {
            binding.rejectValue("username", "registerForm.username.taken");
        }
        if (binding.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.registerUser", binding);
            redirectAttributes.addFlashAttribute("registerUser", registerUser);
            redirectAttributes.addFlashAttribute("registerSuccessful", false);
            redirectAttributes.addFlashAttribute("error", messageHandler.getMessage("register.failure"));
            return "redirect:/home";
        }
        try {
            request.login(registerUser.getUsername(), registerUser.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        redirectAttributes.addFlashAttribute("registerSuccessful", true);
        redirectAttributes.addFlashAttribute("success", messageHandler.getMessage("register.success"));
        return "redirect:/home";
    }
}
