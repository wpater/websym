package app.svc.web.home;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.exceptions.UnauthorizedException;
import app.model.dataTransferObjects.UserDto;

@Controller
@RequestMapping(value = { "/", "/home" })
public class HomeController {
    @RequestMapping(method = RequestMethod.GET)
    public String homePage(Model model) throws UnauthorizedException {
        if (!model.containsAttribute("registerUser"))
            model.addAttribute("registerUser", new UserDto());
        return "index";
    }
}
