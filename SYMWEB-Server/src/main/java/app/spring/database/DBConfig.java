package app.spring.database;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

@Service("DBConfig")
@PropertySource("classpath:db.properties")
public class DBConfig {
    
    private @Value("${jdbc.driverclassname}") String databaseDriverClassName;
    private @Value("${jdbc.url}") String databaseUrl;
    private @Value("${jdbc.username}") String databaseUsername;
    private @Value("${jdbc.password}") String databasePassword;
    
    @Bean(name = "dataSource")
    public DataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(databaseDriverClassName);
        driverManagerDataSource.setUrl(databaseUrl);
        driverManagerDataSource.setUsername(databaseUsername);
        driverManagerDataSource.setPassword(databasePassword);
        return driverManagerDataSource;
    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean() {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setConfigLocation(new ClassPathResource("mybatisConfig.xml"));
        sqlSessionFactoryBean.setMapperLocations(new Resource[]{
                new ClassPathResource("UserMapper.xml"),
                new ClassPathResource("SimulationMapper.xml")
        });
        sqlSessionFactoryBean.setDataSource(dataSource());
        return sqlSessionFactoryBean;
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        return sqlSessionFactoryBean().getObject();
    }

    @Bean
    public MapperFactoryBean mappers() throws Exception {
        MapperFactoryBean mapperFactoryBean = new MapperFactoryBean();
        mapperFactoryBean.setMapperInterface(IUserMapper.class);
        mapperFactoryBean.setMapperInterface(ISimulationMapper.class);
        mapperFactoryBean.setSqlSessionFactory(sqlSessionFactory());
        return mapperFactoryBean;
    }

    @Bean
    public IUserMapper userMapper() throws Exception {
        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(sqlSessionFactoryBean().getObject());
        return sessionTemplate.getMapper(IUserMapper.class);
    }

    @Bean
    public ISimulationMapper simMapper() throws Exception {
        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(sqlSessionFactoryBean().getObject());
        return sessionTemplate.getMapper(ISimulationMapper.class);
    }
}
