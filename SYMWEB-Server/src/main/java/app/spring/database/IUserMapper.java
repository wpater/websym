package app.spring.database;

import java.util.ArrayList;

import app.model.Role;
import app.model.User;

public interface IUserMapper {
    void insertUser(User user);
    int getIdUser(String username);
    void setRoleUser(int id);
    User selectByName(String name);
    User selectByEmail(String email);
    User selectById(int id);
    Role selectRoleByName(String name);
    Role selectRoleById(int id);
    void deleteUser(String username);
    ArrayList<User> getUsers();
}
