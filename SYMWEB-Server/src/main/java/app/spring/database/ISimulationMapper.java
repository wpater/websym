package app.spring.database;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import app.model.Chart;
import app.model.Comment;
import app.model.Config;
import app.model.ConfigTag;
import app.model.Simulation;
import app.model.Simulation.SimulationStatus;
import app.model.SimulationTag;
import app.model.Tag;

public interface ISimulationMapper {
    // Insert
    void insertSimulation(Simulation sim);
    void insertConfig(Config cfg);
    void insertTag(Tag tag);
    void insertConfigTag(ConfigTag ct);
    void insertSimulationTag(SimulationTag simulationTag);    
    void insertComment(Comment comment);
    void insertChart(Chart chart);
    // Update
    void updateConfig(Config cfg);
    void updateSimulation(Simulation sim);
    void updateSimulationPercentage(@Param("idSimulation") int idSimulation, @Param("progressPercentage") int progressPercentage);
    void updateSimulationStatus(@Param("idSimulation") int idSimulation, @Param("simulationStatus") SimulationStatus simulationStatus);
    void updateSimulationSimulationServerIdSimulation(@Param("idSimulation") int idSimulation, @Param("simulationServerIdSimulation") int simulationServerIdSimulation);
    // Get
    Simulation getSimulation(int idSimulation);
    int getIdTagWithName(String name);
    ArrayList<Simulation> getConfigSimulations(int idConfig);
    ArrayList<Simulation> getUserSimulations(int idUser);
    ArrayList<Simulation> getUserSimulationsWithStatus(@Param("idUser") int idUser, @Param("statuses") SimulationStatus[] statuses);
    ArrayList<Simulation> getSimulationsWithStatus(@Param("statuses") SimulationStatus[] statuses);
    ArrayList<Config> getConfigsForUser(int idUser);
    Config getConfig(int idConfig);
    ArrayList<Comment> getCommentsForSimulation(int idSimulation);
    Chart getChart(int idChart);
    ArrayList<Chart> getChartsForSimulation(int idSimulation);
    // Delete
    void deleteConfig(int idConfig);
    void deleteSimulation(int idSimulation);
    void deleteConfigTag(int idConfig);
    void deleteSimulationTag(int idSimulation);
    void deleteSimulationComments(int idSimulation);
    void deleteChartsForSimulation(int idSimulation);
}
