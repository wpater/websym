package app.spring;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import app.spring.database.IUserMapper;
import app.svc.web.user.LoginServiceImpl;

@Configuration
@EnableWebSecurity
@ComponentScan({"app"})
@PropertySource("classpath:server.properties")
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${javax.net.ssl.keyStore}")
    public void setKeyStore(String keyStore) {
        System.setProperty("javax.net.ssl.keyStore", keyStore);
    }
    @Value("${javax.net.ssl.keyStorePassword}")
    public void setKeyStorePassword(String keyStorePassword) {
        System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);
    }
    @Value("${javax.net.ssl.keyStoreType}")
    public void setKeyStoreType(String keyStoreType) {
        System.setProperty("javax.net.ssl.keyStoreType", keyStoreType);
    }
    @Value("${javax.net.ssl.trustStore}")
    public void setTrustStore(String trustStore) {
        System.setProperty("javax.net.ssl.trustStore", trustStore);
    }
    @Value("${javax.net.ssl.trustStorePassword}")
    public void setTrustStorePassword(String trustStorePassword) {
        System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
    }
    @Value("${javax.net.ssl.trustStoreType}")
    public void setTrustStoreType(String trustStoreType) {
        System.setProperty("javax.net.ssl.trustStoreType", trustStoreType);
    }

    
    // Disabled due to problem with POST from simulators. Issue #7
    // TODO: Try to add tokken for posts instead of disabling csrf
    @Configuration
    @Order(1)
    public static class configureSimulators extends WebSecurityConfigurerAdapter {
        protected void configure(HttpSecurity http) throws Exception {
            http.requestMatchers().antMatchers("/simulators","/simulationReport").and()
                    .csrf().disable()
                    .httpBasic();
        }
    }

	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/home", "/login", "/register", "/logout", "/static/**").permitAll()
                .antMatchers("/**").fullyAuthenticated()
                .and().formLogin().loginPage("/login").defaultSuccessUrl("/login?success").failureUrl("/login?error")
                .usernameParameter("ssoId").passwordParameter("password")
                .and().csrf()
                .and().exceptionHandling().accessDeniedPage("/accessDenied");
    }
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    auth.authenticationProvider(authProvider());
	}
	
	@Bean
	@Autowired
	public DaoAuthenticationProvider authProvider() {
	    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
	    authProvider.setUserDetailsService(new LoginServiceImpl(userMapper));
	    authProvider.setPasswordEncoder(passwordEncoder());
	    return authProvider;
	}
    
	
	
	@Bean
	public RestTemplate sslRestTemplate() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException{
        HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(SSLConnectionSocketFactory.getSystemSocketFactory()).build();
	    ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        return new RestTemplate(requestFactory);
	}
	
	
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    IUserMapper userMapper;

    @Autowired
    private Environment env;

    @Bean
    public String userConfigDir(){
        return env.getProperty("userConfigDir");
    }
}
