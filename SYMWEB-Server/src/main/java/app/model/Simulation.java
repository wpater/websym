package app.model;

import java.util.Collection;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Simulation {

    public enum SimulationStatus {
        PENDING(true), IN_PROGRESS(true), DONE(false), ERROR(false), ABORTED(false);
        boolean canStop;
        SimulationStatus(boolean canStop){
            this.canStop = canStop;
        }
        
        public boolean getCanStop(){
            return canStop;
        }
    }

    public Simulation(int idSimulation, String pathToResult, int idUser, int idConfig,
            String relatedSimulationAddress) {
        this.idSimulation = idSimulation;
        this.pathToResult = pathToResult;
        this.idUser = idUser;
        this.idConfig = idConfig;
        this.relatedSimulatorAddress = relatedSimulationAddress;
    }

    private int idSimulation;
    private String pathToResult;
    private int idUser;
    private int idConfig;
    private String relatedSimulatorAddress;
    private int simulationServerIdSimulation;
    private int progressPercentage = 0;
    private SimulationStatus simulationStatus = SimulationStatus.PENDING;
    private Collection<Tag> configTagsCollection;
    private Collection<Tag> simulationTagsCollection;
    
    
    private String configTagsString = "";
    public void setConfigTagsCollection(Collection<Tag> configTagsCollection) {
        this.configTagsCollection = configTagsCollection;
        configTagsString = Tag.createTagsString(configTagsCollection);
    }
    private String simulationTagsString = "";
    public void setSimulationTagsCollection(Collection<Tag> simulationTagsCollection){
        this.simulationTagsCollection = simulationTagsCollection;
        simulationTagsString = Tag.createTagsString(simulationTagsCollection);
    }
    
    public String getTagsString(){
        return configTagsString + simulationTagsString;
    }
    
    
}
