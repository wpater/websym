package app.model;

import app.charts.ChartType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Chart {

    private int idChart;
    private ChartType chartType;
    private int idSimulation;
    private long creationTimestamp;
    
}
