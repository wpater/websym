package app.model;

import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Tag {
    private int idTag;
    private String name;

    public static String createTagsString(Collection<Tag> tagsCollection) {
        String tagsString = "";
        if (tagsCollection != null) {
            for (Tag tag : tagsCollection) {
                tagsString += ("#" + tag.getName() + "; ");
            }
        }
        return tagsString;
    }

}
