package app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {
    private int idUser;
    private boolean enabled;
    @Email
    private String email;
    private String password;
    private String username;
}
