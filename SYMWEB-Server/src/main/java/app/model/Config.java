package app.model;

import java.util.Collection;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Config {

    public Config(int idConfig, String foram_config, String environment_config, String simulation_config,
            int idUser, Collection<Tag> tagsList) {
        this.idConfig = idConfig;
        this.foram_config = foram_config;
        this.environment_config = environment_config;
        this.simulation_config = simulation_config;
        this.idUser = idUser;
        setTagsCollection(tagsList);
    }

    private int idConfig;
    private String foram_config;
    private String environment_config;
    private String simulation_config;
    private int idUser;
    private Collection<Tag> tagsCollection;

    private String tagsString = "";
    
    public void setTagsCollection(Collection<Tag> tagsCollection) {
        this.tagsCollection = tagsCollection;
        tagsString = Tag.createTagsString(tagsCollection);
    }
}
