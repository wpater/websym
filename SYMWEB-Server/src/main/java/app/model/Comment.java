package app.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Comment {
    private int idComment;
    private int idSimulation;
    private String username;
    private String content;
    private Long addDateTimestamp;
    
    public Comment(int idSimulation, String username, String content, Long addDateTimestamp){
        this.idSimulation = idSimulation;
        this.username = username;
        this.content = content;
        this.addDateTimestamp = addDateTimestamp;
    }
    
    public Date getDate(){
        if(addDateTimestamp == null)
            return null;
        return new Date(addDateTimestamp);
    }
    
}
