package app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SimulationReport {

    private int idSimulation;
    private String simulatorAddress;
    private String reportType;
    private String message;
    private String fileContent;

}