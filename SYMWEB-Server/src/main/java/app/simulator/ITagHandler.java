package app.simulator;

public interface ITagHandler {
    public void createConfigTags(int idConfig, String tagsString);
    public void createSimulationTags(int idSimulation, String tagsString);
}
