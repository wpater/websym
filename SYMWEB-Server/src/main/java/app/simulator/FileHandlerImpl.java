package app.simulator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import app.charts.ChartType;
import app.exceptions.UnauthorizedException;
import app.model.Chart;
import app.model.Simulation;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import app.utils.UserUtils;

@Service("fileHandler")
public class FileHandlerImpl implements IFileHandler {
    
    @Autowired
    private String userConfigDir;

    @Autowired
    ISimulationMapper simMapper;

    @Autowired
    IUserMapper userMapper;
    
    @Autowired
    private ApplicationContext appContext;
    
    @Autowired
    ServletContext context; 
    
    @Override
    public byte[] readAllBytesFromFile(String path) throws IOException {
        return Files.readAllBytes(Paths.get(path));
    }

    @Override
    public void deleteConfigDirectory(String username, int idConfig) throws IOException {
        FileUtils.deleteDirectory(new File(getConfigDirPath(username, idConfig)));
    }
    
    @Override
    public void deleteSimulationDirectory(String username, int idSimulation) throws IOException {
        FileUtils.deleteDirectory(new File(getSimulationDirPath(username, idSimulation)));
    }

    @Override
    public boolean createPathToResultDirectory(String username, int idSimulation) {
        return new File(getSimulationResultDirPath(username, idSimulation)).mkdirs();
    }

    @Override
    public boolean saveUserConfigFiles(String username, int idConfig, String foramConfigFileContent,
            String simulationConfigFileContent, String environmentConfigFileContent) {
        boolean dirCreated = new File(getConfigDirPath(username, idConfig)).mkdirs();
        if (!dirCreated)
            return false;
        try {
            PrintWriter foramFile = new PrintWriter(getForamConfigFilePath(username, idConfig));
            PrintWriter simulationFile = new PrintWriter(getSimulationConfigFilePath(username, idConfig));
            PrintWriter environmentFile = new PrintWriter(getEnvironmentConfigFilePath(username, idConfig));
            // Foram config
            foramFile.print(foramConfigFileContent);
            foramFile.close();
            // Simulation config
            simulationFile.print(simulationConfigFileContent);
            simulationFile.close();
            // Environment config
            environmentFile.print(environmentConfigFileContent);
            environmentFile.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private String getUserDirPath(String username){
        return userConfigDir + File.separatorChar + username;
    }
    
    private String getSimulationDirPath(String username, int idSimulation) {
        return getUserDirPath(username) + File.separatorChar + "simulations" + File.separatorChar + idSimulation;
    }

    private String getConfigDirPath(String username, int idConfig){
        return getUserDirPath(username) + File.separatorChar + "configs" + File.separatorChar + idConfig;
    }

    @Override
    public String getForamConfigFilePath(String username, int idConfig) {
        return getConfigDirPath(username, idConfig) + File.separatorChar + "foram.js";
    }

    @Override
    public String getSimulationConfigFilePath(String username, int idConfig) {
        return getConfigDirPath(username, idConfig) + File.separatorChar + "simulation.js";

    }

    @Override
    public String getEnvironmentConfigFilePath(String username, int idConfig) {
        return getConfigDirPath(username, idConfig) + File.separatorChar + "environment.js";

    }

    @Override
    public String getSimulationResultDirPath(String username, int idSimulation) {
        return getSimulationDirPath(username, idSimulation) + File.separatorChar + "result";

    }

    @Override
    public void appendSimulationOutput(int idSimulation, String message) throws IOException {
        Simulation simulation = simMapper.getSimulation(idSimulation);
        String filePath = simulation.getPathToResult() + File.separatorChar + "output.txt";
        File file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
        writer.write(message);
        writer.newLine();
        writer.close();
    }

    @Override
    public String getSimulationOutput(int idSimulation) throws IOException, UnauthorizedException {
        String username = UserUtils.getLoggedUserName();
        if (username == null)
            throw new UnauthorizedException();
        boolean isAdmin = UserUtils.isLoggedUserAdmin();
        Simulation simulation = simMapper.getSimulation(idSimulation);
        int idUser = userMapper.getIdUser(username);
        if (!isAdmin && idUser != simulation.getIdUser())
            throw new UnauthorizedException();
        String filePath = simulation.getPathToResult() + File.separatorChar + "output.txt";
        File file = new File(filePath);
        if (!file.exists()) {
            return null;
        }
        String content = new String(Files.readAllBytes(Paths.get(filePath)));
        return content;

    }

	@Override
	public void saveChartContent(int idSimulation, ChartType chartType, String fileContent) throws IOException {
		Simulation simulation = simMapper.getSimulation(idSimulation);
        String filePath = simulation.getPathToResult() + File.separatorChar + chartType.getFileName();
        File file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(file, false));
        writer.write(fileContent);
        writer.newLine();
        writer.close();
	}
	
	private final String[] zipResourceFiles = new String[] {"js/jquery-1.11.2.min.js", "js/jsapi-visualization-1.1.min.js"};
	
	@Override
	public String zipChartsForSimulation(int idSimulation) throws IOException {
	    Simulation simulation = simMapper.getSimulation(idSimulation);
	    String filePath = simulation.getPathToResult() + File.separatorChar + "charts.zip";
	    FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);
        byte[] buffer = new byte[1024];
	    for(String filename : zipResourceFiles){
	        Resource resource =
	                appContext.getResource("file:" + context.getRealPath("/") + "/static/" + filename);
	        InputStream stream = resource.getInputStream();
	        ZipEntry zipEntry = new ZipEntry(filename);
            zipOutputStream.putNextEntry(zipEntry);
	        int length;
            while ((length = stream.read(buffer)) > 0) {
                zipOutputStream.write(buffer, 0, length);
            }
            stream.close();
	    }
	    HashSet<String> chartFileNames = new HashSet<String>();
	    for(ChartType chartType : ChartType.values()){
	        chartFileNames.add(chartType.getFileName());
	    }
	    FilenameFilter filter = new FilenameFilter() {
            
            @Override
            public boolean accept(File dir, String name) {
                return chartFileNames.contains(name);
            }
        };
	    for(String filename : new File(simulation.getPathToResult()).list(filter)){
	        String absoluteFilePath = simulation.getPathToResult() + File.separator + filename;
            String zippedFilePath = filename;
            //Omit directories
            if(!(new File(absoluteFilePath).isFile()))
                continue;
            ZipEntry zipEntry = new ZipEntry(zippedFilePath);
            zipOutputStream.putNextEntry(zipEntry);
            FileInputStream fileInputStream = new FileInputStream(absoluteFilePath);
            int length;
            while ((length = fileInputStream.read(buffer)) > 0) {
                zipOutputStream.write(buffer, 0, length);
            }
            fileInputStream.close();
	    }
	    zipOutputStream.closeEntry();
	    zipOutputStream.close();
	    return filePath;
	}

    @Override
    public String getChartPath(int id) {
        Chart chart = simMapper.getChart(id);
        Simulation simulation = simMapper.getSimulation(chart.getIdSimulation());
        return simulation.getPathToResult() + File.separatorChar + chart.getChartType().getFileName();
    }
	
}
