package app.simulator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.charts.ChartType;
import app.exceptions.WrongSimulationReportPayloadException;
import app.model.Simulation;
import app.model.SimulationReport;
import app.model.Simulation.SimulationStatus;
import app.spring.database.ISimulationMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Service("simulators")
@Getter
public class SimulatorsHandlerImpl implements ISimulatorsHandler {

    @AllArgsConstructor
    @Getter
    private enum ReportType {
        OUTPUT(false, true, false),
        STARTED(false, false, false),
        DONE(true, false, false),
        ERROR(false, true, false),
        CHART_GENERATED(true, true, true);

        private boolean acceptingReportsAfterAborting;
        private boolean requiringMessage;
        private boolean requiringFileContent;

    }

    private static final Pattern PROGRESS_PATTERN = Pattern.compile("^(.*)progress: (\\d+)%(.*)$",
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);

    private ArrayList<String> simulators;

    @Autowired
    private ISimulationMapper simMapper;

    @Autowired
    private IFileHandler fileHandler;

    @Autowired
    private IMailSender mailSender;
    
    @Autowired
    private IChartHandler chartHandler;
    
    public SimulatorsHandlerImpl() {
        this.simulators = new ArrayList<>();
    }

    @Override
    public void add(String ip) {
        if (!simulators.contains(ip)) {
            simulators.add(ip);
        }
    }

    @Override
    public void handleReport(SimulationReport simulationReport)
            throws WrongSimulationReportPayloadException, IOException {
        if (simulationReport.getSimulatorAddress() == null || "".equals(simulationReport.getSimulatorAddress())
                || simulationReport.getReportType() == null || "".equals(simulationReport.getReportType()))
            throw new WrongSimulationReportPayloadException();
        Simulation simulation = simMapper.getSimulation(simulationReport.getIdSimulation());
        if (simulation == null)
            return;
        ReportType reportType = null;
        try {
            reportType = ReportType.valueOf(simulationReport.getReportType().toUpperCase());
        } catch (Exception e) {
            throw new WrongSimulationReportPayloadException();
        }
        if (simulation.getSimulationStatus() == SimulationStatus.ABORTED
                && !reportType.isAcceptingReportsAfterAborting()) {
            return;
        }
        if(reportType.isRequiringMessage() && simulationReport.getMessage() == null){
            throw new WrongSimulationReportPayloadException();
        }
        if(reportType.isRequiringFileContent() && simulationReport.getFileContent() == null){
            throw new WrongSimulationReportPayloadException();
        }
            
        switch (reportType) {
        case STARTED:
            simMapper.updateSimulationStatus(simulation.getIdSimulation(), SimulationStatus.IN_PROGRESS);
            break;
        case OUTPUT:
            try {
                Matcher matcher = PROGRESS_PATTERN.matcher(simulationReport.getMessage());
                if (matcher.find()) {
                    try {
                        Integer progressPercentage = Integer.parseInt(matcher.group(2));
                        if (progressPercentage < 0 || progressPercentage > 100) {
                            throw new Exception("Progress percentage out of bounds " + progressPercentage);
                        }
                        simMapper.updateSimulationPercentage(simulation.getIdSimulation(), progressPercentage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                fileHandler.appendSimulationOutput(simulation.getIdSimulation(), simulationReport.getMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }
            break;
        case DONE:
            if(simulation.getSimulationStatus().getCanStop())
                simMapper.updateSimulationStatus(simulation.getIdSimulation(), SimulationStatus.DONE);
            mailSender.sendSimulationCharts(simulation.getIdSimulation());
            break;
        case ERROR:
            simMapper.updateSimulationStatus(simulation.getIdSimulation(), SimulationStatus.ERROR);
            fileHandler.appendSimulationOutput(simulation.getIdSimulation(), simulationReport.getMessage());
            break;
        case CHART_GENERATED:
            try {
                ChartType chartType = ChartType.valueOf(simulationReport.getMessage());
                chartHandler.createChart(simulation.getIdSimulation(), chartType, simulationReport.getFileContent());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                throw new WrongSimulationReportPayloadException();
            }
            break;
        }

    }
}
