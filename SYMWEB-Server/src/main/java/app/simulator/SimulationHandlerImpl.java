package app.simulator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import app.exceptions.UnauthorizedException;
import app.model.Config;
import app.model.Simulation;
import app.model.Simulation.SimulationStatus;
import app.model.User;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import app.utils.UserUtils;

@Service("simulationHandler")
public class SimulationHandlerImpl implements ISimulationHandler {

    private final IUserMapper userMapper;
    private final ISimulationMapper simMapper;
    private final IConfigHandler configHandler;
    private final IFileHandler fileHandler;
    private final ITagHandler tagHandler;
    private final RestTemplate sslRestTemplate;

    @Autowired
    public SimulationHandlerImpl(ISimulationMapper simMapper, IUserMapper userMapper, IConfigHandler configHandler,
            IFileHandler fileHandler, ITagHandler tagHandler, RestTemplate sslRestTemplate) {
        this.userMapper = userMapper;
        this.simMapper = simMapper;
        this.configHandler = configHandler;
        this.fileHandler = fileHandler;
        this.tagHandler = tagHandler;
        this.sslRestTemplate = sslRestTemplate;
    }

    @Override
    public int createSimulation(Config cfg, String simulatorAddress, String tagsString) throws UnauthorizedException {
        String username = UserUtils.getLoggedUserName();
        if (username == null)
            throw new UnauthorizedException();
        // Create empty simulation
        Simulation simulation = createEmptySimulation(username, cfg.getIdConfig());
        // create dir for results from simulation
        boolean resultDirCreated = fileHandler.createPathToResultDirectory(username, simulation.getIdSimulation());
        // if everything is correct update simulation in DB
        if (resultDirCreated) {
            simulation.setPathToResult(fileHandler.getSimulationResultDirPath(username, simulation.getIdSimulation()));
            simulation.setRelatedSimulatorAddress(simulatorAddress);
            simMapper.updateSimulation(simulation);
            tagHandler.createSimulationTags(simulation.getIdSimulation(), tagsString);
            return simulation.getIdSimulation();
        }
        simMapper.deleteSimulation(simulation.getIdSimulation());
        return -1;
    }

    @Override
    public ArrayList<Simulation> getSimulations(Integer idUser, Boolean ongoing) throws UnauthorizedException {
        String username = UserUtils.getLoggedUserName();
        if (username == null)
            throw new UnauthorizedException();
        int currentIdUser = userMapper.getIdUser(username);
        if(idUser == null)
            idUser = currentIdUser;
        if(currentIdUser != idUser && !UserUtils.isLoggedUserAdmin()){
            throw new UnauthorizedException();
        }
        SimulationStatus[] statuses = Arrays.stream(SimulationStatus.values()).filter(x -> ongoing == null ? true : (x.getCanStop() == ongoing))
                .toArray(SimulationStatus[]::new);
        assert (statuses != null && statuses.length > 0) : "Wrong statuses filter";
        return simMapper.getUserSimulationsWithStatus(idUser, statuses);
    }

    @Override
    public ArrayList<Simulation> getConfigSimulations(int idConfig) throws UnauthorizedException {
        Config config = configHandler.getConfig(idConfig);
        return simMapper.getConfigSimulations(config.getIdConfig());
    }

    @Override
    public ArrayList<Simulation> getSimulationsInProgress() throws UnauthorizedException {
        if (!UserUtils.isLoggedUserAdmin())
            throw new UnauthorizedException();
        SimulationStatus[] statuses = Arrays.stream(SimulationStatus.values()).filter(x -> x.getCanStop() == true)
                .toArray(SimulationStatus[]::new);
        List<Simulation> list = simMapper.getSimulationsWithStatus(statuses);
        return simMapper.getSimulationsWithStatus(statuses);
    }

    // Create a empty simulation in DB and return simulation object
    private Simulation createEmptySimulation(String username, int idConfig) {
        int idUser = userMapper.getIdUser(username);
        Simulation simulation = new Simulation();
        simulation.setIdUser(idUser);
        simulation.setIdConfig(idConfig);
        simMapper.insertSimulation(simulation);
        return simulation;
    }

    @Override
    public boolean stopSimulation(int idSimulation) throws UnauthorizedException {
        Simulation simulation = validateSimulationOwnership(idSimulation);
        if (!simulation.getSimulationStatus().getCanStop()) {
            return false;
        }
        ResponseEntity<String> loginResponse = sslRestTemplate
                .exchange("https://" + simulation.getRelatedSimulatorAddress() + "/stopSimulation?id="
                        + simulation.getSimulationServerIdSimulation(), HttpMethod.POST, null, String.class);
        if (loginResponse.getStatusCode() != HttpStatus.OK) {
            return false;
        } else {
            simMapper.updateSimulationStatus(simulation.getIdSimulation(), SimulationStatus.ABORTED);
            return true;
        }

    }

    @Override
    public void deleteSimulation(int idSimulation) throws UnauthorizedException, IOException {
        Simulation simulation = validateSimulationOwnership(idSimulation);
        simMapper.deleteSimulationComments(simulation.getIdSimulation());
        simMapper.deleteSimulationTag(simulation.getIdSimulation());
        simMapper.deleteChartsForSimulation(simulation.getIdSimulation());
        User user = userMapper.selectById(simulation.getIdUser());
        fileHandler.deleteSimulationDirectory(user.getUsername(), idSimulation);
        simMapper.deleteSimulation(idSimulation);
    }

    @Override
    public void updateSimulationSimulationServerIdSimulation(int idSimulation, int simulationServerSimulationId)
            throws UnauthorizedException {
        validateSimulationOwnership(idSimulation);
        simMapper.updateSimulationSimulationServerIdSimulation(idSimulation, simulationServerSimulationId);
    }

    @Override
    public Simulation getSimulationWithId(int idSimulation) throws UnauthorizedException {

        return validateSimulationOwnership(idSimulation);
    }

    private Simulation validateSimulationOwnership(int idSimulation) throws UnauthorizedException {
        String username = UserUtils.getLoggedUserName();
        if (username == null)
            throw new UnauthorizedException();
        int idUser = userMapper.getIdUser(username);
        boolean isAdmin = UserUtils.isLoggedUserAdmin();
        Simulation simulation = simMapper.getSimulation(idSimulation);
        if (idUser != simulation.getIdUser() && !isAdmin) {
            throw new UnauthorizedException();
        }
        return simulation;
    }
}
