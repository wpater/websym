package app.simulator;

public interface IMessageHandler {

    public String getMessage(String code);
    public String getMessage(String code, Object[] args);
    
}
