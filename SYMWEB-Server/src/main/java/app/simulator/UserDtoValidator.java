package app.simulator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import app.model.dataTransferObjects.UserDto;
import app.spring.database.IUserMapper;

@Component
public class UserDtoValidator implements Validator {

    @Autowired
    IUserMapper userMapper;

    private Pattern pattern;
    private Matcher matcher;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$";

    @Override
    public boolean supports(Class<?> arg0) {
        return UserDto.class.equals(arg0);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDto userDto = (UserDto) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        if (!errors.hasFieldErrors("username")) {
            if (userDto.getUsername().length() < 6 || userDto.getUsername().length() > 32) {
                errors.rejectValue("username", "registerForm.username.wrongSize");
            }
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (!errors.hasFieldErrors("password")) {
            if (userDto.getPassword().length() < 8 || userDto.getPassword().length() > 32) {
                errors.rejectValue("password", "registerForm.password.wrongSize");
            } else if (!userDto.getPassword().equals(userDto.getMatchingPassword())) {
                errors.rejectValue("matchingPassword", "registerForm.matchingPassword.noMatch");
            }
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (!errors.hasFieldErrors("email")) {
            if (!validateEmail(userDto.getEmail())) {
                errors.rejectValue("email", "registerForm.email.invalid");
            }
        }

    }

    private boolean validateEmail(String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
