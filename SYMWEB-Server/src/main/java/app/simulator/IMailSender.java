package app.simulator;

public interface IMailSender {

    void sendSimulationCharts(int idSimulation);

}
