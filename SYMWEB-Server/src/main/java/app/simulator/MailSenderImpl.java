package app.simulator;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import app.model.Chart;
import app.model.Simulation;
import app.model.User;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import app.spring.mail.MailConfig;

@Service("mailSender")
@PropertySource("classpath:server.properties")
public class MailSenderImpl implements IMailSender {

    private final static String SIMULATION_DONE_SUBJECT = "Simulation results";
    private final static String SIMULATION_DONE_TEXT = "Your simulation has finished. In the attachment you can find the charts showing its progress.";
    private final static String CHART_LINK_FORMAT_TEXT = "Link to %s chart: %s ";
    private final static String CHART_LINK_FORMAT_HTML = "You can view %s chart <a href='%s'>here</a>";
    @Autowired
    IFileHandler fileHandler;

    @Autowired
    ISimulationMapper simMapper;

    @Autowired
    IUserMapper userMapper;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    MailConfig mailConfig;
    
    @Autowired
    IChartHandler chartHandler;
    
    @Value("${application.root.url}")
    private String applicationRootUrl;
    
    @Override
    public void sendSimulationCharts(int idSimulation) {
        Simulation simulation = simMapper.getSimulation(idSimulation);
        if (simulation == null)
            return;
        User user = userMapper.selectById(simulation.getIdUser());
        if (user == null)
            return;
        String zipPath;
        try {
            zipPath = fileHandler.zipChartsForSimulation(idSimulation);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(mailConfig.getUsername());
            helper.setTo(user.getEmail());
            helper.setSubject(SIMULATION_DONE_SUBJECT);
            StringBuilder textBuilder = new StringBuilder(SIMULATION_DONE_TEXT);
            StringBuilder htmlBuilder = new StringBuilder(SIMULATION_DONE_TEXT);
            for(Chart chart : chartHandler.getChartsForSimulation(idSimulation)){
                textBuilder.append('\n');
                htmlBuilder.append("<br>");
                textBuilder.append(String.format(CHART_LINK_FORMAT_TEXT, chart.getChartType().toString(), getChartUrl(chart)));
                htmlBuilder.append(String.format(CHART_LINK_FORMAT_HTML, chart.getChartType().toString(), getChartUrl(chart)));
            }
            helper.setText(textBuilder.toString(), htmlBuilder.toString());

            FileSystemResource file = new FileSystemResource(zipPath);
            helper.addAttachment(file.getFilename(), file);
            javaMailSender.send(message);
        } catch (MailException | MessagingException e) {
            e.printStackTrace();
        }
    }

    private String getChartUrl(Chart chart) {
        return applicationRootUrl+"chart/"+chart.getIdChart();
    }

}
