package app.simulator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.charts.ChartType;
import app.model.Chart;
import app.spring.database.ISimulationMapper;

@Service("chartHandler")
public class ChartHandlerImpl implements IChartHandler {

    @Autowired
    ISimulationMapper simMapper;
    
    @Autowired
    IFileHandler fileHandler;
    
    @Override
    public int createChart(int idSimulation, ChartType chartType, String chartHtmlContent) throws IOException {
        fileHandler.saveChartContent(idSimulation, chartType, chartHtmlContent);
        Chart chart = new Chart();
        chart.setIdSimulation(idSimulation);
        chart.setChartType(chartType);
        chart.setCreationTimestamp(new Date().getTime());
        simMapper.insertChart(chart);
        return chart.getIdChart();   
    }

    @Override
    public Chart getChart(int id) {
        return simMapper.getChart(id);
    }

    @Override
    public ArrayList<Chart> getChartsForSimulation(int idSimulation) {
        return simMapper.getChartsForSimulation(idSimulation);
    }

}
