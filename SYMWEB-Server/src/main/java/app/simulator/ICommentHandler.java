package app.simulator;

import java.util.List;

import app.exceptions.UnauthorizedException;
import app.model.Comment;

public interface ICommentHandler {

    public void addComment(int idSimulation, String content) throws UnauthorizedException;
    public List<Comment> getCommentsForSimulation(int idSimulation) throws UnauthorizedException;
    
}
