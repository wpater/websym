package app.simulator;

import java.io.IOException;
import java.util.ArrayList;

import app.exceptions.WrongSimulationReportPayloadException;
import app.model.SimulationReport;

public interface ISimulatorsHandler {
    void add(String ip);
    ArrayList<String> getSimulators();
    void handleReport(SimulationReport simulationReport) throws WrongSimulationReportPayloadException, IOException;
}
