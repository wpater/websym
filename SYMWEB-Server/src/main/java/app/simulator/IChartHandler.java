package app.simulator;

import java.io.IOException;
import java.util.ArrayList;

import app.charts.ChartType;
import app.model.Chart;

public interface IChartHandler {
    
    public int createChart(int idSimulation, ChartType chartType, String chartHtmlContent) throws IOException;

    public Chart getChart(int id);

    public ArrayList<Chart> getChartsForSimulation(int idSimulation);
    
}
