package app.simulator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;

@Service("messageHandler")
public class MessageHandlerImpl implements IMessageHandler {

    @Autowired
    ResourceBundleMessageSource messageSource;
    
    @Override
    public String getMessage(String code, Object[] args) {
        return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
    }

    @Override
    public String getMessage(String code) {
        return getMessage(code, null);
    }
    
}
