package app.simulator;

import java.io.IOException;
import java.util.ArrayList;

import app.exceptions.UnauthorizedException;
import app.model.Config;

public interface IConfigHandler {
    ArrayList<Config> getConfigsForUser(Integer idUser) throws UnauthorizedException;
    Config deleteConfig(int idConfig) throws UnauthorizedException, IOException;
    Config getConfig(int idConfig) throws UnauthorizedException;
    int addConfigToDB(String simulationConfigText, String environmentConfigText, String foramConfigText, String tagsString) throws UnauthorizedException;
}
