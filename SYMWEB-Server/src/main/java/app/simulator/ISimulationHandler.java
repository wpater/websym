package app.simulator;

import java.io.IOException;
import java.util.ArrayList;

import app.exceptions.UnauthorizedException;
import app.model.Config;
import app.model.Simulation;

public interface ISimulationHandler {
    int createSimulation(Config cfg, String simulatorAddress, String tagsString) throws UnauthorizedException;

    ArrayList<Simulation> getSimulations(Integer idUser, Boolean ongoing) throws UnauthorizedException;

    ArrayList<Simulation> getConfigSimulations(int idConfig) throws UnauthorizedException;
    
    ArrayList<Simulation> getSimulationsInProgress() throws UnauthorizedException;
    
    boolean stopSimulation(int id) throws UnauthorizedException;

    void deleteSimulation(int idSimulation) throws UnauthorizedException, IOException;

    void updateSimulationSimulationServerIdSimulation(int idSimulation, int simulationServerSimulationId)
            throws UnauthorizedException;

    Simulation getSimulationWithId(int idSimulation) throws UnauthorizedException;

}
