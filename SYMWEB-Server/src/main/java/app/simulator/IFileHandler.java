package app.simulator;

import java.io.IOException;

import app.charts.ChartType;
import app.exceptions.UnauthorizedException;

public interface IFileHandler {

	public byte[] readAllBytesFromFile(String path) throws IOException;

	public void deleteConfigDirectory(String username, int idConfig) throws IOException;
	
	public void deleteSimulationDirectory(String username, int idSimulation) throws IOException;

	public boolean saveUserConfigFiles(String username, int idConfig, String foramConfigContent, String simulationConfigContent,
			String environmentConfigContent);

	public String getForamConfigFilePath(String username, int idConfig);

	public String getSimulationConfigFilePath(String username, int idConfig);

	public String getEnvironmentConfigFilePath(String username, int idConfig);

	public boolean createPathToResultDirectory(String username, int idSimulation);
	
	public String getSimulationResultDirPath(String username, int idSimulation);

    public void appendSimulationOutput(int idSimulation, String message) throws IOException;
    
    public String getSimulationOutput(int idSimulation) throws IOException, UnauthorizedException;

	public void saveChartContent(int idSimulation, ChartType chartType, String fileContent) throws IOException;

    public String zipChartsForSimulation(int idSimulation) throws IOException;

    public String getChartPath(int id);

}
