package app.simulator;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.exceptions.UnauthorizedException;
import app.model.Comment;
import app.model.Simulation;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import app.utils.UserUtils;

@Service("commentHandler")
public class CommentHandlerImpl implements ICommentHandler {

    @Autowired
    ISimulationMapper simMapper;
    @Autowired
    IUserMapper userMapper;
    
    @Override
    public void addComment(int idSimulation, String content) throws UnauthorizedException {
        String username = UserUtils.getLoggedUserName();
        if (username == null)
            throw new UnauthorizedException();
        boolean isAdmin = UserUtils.isLoggedUserAdmin();
        int idUser = userMapper.getIdUser(username);
        Simulation simulation = simMapper.getSimulation(idSimulation);
        if(idUser != simulation.getIdUser() && !isAdmin) {
                throw new UnauthorizedException();
        }
        Comment comment = new Comment(idSimulation, username, content, new Date().getTime());
        simMapper.insertComment(comment);
    }

    @Override
    public List<Comment> getCommentsForSimulation(int idSimulation) throws UnauthorizedException {
        String username = UserUtils.getLoggedUserName();
        if (username == null)
            throw new UnauthorizedException();
        boolean isAdmin = UserUtils.isLoggedUserAdmin();
        int idUser = userMapper.getIdUser(username);
        Simulation simulation = simMapper.getSimulation(idSimulation);
        if(idUser != simulation.getIdUser() && !isAdmin) {
                throw new UnauthorizedException();
        }
        return simMapper.getCommentsForSimulation(idSimulation);
        
    }

}
