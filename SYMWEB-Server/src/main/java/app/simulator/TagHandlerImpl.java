package app.simulator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.model.ConfigTag;
import app.model.SimulationTag;
import app.model.Tag;
import app.spring.database.ISimulationMapper;

@Service("tagHandler")
public class TagHandlerImpl implements ITagHandler {

    @Autowired
    ISimulationMapper simMapper;
    
    
    @Override
    public void createConfigTags(int idConfig, String tagsString) {
        String[] fields = tagsString.split(";");
        for (String tagString : fields) {
            if (!"".equals(tagString)) {
                Tag tag = new Tag();
                tag.setName(tagString);
                int idTag = insertTag(tag);
                ConfigTag configTag = new ConfigTag(idTag, idConfig);
                simMapper.insertConfigTag(configTag);
            }
        }
        // add id config as tag
        Tag t = new Tag();
        t.setName("config " + idConfig);
        int idTag = insertTag(t);
        ConfigTag ct = new ConfigTag(idTag, idConfig);
        simMapper.insertConfigTag(ct);
        
    }

    @Override
    public void createSimulationTags(int idSimulation, String tagsString) {
        String[] fields = tagsString.split(";");
        for (String tagString : fields) {
            if (!"".equals(tagString)) {
                Tag tag = new Tag();
                tag.setName(tagString);
                int idTag = insertTag(tag);
                SimulationTag simulationTag = new SimulationTag(idTag, idSimulation);
                simMapper.insertSimulationTag(simulationTag);
            }
        }
        // add id simulation as tag
        Tag t = new Tag();
        t.setName("simulation " + idSimulation);
        int idTag = insertTag(t);
        SimulationTag simulationTag = new SimulationTag(idTag, idSimulation);
        simMapper.insertSimulationTag(simulationTag);
        // add date as tag for simulation
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss");
        Date date = new Date();
        String dateTagString = dateFormat.format(date);
        Tag dateTag = new Tag();
        dateTag.setName(dateTagString);
        idTag = insertTag(dateTag);
        SimulationTag dateSimulationTag = new SimulationTag(idTag, idSimulation);
        simMapper.insertSimulationTag(dateSimulationTag);
    }
    
    private int insertTag(Tag t) {
        try {
            simMapper.insertTag(t);
        } catch (org.springframework.jdbc.UncategorizedSQLException e) {
            return simMapper.getIdTagWithName(t.getName());
        }
        return t.getIdTag();
    }

}
