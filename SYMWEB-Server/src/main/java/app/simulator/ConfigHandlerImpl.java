package app.simulator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.exceptions.UnauthorizedException;
import app.model.Config;
import app.model.Simulation;
import app.spring.database.ISimulationMapper;
import app.spring.database.IUserMapper;
import app.utils.UserUtils;

@Service("configHandler")
public class ConfigHandlerImpl implements IConfigHandler {
    @Autowired
    IUserMapper userMapper;
    @Autowired
    ISimulationMapper simMapper;
    @Autowired
    private IFileHandler fileHandler;
    @Autowired
    private ITagHandler tagHandler;
    @Autowired
    private ISimulationHandler simulationHandler;

    @Override
    public ArrayList<Config> getConfigsForUser(Integer idUser) throws UnauthorizedException {
        String username = UserUtils.getLoggedUserName();
        if (username == null)
            throw new UnauthorizedException();
        int currentIdUser = userMapper.getIdUser(username);
        if(idUser == null)
            idUser = currentIdUser;
        if(currentIdUser != idUser && !UserUtils.isLoggedUserAdmin()){
            throw new UnauthorizedException();
        }
        ArrayList<Config> list = simMapper.getConfigsForUser(idUser);
        Collections.reverse(list);
        return list;
    }

    @Override
    public Config deleteConfig(int idConfig) throws UnauthorizedException, IOException {
        Config config = validateConfigOwnership(idConfig);
        String ownerUsername = userMapper.selectById(config.getIdUser()).getUsername();
        ArrayList<Simulation> list = simMapper.getUserSimulations(config.getIdUser());
        // FIXME get config related simulations, instead of iterating through
        // all of them
        try {
            for (Simulation s : list) {
                if (s.getIdConfig() == idConfig)
                    simulationHandler.deleteSimulation(s.getIdSimulation());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        simMapper.deleteConfig(idConfig);
        simMapper.deleteConfigTag(idConfig);
        fileHandler.deleteConfigDirectory(ownerUsername, idConfig);
        return config;
    }

    @Override
    public Config getConfig(int idConfig) throws UnauthorizedException {
        return validateConfigOwnership(idConfig);
    }

    @Override
    public int addConfigToDB(String simulationConfigText, String environmentConfigText, String foramConfigText,
            String tagsString) throws UnauthorizedException {
        String username = UserUtils.getLoggedUserName();
        if (username == null)
            throw new UnauthorizedException();
        int idUser = userMapper.getIdUser(username);
        // save user files onto file system
        Config config = new Config();
        config.setIdUser(idUser);
        simMapper.insertConfig(config);
        boolean filesSaved = fileHandler.saveUserConfigFiles(username, config.getIdConfig(), foramConfigText,
                simulationConfigText, environmentConfigText);
        if (filesSaved) {
            config.setForam_config(fileHandler.getForamConfigFilePath(username, config.getIdConfig()));
            config.setSimulation_config(fileHandler.getSimulationConfigFilePath(username, config.getIdConfig()));
            config.setEnvironment_config(fileHandler.getEnvironmentConfigFilePath(username, config.getIdConfig()));
            // update config
            simMapper.updateConfig(config);
            // create config tags
            tagHandler.createConfigTags(config.getIdConfig(), tagsString);
            // Required to set up tags properly
            return config.getIdConfig();
        }
        // Cleanup if failed
        simMapper.deleteConfig(config.getIdConfig());
        return -1;
    }

    private Config validateConfigOwnership(int idConfig) throws UnauthorizedException {
        String username = UserUtils.getLoggedUserName();
        if (username == null)
            throw new UnauthorizedException();
        int idUser = userMapper.getIdUser(username);
        boolean isAdmin = UserUtils.isLoggedUserAdmin();
        Config config = simMapper.getConfig(idConfig);
        if (idUser != config.getIdUser() && !isAdmin) {
            throw new UnauthorizedException();
        }
        return config;
    }
}
