DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  idUser INTEGER PRIMARY KEY AUTOINCREMENT ,
  username varchar(255) UNIQUE NOT NULL,
  password varchar(255) DEFAULT NULL,
  enabled TINYINT  DEFAULT 1,
  email varchar(255) DEFAULT NULL
);

DROP TABLE IF EXISTS `role`;
CREATE TABLE role (
  rolename varchar(45) PRIMARY KEY
);

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE user_roles (
  idUser INTEGER NOT NULL,
  rolename varchar(45) NOT NULL,
  CONSTRAINT fk_idUser FOREIGN KEY (idUser) REFERENCES user (idUser),
  CONSTRAINT fk_rolename FOREIGN KEY (rolename) REFERENCES role (rolename)
);

DROP TABLE IF EXISTS `config`;
CREATE TABLE config (
  idConfig INTEGER PRIMARY KEY AUTOINCREMENT ,
  idUser INTEGER NOT NULL,
  simulation_config varchar(255) DEFAULT NULL,
  foram_config varchar(255) DEFAULT NULL,
  environment_config varchar(255) DEFAULT NULL,
  CONSTRAINT fk_idUser FOREIGN KEY (idUser) REFERENCES user (idUser)
);

DROP TABLE IF EXISTS `tag`;
CREATE TABLE tag (
  idTag INTEGER PRIMARY KEY AUTOINCREMENT ,
  name varchar(255) UNIQUE NOT NULL
);

DROP TABLE IF EXISTS `config_tag`;
CREATE TABLE config_tag (
  idTag INTEGER NOT NULL,
  idConfig INTEGER NOT NULL,
  CONSTRAINT fk_idTag FOREIGN KEY (idTag) REFERENCES tag (idTag),
  CONSTRAINT fk_idConfig FOREIGN KEY (idConfig) REFERENCES config (idConfig)
);

DROP TABLE IF EXISTS `simulation_tag`;
CREATE TABLE simulation_tag (
  idTag INTEGER NOT NULL,
  idSimulation INTEGER NOT NULL,
  CONSTRAINT fk_idTag FOREIGN KEY (idTag) REFERENCES tag (idTag),
  CONSTRAINT fk_idSimulation FOREIGN KEY (idSimulation) REFERENCES simulation (idSimulation)
);

DROP TABLE IF EXISTS `simulation`;
CREATE TABLE simulation (
  idSimulation INTEGER PRIMARY KEY AUTOINCREMENT,
  idConfig INTEGER NOT NULL,
  idUser INTEGER NOT NULL,
  path_to_result varchar(255) DEFAULT NULL,
  relatedSimulatorAddress varchar(255) DEFAULT NULL,
  simulationServerIdSimulation INTEGER DEFAULT NULL,
  progressPercentage INTEGER NOT NULL,
  simulationStatus varchar(20) NOT NULL,
  CONSTRAINT fk_idUser FOREIGN KEY (idUser) REFERENCES user (idUser),
  CONSTRAINT fk_idConfig FOREIGN KEY (idConfig) REFERENCES config (idConfig)
);

DROP TABLE IF EXISTS `comment`;
CREATE TABLE comment (
  idComment INTEGER PRIMARY KEY AUTOINCREMENT,
  idSimulation INTEGER NOT NULL,
  username varchar(255) NOT NULL,
  content varchar(255) NOT NULL,
  addDateTimestamp INTEGER NOT NULL,
  CONSTRAINT fk_idSimulation FOREIGN KEY (idSimulation) REFERENCES simulation (idSimulation)
);

DROP TABLE IF EXISTS `chart`;
CREATE TABLE chart (
  idChart INTEGER PRIMARY KEY AUTOINCREMENT,
  idSimulation INTEGER NOT NULL,
  chartType varchar(20) NOT NULL,
  creationTimestamp INTEGER NOT NULL,
  CONSTRAINT fk_idSimulation FOREIGN KEY (idSimulation) REFERENCES simulation (idSimulation)
);