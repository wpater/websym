<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SYMWEB</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="<c:url value='/static/css/animate.css' />" rel="stylesheet" />
<script src="<c:url value='/static/js/bootstrap-notify.min.js' />"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cosmo/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-h21C2fcDk/eFsW9sC9h0dhokq5pDinLNklTKoxIZRUn3+hvmgQSffLLQ4G4l2eEr" crossorigin="anonymous">
<link href="<spring:url value='/static/css/app.css' />" rel="stylesheet" />
<script type="text/javascript">
    $(document).ready(function() {
    	$.notifyDefaults({
    		allow_dismiss : true,
    		delay : 8000,
    		offset : {
    			x : 20,
    			y : 20
    		},
    		placement : {
    			from : 'bottom',
    			align : 'right'
    		},
    		z_index : 1051
    	});
    	<c:if test="${not empty success}">
    		$.notify({
    			message : "${success}"
    		}, {
    			type : 'success'
    		});
    	</c:if>
        <c:if test="${not empty error}">
            $.notify({
        		message : "${error}"
        	}, {
        		type : 'danger'
        	});
    	</c:if>
    	<c:if test="${(not empty registerSuccessful) && !registerSuccessful}">
			$("#registerModal").modal("show");
    	</c:if>
    	<c:if test="${((not empty loginRequest) && loginRequest) || (param.error != null)}">
    		$("#loginModal").modal("show");
    	</c:if>
    });
</script>

</head>

<body>
    <sec:authorize access="hasRole('ROLE_ANONYMOUS')">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#annonymousNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/home">
                        <spring:message code='app.name' />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="annonymousNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="/home">
                                <spring:message code='home' />
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#registerModal" data-toggle="modal" data-target="#registerModal">
                                <span class="glyphicon glyphicon-user"></span>
                                <spring:message code='sign.up' />
                            </a>
                        </li>
                        <li>
                            <a href="#loginModal" data-toggle="modal" data-target="#loginModal">
                                <span class="glyphicon glyphicon-log-in"></span>
                                <spring:message code='sign.in' />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_USER')">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/home">
                        <spring:message code='app.name' />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="/home">
                                <spring:message code='home' />
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <spring:message code='simulations' />
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/simulations?ongoing=true">
                                        <spring:message code='ongoing' />
                                    </a>
                                </li>
                                <li>
                                    <a href="/simulations?ongoing=false">
                                        <spring:message code='results' />
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="/configs">
                                <spring:message code='configs' />
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li>
                                <a href="/admin">
                                    <span class="glyphicon glyphicon-wrench"></span>
                                    <spring:message code="admin" />
                                </a>
                            </li>
                        </sec:authorize>
                        <span class="navbar-text">
                                <span class="glyphicon glyphicon-user"></span>
                                <sec:authentication property="principal.username" />
                        </span>
                        <li>
                            <a href="/logout">
                                <span class="glyphicon glyphicon-log-out"></span>
                                <spring:message code='log.out' />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </sec:authorize>
    <div class="thinPagePanel panel panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title">
                <spring:message code='index.panel.title' />
            </h2>
        </div>
        <div class="pagePanelContent">
            <h5 class="input-group-element text-center">
                <spring:message code='index.description' />
            </h5>
            <sec:authorize access="hasRole('ROLE_ANONYMOUS')">
                <div class="input-group-element">
                    <button type="button" data-toggle="modal" data-target="#loginModal"
                        class="btn btn-primary btn-block input-group-element overflowableContent">
                        <spring:message code='index.sign.in.button.text' />
                    </button>
                </div>
                <div class="input-group-element">
                    <button type="button" data-toggle="modal" data-target="#registerModal"
                        class="btn btn-primary btn-block input-group-element overflowableContent">
                        <spring:message code='index.sign.up.button.text' />
                    </button>
                </div>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_USER')">
                <div class="input-group-element">
                    <div class="dropdown">
                        <button type="button"
                            class="dropdown btn btn-primary btn-block dropdown-toggle overflowableContent"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <spring:message code='index.simulations.button.text' />
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/simulations?ongoing=true">
                                    <spring:message code='ongoing' />
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="/simulations?ongoing=false">
                                    <spring:message code='results' />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="input-group-element">
                    <a class="btn btn-primary btn-block overflowableContent"
                                href="/configs">
                        <spring:message code='index.configs.button.text' />
                    </a>
                </div>
            </sec:authorize>
        </div>
    </div>
    <div class="modal fade" style="border-radius: 0px" id="loginModal" tabindex="-1" role="dialog"
        aria-labelledby="loginModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content panel panel-primary" style="margin-bottom: 0px">
                <div class="panel-heading">
                    <h4 id="loginModalLabel">
                        <spring:message code='index.sign.in.modal.title' />
                    </h4>
                </div>
                <div class="pagePanelContent">
                    <c:url var="loginUrl" value="/login" />
                    <form action="${loginUrl}" method="post">
                        <div class="input-group input-sm">
                            <label class="input-group-addon" for="username">
                                <i class="fa fa-user"></i>
                            </label>
                            <input type="text" class="form-control" id="username" name="ssoId"
                                placeholder="<spring:message code='username'/>" required>
                        </div>
                        <div class="input-group input-sm">
                            <label class="input-group-addon" for="password">
                                <i class="fa fa-lock"></i>
                            </label>
                            <input type="password" class="form-control" id="password" name="password"
                                placeholder="<spring:message code='password'/>" required>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        <div class="input-group-element">
                            <button type="submit" class="btn btn-primary btn-block">
                                <spring:message code='sign.in' />
                            </button>
                        </div>
                        <div class="input-group input-sm">
                            <a href="#registerModal" data-toggle="modal" data-target="#registerModal"
                                data-dismiss="modal" role="button">
                                <spring:message code='index.sign.in.modal.sign.up.link' />
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content panel panel-primary" style="margin-bottom: 0px">
                <div class="panel-heading">
                    <h4 id="registerModalLabel">
                        <spring:message code='index.sign.up.modal.title' />
                    </h4>
                </div>
                <div class="pagePanelContent">
                    <c:url var="registryUrl" value="/register" />
                    <spring:message code="username" var="usernamePlaceholder" />
                    <spring:message code="password" var="passwordPlaceholder" />
                    <spring:message code="password.repeat" var="matchingPasswordPlaceholder" />
                    <spring:message code="email" var="emailPlaceholder" />
                    <form:form action="${registryUrl}" method="POST" modelAttribute="registerUser">
                        <spring:bind path="username">
                            <div class="input-group input-sm">
                                <label class="input-group-addon" for="username">
                                    <i class="fa fa-user"></i>
                                </label>
                                <form:input type="text" path="username" class="form-control" id="username"
                                    name="username" placeholder="${usernamePlaceholder}" required="required"></form:input>
                            </div>
                            <form:errors path="username" element="div" class="alert alert-danger input-group-alert" />
                        </spring:bind>
                        <spring:bind path="password">
                            <div class="input-group input-sm">
                                <label class="input-group-addon" for="password">
                                    <i class="fa fa-lock"></i>
                                </label>
                                <form:input type="password" path="password" class="form-control" id="password"
                                    name="password" placeholder="${passwordPlaceholder}" required="required"></form:input>
                            </div>
                            <form:errors path="password" element="div" class="alert alert-danger input-group-alert" />
                        </spring:bind>
                        <spring:bind path="matchingPassword">
                            <div class="input-group input-sm">
                                <label class="input-group-addon" for="matchingPassword">
                                    <i class="fa fa-lock"></i>
                                </label>
                                <form:input type="password" path="matchingPassword" class="form-control"
                                    id="matchingPassword" name="matchingPassword"
                                    placeholder="${matchingPasswordPlaceholder}" required="required"></form:input>

                            </div>
                            <form:errors path="matchingPassword" element="div"
                                class="alert alert-danger input-group-alert" />
                        </spring:bind>
                        <spring:bind path="email">
                            <div class="input-group input-sm">
                                <label class="input-group-addon" for="email">
                                    <i class="fa fa-at"></i>
                                </label>
                                <form:input type="text" path="email" class="form-control" id="email" name="email"
                                    placeholder="${emailPlaceholder}" required="required"></form:input>
                            </div>
                            <form:errors path="email" element="div" class="alert alert-danger input-group-alert" />
                        </spring:bind>
                        <div class="input-group-element">
                            <button type="submit" class="btn btn-primary btn-block">
                                <spring:message code='sign.up' />
                            </button>
                        </div>
                        <div class="input-group input-sm">
                            <a href="#loginModal" data-toggle="modal" data-target="#loginModal" data-dismiss="modal"
                                role="button">
                                <spring:message code='index.sign.up.modal.sign.in.link' />
                            </a>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
