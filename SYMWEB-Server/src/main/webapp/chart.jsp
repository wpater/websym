<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SYMWEB</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cosmo/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-h21C2fcDk/eFsW9sC9h0dhokq5pDinLNklTKoxIZRUn3+hvmgQSffLLQ4G4l2eEr" crossorigin="anonymous">
    <script src="<c:url value='/static/js/jquery-1.11.2.min.js' />"></script>
    <script src="<c:url value='/static/js/jsapi-visualization-1.1.min.js' />"></script>
</head>

<body>
    <sec:authorize access="hasRole('ROLE_ANONYMOUS')">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#annonymousNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/home">
                        <spring:message code='app.name' />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="annonymousNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="/home">
                                <spring:message code='home' />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_USER')">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/home">
                        <spring:message code='app.name' />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="/home">
                                <spring:message code='home' />
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <spring:message code='simulations' />
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/simulations?ongoing=true">
                                        <spring:message code='ongoing' />
                                    </a>
                                </li>
                                <li>
                                    <a href="/simulations?ongoing=false">
                                        <spring:message code='results' />
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="/configs">
                                <spring:message code='configs' />
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li><a href="/admin"> <span class="glyphicon glyphicon-wrench"></span> <spring:message
                                        code="admin" />
                            </a></li>
                        </sec:authorize>
                        <span class="navbar-text">
                                <span class="glyphicon glyphicon-user"></span>
                                <sec:authentication property="principal.username" />
                        </span>
                        <li>
                            <a href="/logout">
                                <span class="glyphicon glyphicon-log-out"></span>
                                <spring:message code='log.out' />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </sec:authorize>
    <div class="pagePanel panel panel-primary">
        <div class="panel-heading">
            <h5 class="panel-title">
                <spring:message code='chart.panel.title' />
            </h5>
        </div>
        <div class="pagePanelContent">
            <h5>
                <spring:message code='chart.description' />
            </h5>
            <hr>
            <c:choose>
                <c:when test="${not empty chartContent}">
                    <div>
                        ${chartContent}
                    </div>
                </c:when>
                <c:otherwise>
                <h5>
                    <spring:message code='chart.not.available' />
                </h5>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

</body>
</html>
