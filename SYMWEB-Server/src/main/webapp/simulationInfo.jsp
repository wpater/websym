<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="app.svc.web.simulator.OutputController"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SYMWEB</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cosmo/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-h21C2fcDk/eFsW9sC9h0dhokq5pDinLNklTKoxIZRUn3+hvmgQSffLLQ4G4l2eEr" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
<script src="https://rawgit.com/makeusabrew/bootbox/f3a04a57877cab071738de558581fbc91812dce9/bootbox.js"></script>
<link href="<c:url value='/static/css/animate.css' />" rel="stylesheet" />
<script src="<c:url value='/static/js/bootstrap-notify.min.js' />"></script>
<script src="<c:url value='/static/js/jsapi-visualization-1.1.min.js' />"></script>
<c:set var="STATUS_PENDING_CLASSES" value="label label-info" />
<c:set var="STATUS_IN_PROGRESS_CLASSES" value="label label-primary" />
<c:set var="STATUS_DONE_CLASSES" value="label label-success" />
<c:set var="STATUS_ABORTED_CLASSES" value="label label-warning" />
<c:set var="STATUS_ERROR_CLASSES" value="label label-danger" />

<c:if test='${simulation.getSimulationStatus().getCanStop()}'>
    <!-- Output gatherer -->
    <script>
	(function worker() {
		var refresh = true;
		var noChartsElementHtml = "<h5>" + "<spring:message code='simulation.details.charts.not.available' />" +  "</h5>";
		$.ajax({
			url : '/outputJSON',
			type : 'GET',
			data : {
				id : <c:out value="${simulation.getIdSimulation()}"/>
			},
			dataType : "json",
			success : function(data) {
				$("#outputContainer").html(data.output);
				$("#simulationProgressBar")
						.attr('aria-valuenow', data.progress).css('width',
								data.progress + "%");
				$("#simulationProgressLabel").html(data.progress + "%");
				switch(data.status){
				case "PENDING":
					$("#statusLabel").removeClass().addClass("<c:out value="${STATUS_PENDING_CLASSES}"/>");
					$("#statusLabel").text("<spring:message code='pending' />");
					break;
				case "IN_PROGRESS":
					$("#statusLabel").removeClass().addClass("<c:out value="${STATUS_IN_PROGRESS_CLASSES}"/>");
					$("#statusLabel").text("<spring:message code='in.progress' />");
					break;
				case "DONE":
					$("#statusLabel").removeClass().addClass("<c:out value="${STATUS_DONE_CLASSES}"/>");
					$("#statusLabel").text("<spring:message code='done' />");
					break;
				case "ABORTED":
					$("#statusLabel").removeClass().addClass("<c:out value="${STATUS_ABORTED_CLASSES}"/>");
					$("#statusLabel").text("<spring:message code='aborted' />");
					break;
				case "ERROR":
					$("#statusLabel").removeClass().addClass("<c:out value="${STATUS_ERROR_CLASSES}"/>");
					$("#statusLabel").text("<spring:message code='error' />");
					break;
				}
				refresh = data.canStop;
				if(!refresh) {
					$("#stopSimulationForm").remove();
				}
				$( "#chartLinksContainer" ).empty();
				if(Object.keys(data.charts).length > 0){
					for (var key in data.charts) {
					  if (data.charts.hasOwnProperty(key)) {
						  var chartLinkElementHtml = "<a href='/chart/" + key + "'class='btn btn-primary' style='display: table-cell;'>" + data.charts[key] + "</a>";
						  $("#chartLinksContainer").append(chartLinkElementHtml);
					  }
					}	
				} else {
					$("#chartLinksContainer").append(noChartsElementHtml);
				}
			},
			complete : function() {
				if(refresh){
					setTimeout(worker, 5000);	
				}
			}
		});
	})();
</script>
    <!-- Simulation stop confirmation dialog -->
    <script type="text/javascript">
    	$(document).ready(function() {
    		$('#stopSimulationForm').submit(function(e) {
    			var currentForm = this;
    			e.preventDefault();
    			bootbox.confirm({
    				title: "<spring:message code='simulation.details.stop.confirm.title' />",
    			    message: "<spring:message code='simulation.details.stop.confirm.message' />",
    			    buttons: {
    			        cancel: {
    			            label: '<i class="fa fa-times"></i> <spring:message code="cancel"/>',
    			            className: 'btn-primary'
    			        },
    			        confirm: {
    			            label: '<i class="fa fa-check"></i> <spring:message code="confirm"/>',
    			            className: 'btn-danger'
    			        }
    			    },
    			    callback: function (result) {
    			        if ( result) {
    			        	currentForm.submit();
    			        }
    			    }
    			});
    		});
    	});
    </script>
</c:if>
<!-- Notifications -->
<script>
	$(document).ready(function() {
		$.notifyDefaults({
			allow_dismiss : true,
			delay : 8000,
			offset : {
				x : 20,
				y : 20
			},
			placement : {
				from : 'bottom',
				align : 'right'
			}
		});
		<c:if test="${not empty success}">
		$.notify({
			message : "${success}"
		}, {
			type : 'success'
		});
		</c:if>
		<c:if test="${not empty error}">
		$.notify({
			message : "${error}"
		}, {
			type : 'danger'
		});
		</c:if>
	});
</script>
</head>

<body>
    <sec:authorize access="hasRole('ROLE_USER')">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/home"> <spring:message code='app.name' />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="/home"> <spring:message code='home' />
                        </a></li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> <spring:message
                                    code='simulations' /> <span class="caret"></span>
                        </a>
                            <ul class="dropdown-menu">
                                <li><a href="/simulations?ongoing=true"> <spring:message code='ongoing' />
                                </a></li>
                                <li><a href="/simulations?ongoing=false"> <spring:message code='results' />
                                </a></li>
                            </ul></li>
                        <li><a href="/configs"> <spring:message code='configs' />
                        </a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li><a href="/admin"> <span class="glyphicon glyphicon-wrench"></span> <spring:message
                                        code="admin" />
                            </a></li>
                        </sec:authorize>
                        <span class="navbar-text"> <span class="glyphicon glyphicon-user"></span> <sec:authentication
                                property="principal.username" />
                        </span>
                        <li><a href="/logout"> <span class="glyphicon glyphicon-log-out"></span> <spring:message
                                    code='log.out' />
                        </a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_USER')">
        <div class="pagePanel panel panel-primary">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <spring:message code='simulation.details.panel.title' />
                </h5>
            </div>
            <div class="pagePanelContent">
                <h3 class="text-center">
                    <span class="overflowableContent" style="white-space: nowrap"> <spring:message
                            code='simulation.details.status.label' />
                    </span>
                    <c:choose>
                        <c:when test='${simulation.getSimulationStatus().toString().equals("PENDING")}'>
                            <span id="statusLabel" class="<c:out value="${STATUS_PENDING_CLASSES}"/>"> <spring:message
                                    code='pending' />
                            </span>
                        </c:when>
                        <c:when test='${simulation.getSimulationStatus().toString().equals("IN_PROGRESS")}'>
                            <span id="statusLabel" class="<c:out value="${STATUS_IN_PROGRESS_CLASSES}"/>"> <spring:message
                                    code='in.progress' />
                            </span>
                        </c:when>
                        <c:when test='${simulation.getSimulationStatus().toString().equals("DONE")}'>
                            <span id="statusLabel" class="<c:out value="${STATUS_DONE_CLASSES}"/>"> <spring:message
                                    code='done' />
                            </span>
                        </c:when>
                        <c:when test='${simulation.getSimulationStatus().toString().equals("ABORTED")}'>
                            <span id="statusLabel" class="<c:out value="${STATUS_ABORTED_CLASSES}"/>"> <spring:message
                                    code='aborted' />
                            </span>
                        </c:when>
                        <c:when test='${simulation.getSimulationStatus().toString().equals("ERROR")}'>
                            <span id="statusLabel" class="<c:out value="${STATUS_ERROR_CLASSES}"/>"> <spring:message
                                    code='error' />
                            </span>
                        </c:when>
                        <c:otherwise>
                            <span id="statusLabel" class="label label-default"> <spring:message code='unknown' />
                            </span>
                        </c:otherwise>
                    </c:choose>
                </h3>
                <div class="progress" style="height: 40px;">
                    <div id="simulationProgressBar"
                        class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
                        aria-valuenow="${simulation.getProgressPercentage()}" aria-valuemin="0" aria-valuemax="100"
                        style="width: ${simulation.getProgressPercentage()}%; display: inline-table;">
                        <div id="simulationProgressLabel"
                            style="font-size: larger; vertical-align: middle; display: table-cell; height: 100%; font-weight: bold;">${simulation.getProgressPercentage()}%</div>
                    </div>
                </div>
                <hr>
                <h4>
                    <spring:message code='simulation.details.charts' />
                </h4>
                <div style="margin-left: -10px; margin-right: -10px; display: table">
                    <div id="chartLinksContainer" style="display: table; width: 100%; table-layout:fixed; border-spacing: 10px;">
                        <c:choose>
                        <c:when test="${not empty charts}">
                            <c:forEach begin="1" end="${fn:length(charts)}" var="index">
                                <a href="/chart/${charts[index-1].getIdChart()}" class="btn btn-primary" style="display: table-cell;"><spring:message code='simulation.details.chart.${charts[index-1].getChartType().toString()}' /></a>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                                <h5>
                                    <spring:message code='simulation.details.charts.not.available' />
                                </h5>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <hr>
                <div style="text-align: left;">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOutput">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                    href="#collapseOutput" aria-expanded="false" aria-controls="collapseOutput">
                                    <div style="display: table; width: 100%">
                                        <div style="display: table-cell; width: 100%;"><spring:message code='simulation.details.output' /></div>
                                        <div>
                                            <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOutput" class="panel-collapse collapse" role="tabpanel"
                            aria-labelledby="headingOutput">
                            <div id="outputContainer" class="outputPanel contentCommentPanel"
                                style="width: 100%; max-height: 300px; overflow: auto;"><c:out value="${output}" /></div>
                        </div>
                    </div>
                </div>
                <c:url var="stopSimulationUrl" value="/stopSimulation/${simulation.getIdSimulation()}" />
                <c:if test='${simulation.getSimulationStatus().getCanStop()}'>
                    <form action="${stopSimulationUrl}" method="post" id="stopSimulationForm">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        <button type="submit" class="btn btn-warning btn-block" style="margin-bottom: 20px;">
                            <spring:message code='simulation.details.stop.simulation.button.text' />
                        </button>
                    </form>
                </c:if>
                <!-- Modal -->
                <div class="modal fade" id="commentModal" tabindex="-1" role="dialog"
                    aria-labelledby="commentModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class=" panel panel-primary">
                            <div class="panel-heading">
                                <h4 id="commentModalLabel">
                                    <spring:message code='simulation.details.comment.modal.title' />
                                </h4>
                            </div>
                            <c:url var="addCommentUrl" value="/addComment" />
                            <form:form action="${addCommentUrl}" method="POST" class="form-horizontal"
                                commandName="addComment">
                                <div class="modal-body">
                                    <textarea name="content" class="form-control noResize"
                                        style="padding: 10px; width: 100%; height: 200px" required></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-inverse" data-dismiss="modal">
                                        <spring:message code='cancel' />
                                    </button>
                                    <button id="submitCommentButton" type="submit" class="btn btn-success" name="id"
                                        value="${simulation.getIdSimulation()}">
                                        <spring:message code='save' />
                                    </button>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
                <div style="text-align: left;">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingComment">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                    href="#collapseComments" aria-expanded="false" aria-controls="collapseComments">
                                    <div style="display: table; width: 100%">
                                        <div style="display: table-cell; width: 100%;">
                                            <spring:message code='simulation.details.comments' />
                                        </div>
                                        <div>
                                            <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseComments" class="panel-collapse collapse" role="tabpanel"
                            aria-labelledby="headingComment">
                            <c:if test="${fn:length(comments) == 0}">
                                <div class="commentFillerPanel">
                                    <i> <spring:message code='simulation.details.no.comments' />
                                    </i>
                                </div>
                            </c:if>
                            <c:forEach begin="1" end="${fn:length(comments)}" var="index">
                                <div class="panel panel-body commentPanel" style="margin: 10px;">
                                    <div class="usernameCommentPanel">${comments[index-1].username}</div>
                                    <div class="contentCommentPanel">${comments[index-1].content}</div>
                                    <div class="contentDataPanel">
                                        <i> <spring:message code='simulation.details.comment.added.date.label' /> <fmt:formatDate
                                                type="both" dateStyle="short" timeStyle="short"
                                                value="${comments[index-1].getDate()}" />
                                        </i>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#commentModal">
                    <spring:message code='simulation.details.add.comment.button.text' />
                </button>

            </div>
        </div>
    </sec:authorize>
</body>
</html>
