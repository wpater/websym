<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SYMWEB</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cosmo/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-h21C2fcDk/eFsW9sC9h0dhokq5pDinLNklTKoxIZRUn3+hvmgQSffLLQ4G4l2eEr" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
<link href="<c:url value='/static/css/animate.css' />" rel="stylesheet" />
<script src="<c:url value='/static/js/bootstrap-notify.min.js' />"></script>
<c:set var="STATUS_PENDING_CLASSES" value="label label-info" />
<c:set var="STATUS_IN_PROGRESS_CLASSES" value="label label-primary" />
<c:set var="STATUS_DONE_CLASSES" value="label label-success" />
<c:set var="STATUS_ABORTED_CLASSES" value="label label-warning" />
<c:set var="STATUS_ERROR_CLASSES" value="label label-danger" />
<script type="text/javascript">
	$(document).ready(function() {
		$.notifyDefaults({
			allow_dismiss : true,
			delay : 8000,
			offset : {
				x : 20,
				y : 20
			},
			placement : {
				from : 'bottom',
				align : 'right'
			},
			z_index : 1051
		});
		<c:if test="${not empty success}">
		$.notify({
			message : "${success}"
		}, {
			type : 'success'
		});
		</c:if>
		<c:if test="${not empty error}">
		$.notify({
			message : "${error}"
		}, {
			type : 'danger'
		});
		</c:if>
	});
</script>
</head>

<body>
    <sec:authorize access="hasRole('ROLE_USER')">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/home">
                        <spring:message code='app.name' />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="/home">
                                <spring:message code='home' />
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <spring:message code='simulations' />
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <c:choose>
                                    <c:when test="${param.ongoing}">
                                        <li class="active">
                                            <a href="/simulations?ongoing=true">
                                                <spring:message code='ongoing' />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/simulations?ongoing=false">
                                                <spring:message code='results' />
                                            </a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li>
                                            <a href="/simulations?ongoing=true">
                                                <spring:message code='ongoing' />
                                            </a>
                                        </li>
                                        <li class="active">
                                            <a href="/simulations?ongoing=false">
                                                <spring:message code='results' />
                                            </a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </ul>
                        </li>
                        <li>
                            <a href="/configs">
                                <spring:message code='configs' />
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li>
                                <a href="/admin">
                                    <span class="glyphicon glyphicon-wrench"></span>
                                    <spring:message code="admin" />
                                </a>
                            </li>
                        </sec:authorize>
                        <span class="navbar-text">
                                <span class="glyphicon glyphicon-user"></span>
                                <sec:authentication property="principal.username" />
                        </span>
                        <li>
                            <a href="/logout">
                                <span class="glyphicon glyphicon-log-out"></span>
                                <spring:message code='log.out' />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_USER')">
        <div class="pagePanel panel panel-primary">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <spring:message code='simulations.panel.title' />
                </h5>
            </div>
            <div class="pagePanelContent">
                <h5>
                    <spring:message code='simulations.description' />
                </h5>

                <table style="table-layout: fixed;"
                    class="table table-fixed table-bordered table-hover table-condensed panel-primary">
                    <thead class="panel-heading">
                        <tr>
                            <th class="col-xs-1 table-separator overflowableContent">
                                <spring:message code='id' />
                            </th>
                            <th class="col-xs-5 table-separator overflowableContent">
                                <spring:message code='tags' />
                            </th>
                            <th class="col-xs-3 table-separator overflowableContent">
                                <spring:message code='status' />
                            </th>
                            <th class="col-xs-3 overflowableContent">
                                <spring:message code='actions' />
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach begin="1" end="${fn:length(table)}" var="index">
                            <tr>
                                <td class="table-separator overflowableContent text-center">${table[index-1].idSimulation}</td>
                                <td class="table-separator overflowableContent">${table[index-1].getTagsString()}</td>
                                <td class="table-separator overflowableContent">
                                    <c:choose>
                                        <c:when
                                            test='${table[index-1].getSimulationStatus().toString().equals("PENDING")}'>
                                            <div class="label-xs col-xs-12 <c:out value="${STATUS_PENDING_CLASSES}"/>">
                                                <spring:message code='pending' />
                                            </div>
                                        </c:when>
                                        <c:when
                                            test='${table[index-1].getSimulationStatus().toString().equals("IN_PROGRESS")}'>
                                            <div
                                                class="label-xs col-xs-12 <c:out value="${STATUS_IN_PROGRESS_CLASSES}"/>">
                                                <spring:message code='in.progress' />
                                            </div>
                                        </c:when>
                                        <c:when test='${table[index-1].getSimulationStatus().toString().equals("DONE")}'>
                                            <div class="label-xs col-xs-12 <c:out value="${STATUS_DONE_CLASSES}"/>">
                                                <spring:message code='done' />
                                            </div>
                                        </c:when>
                                        <c:when
                                            test='${table[index-1].getSimulationStatus().toString().equals("ABORTED")}'>
                                            <span class="label-xs col-xs-12 <c:out value="${STATUS_ABORTED_CLASSES}"/>">
                                                <spring:message code='aborted' />
                                            </span>
                                        </c:when>
                                        <c:when
                                            test='${table[index-1].getSimulationStatus().toString().equals("ERROR")}'>
                                            <span class="label-xs col-xs-12 <c:out value="${STATUS_ERROR_CLASSES}"/>">
                                                <spring:message code='error' />
                                            </span>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="label-xs col-xs-12 label label-default">
                                                <spring:message code='unknown' />
                                            </span>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="overflowableContent">
                                    <div class="form-actions">
                                        <span class="input-group-btn">
                                            <a class="btn btn-primary btn-xs col-xs-12 overflowableContent"
                                                href="/simulations/${table[index-1].idSimulation}">
                                                <spring:message code='details' />
                                            </a>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <a href="/create" class="btn btn-success btn-block" role="button">
                    <spring:message code='simulations.create.simulation.button.text' />
                </a>
            </div>
        </div>
    </sec:authorize>
</body>
</html>
