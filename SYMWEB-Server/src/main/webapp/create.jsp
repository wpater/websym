<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SYMWEB</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cosmo/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-h21C2fcDk/eFsW9sC9h0dhokq5pDinLNklTKoxIZRUn3+hvmgQSffLLQ4G4l2eEr" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="<c:url value='static/css/app.css' />" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
<link href="<c:url value='/static/css/animate.css' />" rel="stylesheet" />
<script src="<c:url value='/static/js/bootstrap-notify.min.js' />"></script>
<script>
	$(document)
			.ready(
					function() {
						$('.inputfile')
								.each(
										function() {
											var $input = $(this), $label = $input
													.next('label'), labelVal = $label
													.html();
											$input
													.on(
															'change',
															function(e) {
																var fileName = '';

																if (this.files
																		&& this.files.length > 1)
																	fileName = (this
																			.getAttribute('data-multiple-caption') || '')
																			.replace(
																					'{count}',
																					this.files.length);
																else if (e.target.value)
																	fileName = e.target.value
																			.split(
																					'\\')
																			.pop();

																if (fileName)
																	$label
																			.find(
																					'span')
																			.html(
																					fileName);
																else
																	$label
																			.html(labelVal);
															});
											$input
													.on(
															'focus',
															function() {
																$input
																		.addClass('has-focus');
															})
													.on(
															'blur',
															function() {
																$input
																		.removeClass('has-focus');
															});
										});
					});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$.notifyDefaults({
			allow_dismiss : true,
			delay : 8000,
			offset : {
				x : 20,
				y : 20
			},
			placement : {
				from : 'bottom',
				align : 'right'
			},
			z_index : 1051
		});
		<c:if test="${not empty success}">
		$.notify({
			message : "${success}"
		}, {
			type : 'success'
		});
		</c:if>
		<c:if test="${not empty error}">
		$.notify({
			message : "${error}"
		}, {
			type : 'danger'
		});
		</c:if>
	});
</script>
</head>

<body>
    <sec:authorize access="hasRole('ROLE_USER')">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/home">
                        <spring:message code='app.name' />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="/home">
                                <spring:message code='home' />
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <spring:message code='simulations' />
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/simulations?ongoing=true">
                                        <spring:message code='ongoing' />
                                    </a>
                                </li>
                                <li>
                                    <a href="/simulations?ongoing=false">
                                        <spring:message code='results' />
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="/configs">
                                <spring:message code='configs' />
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li>
                                <a href="/admin">
                                    <span class="glyphicon glyphicon-wrench"></span>
                                    <spring:message code="admin" />
                                </a>
                            </li>
                        </sec:authorize>
                        <span class="navbar-text">
                                <span class="glyphicon glyphicon-user"></span>
                                <sec:authentication property="principal.username" />
                        </span>
                        <li>
                            <a href="/logout">
                                <span class="glyphicon glyphicon-log-out"></span>
                                <spring:message code='log.out' />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_USER')">
        <div class="thinPagePanel panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title">
                    <spring:message code='create.panel.title' />
                </h2>
            </div>
            <div class="pagePanelContent">
                <c:url var="createSimulationUrl"
                    value="/createSimulationAndConfig?${_csrf.parameterName}=${_csrf.token}" />
                <form:form action="${createSimulationUrl}" method="POST" class="form-horizontal"
                    commandName="startSimulation" enctype="multipart/form-data">
                    <h5 class="input-group-element">
                        <spring:message code='create.description' />
                    </h5>
                    <div class="input-group input-sm">
                        <label class="input-group-addon" for="simulation_config">
                            <i class="fa fa-cog"></i>
                        </label>
                        <input type="file" class="inputfile" id="simulation_config" name="simulation_config" required>
                        <label class="form-control btn btn-primary overflowableContent" for="simulation_config">
                            <i class="fa fa-upload" style="padding-right: 5px" aria-hidden="true"></i>
                            <span>
                                <spring:message code='create.choose.simulation.file.button.text' />
                            </span>
                        </label>
                    </div>
                    <div class="input-group input-sm">
                        <label class="input-group-addon" for="foram_config">
                            <i class="fa fa-cog"></i>
                        </label>
                        <input type="file" class="inputfile" id="foram_config" name="foram_config" required>
                        <label class="form-control btn btn-primary overflowableContent" for="foram_config">
                            <i class="fa fa-upload" style="padding-right: 5px" aria-hidden="true"></i>
                            <span>
                                <spring:message code='create.choose.foram.file.button.text' />
                            </span>
                        </label>
                    </div>
                    <div class="input-group input-sm">
                        <label class="input-group-addon" for="environment_config">
                            <i class="fa fa-cog"></i>
                        </label>
                        <input type="file" class="inputfile" id="environment_config" name="environment_config" required>
                        <label class="form-control btn btn-primary overflowableContent" for="environment_config">
                            <i class="fa fa-upload" style="padding-right: 5px" aria-hidden="true"></i>
                            <span>
                                <spring:message code='create.choose.environment.file.button.text' />
                            </span>
                        </label>
                    </div>
                    <div class="input-group input-sm">
                        <label class="input-group-addon" for="tags">
                            <i class="fa fa-tags" aria-hidden="true"></i>
                        </label>
                        <input type="text" class="form-control" id="tags" name="tags"
                            placeholder="<spring:message code='tags.form.hint' />">
                    </div>
                    <div class="input-group input-sm">
                        <label class="input-group-addon" for="machine">
                            <i class="fa fa-cog"></i>
                        </label>
                        <select class="form-control" id="machine" name="machine" required>
                            <c:choose>
                                <c:when test="${fn:length(simulators) == 0}">
                                    <option selected value><spring:message
                                            code='no.available.simulation.servers' /></option>
                                </c:when>
                                <c:otherwise>
                                    <option disabled selected value style="display: none"><spring:message
                                            code='choose.simulation.server' /></option>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach begin="1" end="${fn:length(simulators)}" var="index">
                                <option>${simulators.get(index-1)}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="form-actions input-group-element">
                        <button type="submit" class="btn btn-primary btn-block" name="save&run" value="Save&Run">
                            <spring:message code='create.start.simulation.button.text' />
                        </button>
                    </div>
                </form:form>
            </div>
        </div>
    </sec:authorize>
</body>
</html>
