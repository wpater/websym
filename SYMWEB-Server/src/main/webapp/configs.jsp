<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SYMWEB</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cosmo/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-h21C2fcDk/eFsW9sC9h0dhokq5pDinLNklTKoxIZRUn3+hvmgQSffLLQ4G4l2eEr" crossorigin="anonymous">
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet" />
<script src="https://rawgit.com/makeusabrew/bootbox/f3a04a57877cab071738de558581fbc91812dce9/bootbox.js"></script>
<link href="<c:url value='/static/css/animate.css' />" rel="stylesheet" />
<script src="<c:url value='/static/js/bootstrap-notify.min.js' />"></script>
</head>
<!-- Search -->
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$(".search")
								.keyup(
										function() {
											var searchTerm = $(".search").val();
											var listItem = $('.results tbody')
													.children('tr');
											var searchSplit = searchTerm
													.replace(/ /g,
															"'):containsi('")

											$
													.extend(
															$.expr[':'],
															{
																'containsi' : function(
																		elem,
																		i,
																		match,
																		array) {
																	return (elem.textContent
																			|| elem.innerText || '')
																			.toLowerCase()
																			.indexOf(
																					(match[3] || "")
																							.toLowerCase()) >= 0;
																}
															});

											$(".results tbody tr").not(
													":containsi('"
															+ searchSplit
															+ "')").each(
													function(e) {
														$(this).attr('visible',
																'false');
													});

											$(
													".results tbody tr:containsi('"
															+ searchSplit
															+ "')").each(
													function(e) {
														$(this).attr('visible',
																'true');
													});

											var jobCount = $('.results tbody tr[visible="true"]').length;
											$('.counter').text(
													jobCount + ' item');

											if (jobCount == '0') {
												$('.no-result').show();
											} else {
												$('.no-result').hide();
											}
										});
					});
</script>
<!-- Delete confirmation -->
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('.deleteConfigForm')
								.each(
										function() {
											this
													.addEventListener(
															"submit",
															function(e) {
																var currentForm = this;
																e
																		.preventDefault();
																bootbox
																		.confirm({
																			title : '<spring:message code="configs.delete.confirm.title"/>',
																			message : '<spring:message code="configs.delete.confirm.message"/>',
																			buttons : {
																				cancel : {
																					label : '<i class="fa fa-times"></i> <spring:message code="cancel"/>',
																					className : 'btn-primary'
																				},
																				confirm : {
																					label : '<i class="fa fa-check"></i> <spring:message code="confirm"/>',
																					className : 'btn-danger'
																				}
																			},
																			callback : function(
																					result) {
																				if (result) {
																					currentForm
																							.submit();
																				}
																			}
																		});
															}, false);

										});
					});
</script>
<!-- Notifications -->
<script>
	$(document).ready(function() {
		$.notifyDefaults({
			allow_dismiss : true,
			delay : 8000,
			offset : {
				x : 20,
				y : 20
			},
			placement : {
				from : 'bottom',
				align : 'right'
			}
		});
		<c:if test="${not empty success}">
		$.notify({
			message : "${success}"
		}, {
			type : 'success'
		});
		</c:if>
		<c:if test="${not empty error}">
		$.notify({
			message : "${error}"
		}, {
			type : 'danger'
		});
		</c:if>
	});
</script>
<body>
    <sec:authorize access="hasRole('ROLE_USER')">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/home">
                        <spring:message code='app.name' />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="/home">
                                <spring:message code='home' />
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <spring:message code='simulations' />
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/simulations?ongoing=true">
                                        <spring:message code='ongoing' />
                                    </a>
                                </li>
                                <li>
                                    <a href="/simulations?ongoing=false">
                                        <spring:message code='results' />
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="/configs">
                                <spring:message code='configs' />
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li>
                                <a href="/admin">
                                    <span class="glyphicon glyphicon-wrench"></span>
                                    <spring:message code="admin" />
                                </a>
                            </li>
                        </sec:authorize>
                        <span class="navbar-text">
                                <span class="glyphicon glyphicon-user"></span>
                                <sec:authentication property="principal.username" />
                        </span>
                        <li>
                            <a href="/logout">
                                <span class="glyphicon glyphicon-log-out"></span>
                                <spring:message code='log.out' />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </sec:authorize>
    <div class="pagePanel panel panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title">
                <spring:message code='configs.panel.title' />
            </h2>
        </div>
        <div class="pagePanelContent">
            <h5>
                <spring:message code='configs.description' />
            </h5>
            <sec:authorize access="hasRole('ROLE_USER')">
                <div class="form-group pull-right">
                    <input type="text" class="search form-control"
                        placeholder="<spring:message code='configs.search.hint'/>">
                </div>
                <span class="counter pull-right"></span>
                <table style="table-layout: fixed;"
                    class="table table-fixed table-bordered table-hover table-condensed results panel-primary">
                    <thead class="panel-heading">
                        <tr>
                            <th class="col-xs-1 table-separator">
                                <spring:message code='id' />
                            </th>
                            <th class="col-xs-5 table-separator overflowableContent">
                                <spring:message code='tags' />
                            </th>
                            <th class="col-xs-6 overflowableContent">
                                <spring:message code='actions' />
                            </th>
                        </tr>
                        <tr class="warning no-result">
                            <td colspan="4">
                                <i class="fa fa-warning"></i>
                                No result
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach begin="1" end="${fn:length(table)}" var="index">
                            <tr>
                                <td class="table-separator text-center">${index}</td>
                                <td class="table-separator overflowableContent">${table[index-1].getTagsString()}</td>
                                <td>
                                    <div class="form-actions">
                                        <div class="input-group-btn">
                                            <div class="col-xs-3 configsButtonCell">
                                                <a class="btn btn-primary btn-xs overflowableContent"
                                                    href="/loadConfig?id=${table[index-1].getIdConfig()}">
                                                    <spring:message code='load' />
                                                </a>
                                            </div>
                                            <div class="col-xs-4 configsButtonCell">
                                                <a class="btn btn-primary btn-xs overflowableContent"
                                                    href="/configSimulations?id=${table[index-1].getIdConfig()}">
                                                    <spring:message code='simulations' />
                                                </a>
                                            </div>
                                            <div class="col-xs-3 configsButtonCell">
                                                <form class="deleteConfigForm" action="/deleteConfig" method="post">
                                                    <input type="hidden" name="${_csrf.parameterName}"
                                                        value="${_csrf.token}" />
                                                    <input type="hidden" name="id"
                                                        value="${table[index-1].getIdConfig()}" />
                                                    <button type="submit"
                                                        class="btn btn-danger btn-block btn-xs overflowableContent">
                                                        <spring:message code='delete' />
                                                    </button>
                                                </form>
                                            </div>
                                            <div class="col-xs-2 configsButtonCell">
                                                <a class="btn btn-success btn-xs overflowableContent"
                                                    href="/runConfig?id=${table[index-1].getIdConfig()}">
                                                    <spring:message code='run' />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <c:url var="createSimulationUrl" value="/createConfig" />
                <form:form action="${createSimulationUrl}" method="POST" class="form-horizontal"
                    commandName="startSimulation">
                    <ul class="nav nav-pills nav-justified">
                        <li class="active">
                            <a data-toggle="tab" href="#foram">
                                <spring:message code='config.foram' />
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#simulation">
                                <spring:message code='config.simulation' />
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#environment">
                                <spring:message code='config.environment' />
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" style="">
                        <div id="foram" class="tab-pane fade in active">
                            <div class="input-form">
                                <div style="width: 100%">
                                    <textarea class="form-control config-editor" id="foram_config" name="foram_config"
                                        required>${foram}</textarea>
                                </div>
                            </div>
                        </div>
                        <div id="simulation" class="tab-pane fade">
                            <div class="input-form">
                                <div style="width: 100%">
                                    <textarea class="form-control config-editor" id="simulation_config"
                                        name="simulation_config" required>${simulation}</textarea>
                                </div>
                            </div>
                        </div>
                        <div id="environment" class="tab-pane fade">
                            <div class="input-form input-group">
                                <div style="width: 100%">
                                    <textarea class="form-control config-editor" id="environment_config"
                                        name="environment_config" required>${environment}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <label class="input-group-addon" for="tags">
                                    <i class="fa fa-tags"></i>
                                </label>
                                <input type="text" class="form-control" id="tags" name="tags"
                                    placeholder="<spring:message code='tags.form.hint'/>">
                                <span class="input-group-btn">
                                    <button class="btn btn-success" type="submit" name="run" value="true">
                                        <spring:message code='save.and.run' />
                                    </button>
                                    <button class="btn btn-primary" type="submit" name="run" value="false">
                                        <spring:message code='save' />
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form:form>
            </sec:authorize>
        </div>
    </div>
</body>
</html>
