// ---- ENVIRONMENT ----

function randInt(maxValue) {
   return Math.round((Math.random() * maxValue))
}

// initialization-only kernels

function oceanSize() {
   return {x: 1, y: 1, z: 0.15};
    //2m x 2m x 15 cm
}

function initialForamsCount(x, y, z) {
     return count = randInt(6);
}

/* in portions (grams?) per cubic meter
*  1 kg = 1000 grams
*  1 grams = 0.001 kg
* */
function initialAlgaeDensity(x, y, z) { //x, y, z is expressed in meteres
    var surfaceDensity = 20000.0; //100 kg in 1 cubic meter
    var returnDensity = surfaceDensity*(oceanSize().z-z)/oceanSize().z;
    return returnDensity; //grams per cubic meter: 10 kg in m3
}

function boundaryConditions() {
   return "fixed";
//   return "periodic";
//   return "mixed";
}

// other kernels

/**
 Each kernel gets two parameters:

 time          - time in hours from the beginning of the simulation
 envStates     - array of environment states in the previous step of the cell and its neighbors:
                 envStates[0] - current cell
                 envStates[1] - front neighbor (x + 1, y,     z    )
                 envStates[2] - right neighbor (x,     y + 1, z    )
                 envStates[3] - upper neighbor (x,     y,     z + 1)
                 envStates[4] - back  neighbor (x - 1, y,     z    )
                 envStates[5] - left  neighbor (x,     y - 1, z    )
                 envStates[6] - lower neighbor (x,     y,     z - 1)

                 elements of this array may have null values if current cell is on the edge of the grid (eg. if cell lies on
                 the right edge then it has no right neighbor)


 each element of envStates[] array has following structure:

 envState {                        - mind you that in time = 0, some fields may not be initialized (insolation, algaeEnergy etc.)
   oxygen,
   temperature,
   salinity,
   algaeAvailability,
   insolation,
   ph,
   position: { x, y, z },
   algaeEnergy,
   algaeGrowth,
   currentDirection: { x, y, z }
 }
 */

/* amount of energy in a single gram of algae (food).
* from foraminifera point of view*/
function algaeEnergy(time, envStates) {
   return 300000.0;
}

function algaeGrowth(time, envStates) {
    if(envStates[0].algaeAvailability > 2.0*(1.2 - envStates[0].position.z / oceanSize().z))
        return 0.0;
    if(envStates[0].position.z > 0.0)
        return Math.abs(0.025*envStates[6].algaeAvailability);
    else
        return 0.007;
    /*if (envStates[0].algaeAvailability < 5.0) {
        var foodToAddOnSurface = 0.05;
        var foodToAdd = foodToAddOnSurface * (1.0 - envStates[0].position.z / oceanSize().z);
        return foodToAdd;
    }
    else
        return 0;*/
}


function insolation(time, envStates) {
    return 0.0; // TODO: return proper value
}

function oxygen(time, envStates) {
    var surfaceOxygen = 1.0;
    var o2 = surfaceOxygen - 10 * envStates[0].position.z;
    return Math.max(0.0, o2);
}

function temperature(time, envStates) {
    return 0.0; // TODO: return proper value
}

function salinity(time, envStates) {
    return 0.0; // TODO: return proper value
}

function ph(time, envStates) {
   return 0.0; // TODO: return proper value
}

// TODO: change kernel name to currentSpeed
function currentDirection(time, envStates) {
   return {x: 0.0, y: 0.0, z: 0.0};
}

