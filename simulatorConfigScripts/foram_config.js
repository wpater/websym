// ---- FORAM ----

function rand() {
   return Math.random();
}

function reproductionType() {
   //return "sexual";
   return "sexual_asexual";
}

function foramActiveMotion() {
   return true; //benthic
}


function initialEnergy() {
   return 700.0;
}

/**
 Each kernel gets three parameters:

 envState {
   oxygen,
   temperature,
   salinity,
   algaeAvailability,
   insolation,
   ph,
   position: { x, y, z },
   algaeEnergy,
   algaeGrowth,
   currentDirection: { x, y, z }
 }

 foramState {
   genotype: {
     translationFactor: double[3],       - value of each gene is an array of 3 elements: [effective value, value from A chromosome, value B]
     growthFactor: double[3],
     rotationAngle: double[3],
     deviationAngle: double[3],
     ...
   },
   foramActiveMotion,
   energy,
   age,
   shell: {
     firstChamberRadius,
     lastChamberRadius,
     chambersCount,
     volumeShell
   }
 }

 time - time in hours from the beginning of the simulation
 */

function consumptionPerHour(envState, foramState, time)
{
   //Genome.FOOD_COLLECTING_RATE and Genome.FOOD_COLLECTING_RANGE influence energy consumption!
   var volumeOfPseudopodias = 4.19*Math.pow(foramState.genotype.foodCollectingRange[0],3)*foramState.genotype.foodCollectingRate[0];

   var volumeOfCytoplasm = foramState.energy / foramState.genotype.metabolicEffectiveness[0];
    if(envState.position.z < 0.35)
        volumeOfCytoplasm = volumeOfCytoplasm* (0.5+foramState.shell.shapeFactor);
    else
        volumeOfCytoplasm = volumeOfCytoplasm* (1.5-foramState.shell.shapeFactor);
   //we take volume of cytoplasm powered to 0.2!
   var consumption = Math.pow(volumeOfCytoplasm, 0.2) * foramState.genotype.energyDemandPerVolumePerHour[0];

   if(!isInHibernationState(envState, foramState, time))
      return consumption;
    else
      return consumption *  foramState.genotype.hibernationEnergyRate[0];
}

function energyNeededForGrowth(envState, foramState, time) {
   //use 50% of stored energy to build new chamber
   //return 0.3*foramState.shell.volumeShell*foramState.genotype.metabolicEffectiveness[0];

   //energy needs are depenedent on new chamber size
   var newChamberRadius = foramState.shell.lastChamberRadius*foramState.genotype.translationFactor[0];
   var newChamberVolume = 4.19*Math.pow(newChamberRadius, 3.0);
   return 0.7*newChamberVolume;  //the cost of building new chamber
}

function growthProbability(envState, foramState, time) {
   return 0.98;
}

function chambersLimit(envState, foramState, time) {
   return 10;
}

function energyNeededToReproduce(envState, foramState, time) {
   return 0.8*foramState.shell.volumeShell*foramState.genotype.metabolicEffectiveness[0];
}

function reproductionProbability(envState, foramState, time) {
   return 0.99;
}

function gametesProduction(envState, foramState, time) {
    var gamontVolume = 100000.0; //in microm^3
    var gametVolume = 1000.0;
    var volumeOfCytoplasm = foramState.energy / foramState.genotype.metabolicEffectiveness[0];
   //volume of cytoplasm is expressed in microm^3
   //about 50% of cytoplasm is vanishing during reproduction - assumption
    if(envState.position.z < 0.35)
         volumeOfCytoplasm = volumeOfCytoplasm* (1.6-foramState.shell.shapeFactor);
    else
         volumeOfCytoplasm = volumeOfCytoplasm* (foramState.shell.shapeFactor+0.1);
   if(foramState.foramType.ploidy == "HAPLOID")
      return volumeOfCytoplasm / gametVolume;
                       //produces gametes, each with size 1-3 micrometers
                       //volume of gametes ~4 microm^3 to ~100 microm^3
   else
      return volumeOfCytoplasm / gamontVolume;
                       //produces gamonts, each with size around 40-80 micrometers
                       //volume of gamonts, ~250k microm^3 to 2m microm^2
}

function gametesSievingCoefficient(envState, foramState, time) {
    if(foramState.foramType.ploidy == "HAPLOID")
        return 0.997; //gametes
    else
        return 0.165;  //gamonts
}


function crossingOverOperator(envState, foramState, time) {
   // return "OnePointCrossingOverOperator";
   // return "TwoPointCrossingOverOperator";
   return "UniformCrossingOverOperator";
}

function globalMutationProbability(envState, foramState, time) {
   return 1.0;
}

function foramActiveSpeed(envState, foramState, time) {
    var returnObject = {x: 0.000004, y: 0.000004, z: 0.0000001};
    //active speed depends on shell size, volume
   return returnObject;
}

function activeMotionEnergyCostPerMeter(envState, foramState, time) {
   var returnObject = {up: 0.001, down: 0.001, horizontally: 0.0001};
   var contributions = foramState.shell.volumeShell * (2.0-foramState.shell.shapeFactor);
    //returnObject.horizontally = returnObject.horizontally*contributions;
   returnObject.horizontally = returnObject.horizontally*contributions*(10*envState.position.z+1);
   returnObject.up = returnObject.up*contributions*(200*envState.position.z+1);
   returnObject.down = returnObject.down*contributions*(200*envState.position.z+1);
    //energy movement depends on shell size
   return returnObject;
}

// -------------------------------------------------------------------------

function shouldDie(envState, foramState, time) {
    var energyLow = foramState.energy < foramState.genotype.minEnergy[0];
   /* var treshold;
    if(foramState.foramType.ploidy == "DIPLOID"){
        treshold = (foramState.shell.chambersCount == 1) ? 0.95 : 0.99;
    } else {
        treshold = (foramState.shell.chambersCount == 1) ? 0.975 : 1.0;
    }
    var predators = rand() > treshold;*/
    var predators = false;
    return energyLow || predators;
}


function isInHibernationState(envState, foramState, time) {
   return foramState.energy < foramState.genotype.hibernationEnergyLevel[0];
}

function canReproduce(envState, foramState, time) {
    var oldEnough;
    var energyEnough;
    if(foramState.foramType.ploidy == "DIPLOID"){
        oldEnough = foramState.age >= foramState.genotype.minAdultAge[0];
    } else {
        oldEnough = foramState.age >= 0.7 * foramState.genotype.minAdultAge[0];
    }
    energyEnough = foramState.energy > energyNeededToReproduce(envState, foramState, time);
    var reproductionProbable   = rand() > reproductionProbability(envState, foramState, time);

   return oldEnough  && energyEnough && reproductionProbable && !isInHibernationState(envState, foramState, time);
}

function canCreateChamber(envState, foramState, time) {
   var volumeOfCytoplasm   = foramState.energy/foramState.genotype.metabolicEffectiveness[0];
   var needMoreSpace       = volumeOfCytoplasm > 0.95 * foramState.shell.volumeShell;
   var energyEnough        = foramState.energy > energyNeededForGrowth(envState, foramState, time);
   var growthProbable      = rand() > growthProbability(envState, foramState, time);
   return  needMoreSpace && energyEnough && growthProbable && !isInHibernationState(envState, foramState, time);
}

function canMigrate(envState, foramState, time) {
   // benthic forams cannot move when in hibernation
   //migarate only if there os no food.
   var shouldMigrate = envState.algaeAvailability < 0.2;
   return shouldMigrate && !(foramState.foramActiveMotion && isInHibernationState(envState, foramState, time));
}

// -------------------------------------------------------------------------

/**
 * position {  x, y, z }  - position in meters
 */
function initialGenome(position) {
   return [
      {
         name: "translationFactor",
         value: 0.15,
         mutationProbability: 0.7,
         mutationRate: 0.1,
         minValue: -1.0,
         maxValue: 1.0
      },
      {
         name: "growthFactor",
         value: 1.1,
         mutationProbability: 0.7,
         mutationRate: 0.1,
         minValue: 1.0,
         maxValue: 2.0
      },
      {
         name: "rotationAngle",
         value: 1.0,
         mutationProbability: 0.7,
         mutationRate: 2.0,
         minValue: -180.0,
         maxValue: 180.0
      },
      {
         name: "deviationAngle",
         value: 15.0,
         mutationProbability: 0.8,
         mutationRate: 5.0,
         minValue: -180.0,
         maxValue: 180.0
      },

      {
         name: "haploidFirstChamberRadius",
         value: 40.0, //in micrometers 10e-3 m
         mutationProbability: 0.7,
         mutationRate: 0.05,
         minValue: 30.0,
         maxValue: 100.0
      },
      {
         name: "diploidFirstChamberRadius",
         value: 10.0,  //in micrometers
         mutationProbability: 0.7,
         mutationRate: 0.05,
         minValue: 5.0,
         maxValue: 30.0
      },
      {
         name: "wallThicknessFactor",
         value: 0.1,
         mutationProbability: 0.7,
         mutationRate: 0.01,
         minValue: 0.01,
         maxValue: 0.5
      },
      {
         name: "minAdultAge",
         value: 110*24,
         mutationProbability: 0.2,
         mutationRate: 0.5,
         minValue: 30*24,
         maxValue: 360*24
      },
      {
         name: "foodCollectingRate",
         value: 0.05,
         mutationProbability: 0.1,
         mutationRate: 0.00001,
         minValue: 0.001,
         maxValue: 0.1
      },
      {
         name: "foodCollectingRange",
         value: 0.001,
         mutationProbability: 0.2,
         mutationRate: 0.000001,
         minValue: 0.0001,
         maxValue: 0.1
      },
      {
         name: "energyDemandPerVolumePerHour",
         value: 0.1,
         mutationProbability: 0.1,
         mutationRate: 0.001,
         minValue: 0.001,
         maxValue: 0.999
      },
      {
         name: "minEnergy",
         value: 1.0,
         mutationProbability: 0.2,
         mutationRate: 0.1,
         minValue: 0.0,
         maxValue: 100
      },
      {
         name: "chamberGrowthCostFactor",
         value: 0.1,
         mutationProbability: 0.1,
         mutationRate: 0.001,
         minValue: 0.01,
         maxValue: 0.9
      },
      {
         name: "metabolicEffectiveness",
         value: 0.5,
         mutationProbability: 0.2,
         mutationRate: 0.001,
         minValue: 0.1,
         maxValue: 0.999
      },

      {
         name: "hibernationEnergyLevel",
         value: 100,
         mutationProbability: 0.1,
         mutationRate: 0.001,
         minValue: 0.001,
         maxValue: 1000
      },
      {
         name: "hibernationEnergyRate",
         value: 0.001,
         mutationProbability: 0.1,
         mutationRate: 0.001,
         minValue: 0.0001,
         maxValue: 100
      }
   ]
}
