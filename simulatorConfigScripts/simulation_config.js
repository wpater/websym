
// ---- SIMULATION ----

function simulationName() {
}

function simulationDescription() {
   return "sample simulation description";
}


function statsStride() {
    return 5; //
}

function simulationDuration() {
   return 200*24;
}

function unitLengthInMeters() {
   return {x: 0.1, y: 0.1, z: 0.01};
}

function scaleGrid() {
    return {x: 1.0, y: 1.0, z: 5.0};
}

function stepDurationInHours() {
   return 6;
}
